'use strict'
const express = require('express')
const fs = require('fs')
const http = require('http')
const https = require('https')
const path = require('path')
const cors = require('cors')
const saveFolder = 'saves'
const app = express()
const httpsPort = 3443
const httpPort = 80
const directoryToServe = path.join(__dirname, "dist")
//redirect from http on 8080 to https on 3443
//app.all('*', ensureSecure); // at top of routing calls

const server =http.createServer(app).listen(httpPort)

/*function ensureSecure(req, res, next){
  if(req.secure){
    // OK, continue
    return next();
  };
  // handle port numbers if you need non defaults
  // res.redirect('https://' + req.host + req.url); // express 3.x
  res.redirect('https://' + req.hostname + ':'+ httpsPort +req.url); // express 4.x
}*/

app.use(cors());
app.use('/', express.static(directoryToServe))


/*const httpsOptions = {
	cert: fs.readFileSync(path.join(__dirname, 'ssl', 'server.crt')),
	key: fs.readFileSync(path.join(__dirname, 'ssl', 'server.key'))
  //cert: fs.readFileSync(path.join(__dirname, 'ssl_192.168.1.51', 'server.crt')),
  //key: fs.readFileSync(path.join(__dirname, 'ssl_192.168.1.51', 'server.key'))
}*/

/*const server = https.createServer(httpsOptions, app)
server.listen(httpsPort, function(){
      console.log(`Serving ${directoryToServe}/ at https://localhost:${httpsPort}`)
    })
*/
//
const io = require('socket.io')(server);
/*I listen on the connection event for incoming sockets,
and I log it to the console.*/
io.on('connection', function(socket){
  //console.log('a user connected');
  socket.broadcast.emit('user connected');
  /*I listen on the disconnect event
  and call function() when it occurs
  */
  /*socket.on('disconnect', function(){
    console.log('user disconnected');
  });*/
  socket.on('disconnect', function(){
    socket.broadcast.emit('user disconnected');
  });
  /*
  when a chat message event occurs function(msg)
  is called
  */
  socket.on('chat message', function(msg){
    console.log('message: ' + msg);
    //send event to everyone, including the sender
    //io.emit('chat message', msg);
    socket.broadcast.emit('chat message', msg);
  });

	socket.on('save file', function(detail){
		let filename = detail.name
		let file = detail.file
		//let basePath = saveFolder
		//marshall the file: pretty printing (readable) and  spacing set to 2
		file = JSON.stringify(file, null, 2)
		console.log('SAVING ' + filename + ' into \'' + saveFolder +'\'')
		//IMPORTANTE: remember to run node instead of nodemon because of reset of connection
		fs.writeFile(path.join(__dirname, saveFolder, filename), file, (err) => {
  		if (err){
				console.log(err);
			}else{
				console.log("Successfully Written to File.");
			}
		});

	})
	socket.on('list request', function(detail){
		//let name = detail.name
		//let file = detail.file
		//let filename = name+'.json'
		//const baseFolder = saveFolder
		let filenames = []
		console.log('LOADING list from \'' + saveFolder+'\'')

		//read all the file into the folder
		fs.readdirSync(path.join(__dirname, saveFolder)).forEach(file => {
			//add to an array
			filenames.push(file)
		});
		//send message back to client
		socket.emit('file list', {filenames: filenames})
		/*fs.readFile(basePath+ filename, "utf-8", (err, data) => {
    	if (err){
				console.log(err)
			}else{
				//unmarshall the file
				let file = JSON.parse(data)
				console.log(file)
			}
		})*/


	})
});
//
