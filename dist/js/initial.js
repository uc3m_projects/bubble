$(document).ready(function(){

	if(detectMob()){
		$("#mobile_panel").show();
		$("#initial_panel").hide();
		$("#help").hide();
		$("#task").hide();
		$(".initial_message").hide();
	}

	if($(window).width()>=840){
		$(".initial_message").width(840);
		$(".initial_message").css("left",($(window).width()-$(".initial_message").width())/2);
	}
	else{
		$(".initial_message").width($(window).width());
		$(".initial_message").css("left",0);
	}

	$("#instructions_computer_manual").width($(".initial_message").width()-40);
	$("#instructions_oculus_manual").width($(".initial_message").width()-40);

	$(".initial_message").css("top",($(window).height()-$(".initial_message").height())/2);

	$("#start").css("right",($(window).width()-$(".initial_message").width())/2+10);
	$(".initial_buttons").css("bottom",($(window).height()-$(".initial_message").height())/2+10);

	$(".title_instructions").click(function(){
		if($(this).children(".material-icons").html()=="keyboard_arrow_up")
			$(this).children(".material-icons").html("keyboard_arrow_down");
		else
			$(this).children(".material-icons").html("keyboard_arrow_up");
		if($(this).children("h3").html()=="Learning Activity"){
			$(".instructions_activity").toggle();
		}
		else
			$(this).next("p").toggle();

	});

	$("#start").click(function(){
		$("#initial_panel").hide();
		$(".initial_message").hide();
	});

	$("#help").click(function(){
		$("#initial_panel").show();
		$(".initial_message").show();
	});

	$("#task").click(function(){
		$("#initial_panel").show();
		$(".initial_message").show();
		$(".instructions_activity").show();
		$("#instructions_computer").hide();
		$("#instructions_glasses").hide();
	});
});

function detectMob() {
    const toMatch = [
        /Android/i,
        /webOS/i,
        /iPhone/i,
        /iPad/i,
        /iPod/i,
        /BlackBerry/i,
        /Windows Phone/i
    ];

    return toMatch.some((toMatchItem) => {
        return navigator.userAgent.match(toMatchItem);
    });
}