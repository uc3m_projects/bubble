var socket = io();
//a lock variable to prevent multiple event triggering
var eventLocker = false

//global variables to push and pull bubbles
var push = false
var pull = false
//global map to navigate through menus: it will hold the current starting index of menu options
var menuMap = {}
//global var to navigate through the saves list
var loadMenuIndex = 0
//global var to handle speech cmd into menus
var newNameCnt = 0
var newSaveCnt = 0
var waitingForName = false
var editingName = false
var editingFrequency = false
var waitingForSave = false
//global var to push without using menu
var requestingPushInto = null
var waitingForPushInto = false
//global var to travel through levels
var currStack = null
//global var to prevent conflicts with raycaster
var buttonIntersected = false
var rightWristMenuOpen = false
var leftWristMenuOpen = false
//global var to easily handle change in the frequency
var currMaxFreq = 0
//global var to hold the current filename
var currFilename = null
//TODO: rimuovere
var allRecords = []
var csvString = ""
var csvIndex = 1760

//this variable is to handle the interaction with keyboard
var lookAtEntityFlag = false
var lookAtEntity = null
var lastKey = null
var outerBubbleId = "";
//
const saveFolder = './saves/'
//dimension of each page of a list
const menuSize = 4
//dimension of load list
const loadMenuSize = 4
//const to prevent conflicts in clicking wrist menus
//original y was 0.3
//TODO: modificare original e hiding come segue
//original = "0 0.3 -0.5"
//hiding = "0 0.3 -0.6"
//original debug = "-2 1 -0.5"
//hiding debug "-2 1 -0.6"
const wristMenuOriginalPos = "0 0.6 -1.5"
const wristMenuHidingPos = "0 0.6 -20"
//TODO: provvisorio, da rimuovere
//const rightMenuPos = "2 1 -0.5"
//delay time (in ms) to destroy intersectable menu
const menuDelay = 1000
const cleanTime = 3000
//delay time to prevent multiple event firings
const blinkEventLockerTime = 1000
//delay
const hoverVisibilityTimeout = 3000
const speechVisibilityTimeout = 5000
const logVisibilityTimeout = 4000
//some const strings
const defaultFilename = 'New project'
const filenameBoxId = 'filenameBox'
const pathBoxId = 'pathBox'
const speechBoxId = 'speechBox'
const logBoxId = 'logBox'
//const messages to log
const nothingToLoadMsg = 'There are no savestates to load'
const noPushIntoTermsMsg = 'You can not push bubbles into individual terms'
const noPushIntoMsg = 'There are no categories in which you can push at this level'
const noPushOutMsg = 'You are not inside a category'
const duplicateNameMsg = 'Name already present, say another one'
const pushIntoMsg = 'Select a target from the list\nor click on a category bubble'
const addNameMsg = 'Speak and give a name to the new bubble\nClick on button when satisfied'
const newNameMsg = 'Speak and give a new name to the bubble\nClick on button when satisfied'
const newFreqMsg = 'Speak and give a new frequency to the term\nClick on button when satisfied'
const newSaveMsg = 'Speak and give a name to your savestate\nClick on button when satisfied'
const currLvlMsg = 'You are already in this level'

function cleaner(){
  setInterval((e) =>{
    let innerMenus = Array.from(document.getElementsByClassName('innerMenu'))
    innerMenus.forEach((el)=>{
      let visible = d3.select(el).attr('visible')
      if(!visible){
        d3.select(el).remove()
      }
    })
  }, cleanTime)
}
cleaner()
socket.on('chat message', function(msg){
    //sanitize string
    msg = msg.trim().toLowerCase();
    console.log('message received:' + msg);
    var speechBox = document.getElementById(speechBoxId)
    if(msg.includes('no-speech') ||
      msg.includes('not-allowed') ||
      msg.includes('error occurred')
    ){
      return
    }
    speechBox.setAttribute('value', msg);
    /*setTimeout((e)=>{
      speechBox.setAttribute('value', msg);
    }, speechVisibilityTimeout)*/
    command(msg);
});

//return true if string is a valid color
function validTextColor(stringToTest) {
    //Alter the following conditions according to your need.
    if (stringToTest === "") { return false; }
    if (stringToTest === "inherit") { return false; }
    if (stringToTest === "transparent") { return false; }

    var image = document.createElement("img");
    image.style.color = "rgb(0, 0, 0)";
    image.style.color = stringToTest;
    if (image.style.color !== "rgb(0, 0, 0)") { return true; }
    image.style.color = "rgb(255, 255, 255)";
    image.style.color = stringToTest;
    return image.style.color !== "rgb(255, 255, 255)";
}
//a single function to set/unset pull/push variable when button is down/up
function updateMovingVariables(toPull=true, buttondown=true){
  //if buttondown is true the pull/push variable must be set to true and viceversa
  if(toPull){
    pull = buttondown
  }else{
    push = buttondown
  }
}
function command(msg){
    let scene = document.querySelector('a-scene')
    if(waitingForName){
      let icons = ['android-add']
      let menuLabel = 'newNameMenu'
      let callback = confirmNameButton
      if(editingName){
        icons = ['edit']
        menuLabel = 'editNameMenu'
        callback = editNameButton
      }else if(editingFrequency){
        icons = ['edit']
        menuLabel = 'editFrequencyMenu'
        callback = editFrequencyButton
      }
      let options = [msg]
      let oldMenu = document.getElementsByClassName(menuLabel)[0]
      let bubble = oldMenu.parentNode
      //create a new inner menu not hidden
      let newMenu = createMenu(bubble, options, icons, callback, false, true)
      newMenu.classed(menuLabel, true)
      //remove old menu
      d3.select(oldMenu)
      .attr('visible', false)
      .attr('position', '0 0 0')
      //setTimeout((e)=>{oldMenu.remove()}, menuDelay)
      return

    }
    if(waitingForSave){
      let menuLabel = 'newSaveMenu'
      let oldMenu = document.getElementsByClassName(menuLabel)[0]
      let menuId = d3.select(oldMenu).attr('id')
      let controller = oldMenu.parentNode
      let saveCnt = 0
      let icons = ['edit']
      let callback = confirmSaveButton
      //default name
      let savename = msg+'.json'
      function newSaveCallback(detail){
        //remove listener from socket so that the same callback won't be called more than once
        socket.removeListener('file list', newSaveCallback)
        //let's prevent this event from firing too often
        console.log(savename)
        //get all files in the folder
        let filenames = detail.filenames
        //check if is already present
        filenames.sort().forEach(function(file){
          //if the file is already present modify the name
          if(file == savename){
            savename = msg+(++saveCnt)+'.json'
          }
        })
        let options = [savename]
        let icons = ['edit']
        //create a new wrist menu
        let newMenu = createWristMenu(controller, menuId, options, icons, callback, false)
        //mark the menu so it can be accessed outside the callback
        newMenu.classed(menuLabel, true)
        //mark the menu as an innerMenu
        .classed('innerMenu', true)
        //just click on inner menu to go back to previous menu
        .on('click', hideInnerWristMenus)
        //remove old menu
        d3.select(oldMenu).attr('visible', false)
        .attr('position', wristMenuHidingPos)
      }
      //request file list to server
      socket.emit('list request')
      //when we receive the answer
      socket.on('file list', newSaveCallback)
      return
    }
    //var mainBox = document.querySelector("#mainBox");
    //var pos = mainBox.getAttribute('position');
    switch(msg){
        /*case 'backward':
            mainBox.setAttribute('position', {x: pos.x, y:pos.y, z: pos.z + 1});
            break;
        case 'forward':
            mainBox.setAttribute('position', {x: pos.x, y:pos.y, z: pos.z - 1});
            break;
        case 'left':
            mainBox.setAttribute('position', {x: pos.x - 1, y:pos.y, z: pos.z});
            break;
        case 'right':
            mainBox.setAttribute('position', {x: pos.x + 1, y:pos.y, z: pos.z});
            break;
        case 'up':
            mainBox.setAttribute('position', {x: pos.x, y:pos.y + 1, z: pos.z});
            break;
        case 'down':
            mainBox.setAttribute('position', {x: pos.x, y:pos.y - 1, z: pos.z});
            break;*/
            //oppure exit?
        default:
            var bubble = d3.select(document.getElementById(msg))
            console.log(bubble)
            //the dome is not holdable
            if(!bubble.empty() && bubble.classed('holdable')){
              //add a class label to signal that is being attracted
              bubble.classed('attracted', true)
              //if a bubble is selected
              //for debug: USE '#camera' with WASD and '#cameraRig' with OCULUS
              var cameraPos = d3.select('#cameraRig').attr('position')
              console.log(cameraPos)
              //disable bouncing
              //bubble.attr('bouncing', null)
              var currPos = bubble.attr('position')
              var currRad = bubble.attr('geometry').radius
              //var nextPos = {x: cameraPos.x, y: cameraPos.y+ 1.6, z: cameraPos.z - 1.8}
              var nextPos = {x: cameraPos.x, y: cameraPos.y+1.6, z: cameraPos.z-currRad}
              var animation = {
                property: 'position',
                from: currPos,
                to: nextPos,
                easing: 'easeInQuad',
                dur: 1000
              }
              bubble.attr('animation__move', animation)
            }else {
              scene.emit('find-word', {word: msg})
            }

            /*var isSelected = mainBox.is('cursor-hovered')
            //var textBoxVisible = d3.select('#textBox').attr("visible")
            var cmdIsColor = validTextColor(msg)
            if(isSelected && msg == 'open' && !textBoxVisible)
              d3.select('#textBox').attr("visible", true)
            if(isSelected && msg == 'close' && textBoxVisible)
              d3.select('#textBox').attr("visible", false)
            //change coloro only if not selected
            if(!isSelected && cmdIsColor)
              mainBox.setAttribute('color', msg);*/
        break;
    }
}
//recursive function to search max freq
function lookForMaxFreq(obj){
  if(obj instanceof Array ){
    //we are in the term array
    obj.forEach((el)=>{
      if(el instanceof Object && el.hasOwnProperty('freq')){
        if(el.freq > currMaxFreq){
          //update frequency
          currMaxFreq = el.freq
        }
      }
    })
  }else if(obj instanceof Object){
    //we are in another object
    for(let prop in obj){
      if(obj.hasOwnProperty(prop)){
        //call recursion on obj[prop]
        lookForMaxFreq(obj[prop])
      }
    }
  }

}
//recursive function to search the path for a word
function getWordPath(obj, word){
  if(obj instanceof Array ){
    //we are in the term array
    for(let i=0; i<obj.length; i++){
      if(obj[i] instanceof Object && obj[i].hasOwnProperty('name')){
        if(obj[i].name == word){
          //update found
          return word
        }
      }
    }
    return null
  }else if(obj instanceof Object){
    //we are in another object
    for(let prop in obj){
      if(prop == word){
        return prop
      }else{
        //call recursion on obj[prop]
        let returned = getWordPath(obj[prop], word)
        if(returned != null && typeof returned !== 'undefined'){
          return prop + '/' + returned
        }
      }
    }
    return null
  }
}
//recursive function to build the proportions object
function getProportions(obj){
  let returnObj = {}
  let sum = 0
  let max = 0
  if(obj instanceof Array ){
    //we are in the term array
    for(let i=0; i<obj.length; i++){
      if(obj[i] instanceof Object && obj[i].hasOwnProperty('freq') && obj[i].hasOwnProperty('name')){
        let termName = obj[i].name
        //update sum
        sum += obj[i].freq
        //update max
        if(obj[i].freq > max){
          max = obj[i].freq
        }
        //create a subfield for each term
        returnObj[termName] = {}
        //add these fields only to have the same recursive structure
        returnObj[termName]['sumFreq'] = obj[i].freq
        returnObj[termName]['maxFreq'] = obj[i].freq
      }
    }
    returnObj['sumFreq'] = sum
    returnObj['maxFreq'] = max
    return returnObj

  }else if(obj instanceof Object){
    //we are in another object: iterate through properties
    for(let prop in obj){
      if(prop != 'color' && prop != 'name'){
        //fill the property with the result obtained applying recursion on the next level
        if(prop == 'terms'){
          return getProportions(obj[prop])
        }else{
          returnObj[prop] = getProportions(obj[prop])
        }
        //update sum
        sum += returnObj[prop]['sumFreq']
        //update max using the sums of inner levels
        if(returnObj[prop]['sumFreq'] > max){
          max = returnObj[prop]['sumFreq']
        }
      }//fine if su campi particolari: serve davvero?

    }//fine for
    //fill the properties for this level
    returnObj['sumFreq'] = sum
    returnObj['maxFreq'] = max
    return returnObj
  }
}
//recursive function to search all terms and fill the csv
function getAllTerms(obj){

  if(obj instanceof Array ){
    //we are in the term array
    obj.forEach((el)=>{
      if(el instanceof Object && el.hasOwnProperty('name')){
        const limit = 5
        let argument = {}
        argument.keywords = el.name
        argument.limit = limit
        argument.print_urls = false
        argument.format = 'jpg'
        argument.aspect_ratio = 'wide'
        let argStr = ""
        for(let i=0; i< limit; i++){
          argStr += csvIndex++ +"," + el.name + ",image,,./media/" + el.name +(i+1)+".jpg,,,\n\n"
        }
        allRecords.push(argument)
        csvString += argStr

      }
    })
  }else if(obj instanceof Object){
    //we are in another object
    for(let prop in obj){
      if(obj.hasOwnProperty(prop)){
        //call recursion on obj[prop]
        getAllTerms(obj[prop])
      }
    }
  }

}
function showInfo(){
  let visible = this.getAttribute('visible')
  /*if(!visible)
    return false*/
  let id = this.getAttribute('id')
  if(id != null){
    d3.select('#'+speechBoxId)
      .attr('value', id)
    //after a certain time remove text
    /*setTimeout((e)=>{
      d3.select('#'+speechBoxId)
        .attr('value', '')
    }, hoverVisibilityTimeout)*/
  }
}

function hideInfo(){
  let visible = this.getAttribute('visible')
  if(!visible)
    return false
  d3.select('#'+speechBoxId)
    .attr('value', '')
}
function getArraysFromObject(json){
  let obj = {}
  let termKeys = 'termKeys'
  let termData = 'termData'
  let keys = 'keys'
  let data = 'data'
  obj[keys] = [];
  obj[data] = [];
  obj[termKeys] = [];
  obj[termData] = [];

  // we fill the arrays with the object properties
  for (let key in json) {
    if (key!='color' && key!='name' && json.hasOwnProperty(key)) {
      if(key == 'terms'){
        //fill the terms arrays with the array data itself
        for(let i=0; i<json.terms.length; i++){
          obj[termKeys].push(json.terms[i].name)
          obj[termData].push(json.terms[i])
        }
      }else{
        obj[keys].push(key);
        obj[data].push(json[key]); // Not necessary, but cleaner, in my opinion.
      }

    }
  }

  return obj
}
function createImage(jsonData, lvl, path){
  const pos = {x: 0, y: 3, z: -3}
  let domeId = jsonData.name
  let classLabel = 'level'+lvl
  let imageCard = d3.select('a-scene')
    .append('a-entity')
    .attr('id', domeId + "-image")
    .attr('mixin', 'myBubble-image')
    .attr('material', 'shader:flat;src:'+path)
    .classed(classLabel, true)
    .classed('card', true)

  return imageCard

}
function createCard(jsonData, lvl, headerStr="Header", bodyStr="Body"){
  /*if(jsonData == null)
    return
  let domeId = jsonData.name
  let freq = jsonData.freq*/
  let domeId = jsonData.name
  let classLabel = 'level'+lvl
  let bubbleInfo = d3.select('a-scene')
    .append('a-plane')
    .attr('id', domeId + "-info")
    .attr('mixin', 'myBubble-info')
    .classed(classLabel, true)
    .classed('card', true)
    //.attr('position', {x:0, y: 0, z: -3})
  let header = bubbleInfo
    .append("a-text")
    .attr('id', domeId + "-info-header")
    .attr("scale", "0.5 0.5 0.5")
    .attr("text", "align:center")
    //.attr("value", "Concerning " + domeId)
    .attr("value", headerStr)
    .attr("color", "red")
    .attr("position", function(d, i){
      var x = 0
      var y = 0.55
      var z = 0
      return x + " " + y + " " + z
    })
    .classed('header', true);
  let words = bubbleInfo
    .append("a-text")
    .attr('id', domeId + "-info-body")
    .attr("text", "align:left")
    .attr("color", "black")
    .attr("scale", "0.4 0.4 0.4")
    //.attr("value", 'frequency: ' + freq)
    .attr("value", bodyStr)
    .attr("position", function(d, i){
      var x = -0.9
      var y = 0.15
      var z = 0
      return x + " " + y + " " + z
    })
    .classed('body', true);
    return bubbleInfo

}
function createBubble(id, lvlLabel, nested, posMin = '-10 1.6 -8', posMax = '10 2 -2', rad = 1){
  let bubble = d3.select('a-scene')
     .append('a-entity')
     .attr('id', id)
     .classed('collidable', true)
     .classed('holdable', true)
     .classed(lvlLabel, true)
     .classed('nested', nested)
     .attr('geometry', 'radius:'+ rad)
     .attr('mixin', 'myBubble')
     .attr('random-position', 'min:' + posMin + ';max:' + posMax)
     .on('mouseenter', showInfo)
     .on('mouseleave', hideInfo)
     .on('hover-start', showInfo)
     .on('hover-end', hideInfo)


  return bubble
}

function createBubbleText(bubble, id, value){
  let bubbleId = bubble.attr('id')
  let bubbleRadius = parseFloat(document.getElementById(bubbleId).components.geometry.attrValue.radius)
  if(bubbleRadius > 1){
    bubbleRadius = 1
  }
  let textPos = {x: 0, y: -(bubbleRadius+0.3*bubbleRadius), z:0}
  let scale = {x: bubbleRadius, y: bubbleRadius, z: bubbleRadius}
  let text = bubble.append('a-entity')
      .attr('id', id)
      .attr('mixin', 'myBubble-text')
      .attr('position', textPos)
      .attr('text-geometry', 'value:'+value)
      //.attr('scale', scale)
  return text
}
function createInnerBubble(bubble, id, outerRad = 0, innerRad = 0){
  let bubbleRadius = parseFloat(document.getElementById(bubble.attr('id')).components.geometry.attrValue.radius)
  if(bubbleRadius > 1){
    //happens if two bubbles have the same name
    bubbleRadius = 1
  }
  if(outerRad == 0){
    outerRad = bubbleRadius
  }
  if(innerRad == 0){
    innerRad = (1/4)*outerRad
  }
  let innerBubble = bubble.append('a-entity')
      .classed('innerBubble', true)
      .attr('id', id)
      .attr('mixin', 'inner-bubble')
      .attr('geometry', 'radius:'+innerRad)
      .attr('random-position', 'min: ' + -outerRad/2 + ' ' + -outerRad/2 + ' ' + -outerRad/2 + ';' +
                               'max: ' + outerRad/2 + ' ' + outerRad/2 + ' ' + outerRad/2)
  return innerBubble
}
function createLevelBar(bubble, id, frequency){
  const maxLength = 100
  const startAngle = 40
  let perc = (frequency/currMaxFreq)
  let newLength = perc*maxLength
  let newStart = (startAngle+maxLength) - newLength
  let color = 'red'
  if(perc > 33/100){
    color = 'yellow'
  }
  if(perc > 66/100){
    color = 'green'
  }
  let enveloper = bubble.append('a-entity')
      .classed('levelBar', true)
      .attr('id', id)
      .attr('position', '0 0.3 0')
  let container = enveloper.append('a-entity')
      .attr('id', id +'-container')
      .attr('mixin', 'level-bar-container')
  let content = enveloper.append('a-entity')
      .attr('id', id +'-content')
      .attr('mixin', 'level-bar-content')
      .attr('position', '0 0 0.01')
      .attr('geometry', 'thetaStart:'+newStart +';thetaLength:' + newLength)
      .attr('material', 'color:'+color)
  enveloper.on('click', ()=>{
    d3.event.stopPropagation()
  })
  return enveloper
}
function removeInnerBubble(bubble, id){
  d3.select(document.getElementById(id)).remove()
}
function createBubbles(jsonData, lvl=0, posMin = '-10 1.6 -8', posMax = '10 2 -2'){
  var i=0
  if(jsonData == null){
    //happens when we enter in an empty bubble
    return
  }
  let classLabel = 'level'+lvl
  /*let lastLevel = false
  if(jsonData.terms != null){
    lastLevel = true
  }*/
  let obj = getArraysFromObject(jsonData)
  let proportions = getProportions(jsonData)
  console.log(proportions)
  let outers = [{data: obj['data'], keys:obj['keys'] , nested: true},
    {data: obj['termData'], keys: obj['termKeys'], nested: false}]

  //create the outer bubbles using both terms and properties
  outers.forEach(function(entry){
    let data = entry['data']
    let keys = entry['keys']
    let nested = entry['nested']
    for(let i=0; i<data.length; i++){
      const maxCategoryRad = 1
      const termRad = 1
      let outerRad = termRad
      let outKey = keys[i]
      if(nested){
        //if it is not a term compute the radius for the outer bubble
        if(typeof proportions[outKey] !== 'undefined'){
          let sumFreq = proportions[outKey]['sumFreq']
          let maxFreq = proportions['maxFreq']
          if(sumFreq > 0){
            //represent the rad proportionally to frequency
            outerRad = (sumFreq/maxFreq)*maxCategoryRad
          }else{
            //if the category is empty represent it as 10% of max size
            outerRad = maxCategoryRad/10
          }
        }

      }
      let bubble = createBubble(outKey, classLabel, nested, posMin, posMax, outerRad)
      let text = createBubbleText(bubble, outKey+'-text', outKey)
      //only for the properties generate the inner bubbles
      if(nested){
        //generate the inner bubbles following the same schema
        let innerObj = getArraysFromObject(data[i])
        let inners = [{data: innerObj['data'], keys:innerObj['keys'] , nested: true},
          {data: innerObj['termData'], keys: innerObj['termKeys'], nested: false}]
        inners.forEach(function(innerEntry){
          let innerData = innerEntry['data']
          let innerKeys = innerEntry['keys']
          for(let j=0; j<innerData.length; j++){
            let inKey = innerKeys[j]
            let maxInnerRad = outerRad/4
            //default value for inner rad
            let innerRad = maxInnerRad/2
            if(typeof proportions[outKey] !== 'undefined' && typeof proportions[outKey][inKey] !== 'undefined'){
              let sumFreqInner = proportions[outKey][inKey]['sumFreq']
              let maxFreqInner = proportions[outKey]['maxFreq']
              if(sumFreqInner > 0){
                //represent the rad proportionally to frequency
                innerRad = (sumFreqInner/maxFreqInner)*maxInnerRad
              }
            }
            let innerBubble = createInnerBubble(bubble, inKey+'-inner', outerRad, innerRad)
          }//fine for su inners
        })
      }else{
        //if it is a term generate the level bar
        let frequency = data[i].freq
        let levelBar = createLevelBar(bubble, outKey+'-levelBar', frequency)
      }//fine if su nested
    }//fine for su outers
  })

  //


}

function createBubbles_old(jsonData, lvl=0, posMin = '-10 1.6 -8', posMax = '10 2 -2'){
  var i=0
  if(jsonData == null){
    //happens when we enter in an empty bubble
    return
  }
  let classLabel = 'level'+lvl
  /*let lastLevel = false
  if(jsonData.terms != null){
    lastLevel = true
  }*/
  let obj = getArraysFromObject(jsonData)

  let outers = [{data: obj['data'], keys:obj['keys'] , nested: true},
    {data: obj['termData'], keys: obj['termKeys'], nested: false}]

  //create the outer bubbles using both terms and properties
  outers.forEach(function(entry){
    let data = entry['data']
    let keys = entry['keys']
    let nested = entry['nested']
    for(let i=0; i<data.length; i++){
      const outerRad = 1
      let outKey = keys[i]
      let bubble = createBubble(outKey, classLabel, nested, posMin, posMax, outerRad)
      let text = createBubbleText(bubble, outKey+'-text', outKey)
      //only for the properties generate the inner bubbles
      if(nested){
        //generate the inner bubbles following the same schema
        let innerObj = getArraysFromObject(data[i])
        let inners = [{data: innerObj['data'], keys:innerObj['keys'] , nested: true},
          {data: innerObj['termData'], keys: innerObj['termKeys'], nested: false}]
        inners.forEach(function(innerEntry){
          let innerData = innerEntry['data']
          let innerKeys = innerEntry['keys']
          for(let j=0; j<innerData.length; j++){
            let inKey = innerKeys[j]
            let innerBubble = createInnerBubble(bubble, inKey+'-inner', outerRad)
          }//fine for su inners
        })
      }else{
        //if it is a term generate the level bar
        let frequency = data[i].freq
        let levelBar = createLevelBar(bubble, outKey+'-levelBar', frequency)
      }//fine if su nested
    }//fine for su outers
  })

  //


}

//receive the current stack and return the previous lvl of json
function getPrevLvlData(json, stack){
  //if stack is empty prev is  null
  let prev = null
  //if stack has only one element prev is json. In any case initialize prev
  if(stack.length >= 1)
    prev = json
  //enter for only if length > 1
  for(let i=0; i<(stack.length-1); i++){
    let id = stack[i].id
    prev = prev[id]

  }
  return prev
}

//EVENT LISTENER paint-on-mouseenter is attached directly to the html element
AFRAME.registerComponent('paint-on-mouseenter', {
  schema: {
    //il parametro da passare
    to: {default: '2.5 2.5 2.5'},
    color: {default: 'red'}
  },
  //la funzione che viene eseguita
  //mouseenter mouseleave
  //hover-start hover-end
  init: function () {
    var data = this.data;
    var originalColor = this.el.getAttribute('color')
    var originalScale = this.el.getAttribute('scale')
    var originalOpacity = this.el.getAttribute('material').opacity
    var scaleString = originalScale.x + " " + originalScale.y + " " + originalScale.z
    this.el.addEventListener('mouseenter', function () {
      //read again the color because it can change
      originalColor = this.getAttribute('color')
      console.log('mouseenter! Original color was: ' + originalColor)
      //console.log(this.is('cursor-hovered'))
      this.setAttribute('color', data.color);
      this.setAttribute('scale', data.to)
      this.setAttribute('material', 'opacity: 0.5')
    });
    this.el.addEventListener('mouseleave', function () {
      //console.log('mouseleave! Original color was: ' + originalColor)
      //console.log(this.is('cursor-hovered'))
      this.setAttribute('color', originalColor);
      this.setAttribute('scale', scaleString)
      //console.log("originalOpacity: " + originalOpacity)
      this.setAttribute('material', 'opacity: ' + originalOpacity)
    });
  }
});

AFRAME.registerComponent('scale-on-mouseenter', {
  schema: {
    //il parametro da passare
    to: {default: '2.5 2.5 2.5'},
    color: {default: 'red'}
  },
  //la funzione che viene eseguita
  //mouseenter mouseleave
  //hover-start hover-end
  init: function () {
    var data = this.data;
    var originalColor = this.el.getAttribute('color')
    var originalScale = this.el.getAttribute('scale')
    var originalOpacity = this.el.getAttribute('material').opacity
    var scaleString = originalScale.x + " " + originalScale.y + " " + originalScale.z
    this.el.addEventListener('mouseenter', function () {
      //read again the color because it can change
      //originalColor = this.getAttribute('color')
      //console.log('mouseenter! Original color was: ' + originalColor)
      //console.log(this.is('cursor-hovered'))
      //this.setAttribute('color', data.color);
      this.setAttribute('scale', data.to)
      this.setAttribute('material', 'opacity: 0.5')
    });
    this.el.addEventListener('mouseleave', function () {
      //console.log('mouseleave! Original color was: ' + originalColor)
      //console.log(this.is('cursor-hovered'))
      //this.setAttribute('color', originalColor);
      this.setAttribute('scale', scaleString)
      //console.log("originalOpacity: " + originalOpacity)
      this.setAttribute('material', 'opacity: ' + originalOpacity)
    });
  }
});


AFRAME.registerComponent("random-position",{
  schema:{
    min:{default:{x:-10,y:-10,z:-10},type:"vec3"},
    max:{default:{x:10,y:10,z:10},type:"vec3"},
    away:{default: false}
  },
  update:function(){
    var t=this.data,a=t.max,e=t.min;
    //
    let randX = (Math.random()*(a.x-e.x)+e.x).toFixed(2);
    let randY = (Math.random()*(a.y-e.y)+e.y).toFixed(2);
    let randZ = (Math.random()*(a.z-e.z)+e.z).toFixed(2);

    //console.log(t.away)
    //console.log(randZ)
    /*if(t.away){
      do{
        randZ = (Math.random()*(a.z-e.z)+e.z).toFixed(2);
      }
      while(Math.abs(randZ) < 2)
    }*/

    //
    this.el.setAttribute("position",{
      x:randX,
      y:randY,
      z:randZ
    }
    )
  }
});

AFRAME.registerComponent("bouncing",{
  schema:{
    amplitude:{default:0.2,type:"float"}
  },
  init: function(){
    var data=this.data
    var offset = data.amplitude/2
    var pos =this.el.getAttribute("position")
    var from= {x:pos.x, y:pos.y-offset, z:pos.z}
    var to = {x:pos.x, y:pos.y+offset, z:pos.z}
    var animation = {
      property: 'position',
      from: from,
      to: to,
      dir: 'alternate',
      loop: true,
      easing: 'easeInOutQuad',
      enabled: true
    }
    this.el.setAttribute("animation__bouncing",animation)
    //update animation when entity is moved
    /*this.el.addEventListener('componentchanged', function (evt) {
      if (evt.detail.name === 'position') {
        var el = evt.target
        console.log('Entity has moved to', el.getAttribute('position'), '!');
        console.log(animation)
        pos=el.getAttribute("position")
        animation.from ={x:pos.x, y:pos.y-offset, z:pos.z}
        animation.to = {x:pos.x, y:pos.y+offset, z:pos.z}
        el.setAttribute("animation__bouncing",animation)
      }
    }) */

  },
  remove:function(){
    //this.el.setAttribute('animation__bouncing', 'enabled', false)
    this.el.removeAttribute('animation__bouncing')
    //var pos = this.el.getAttribute("position")
    //this.el.setAttribute('position', {x:pos.x, y:pos.y + this.offset, z:pos.z })
    /*var pos = this.el.getAttribute('position')
    if(pos.y < 2)
      this.el.setAttribute('position', {x:pos.x, y:2, z:pos.z})*/
  }
});
AFRAME.registerComponent('stop-bouncing-when-grabbed', {
  init: function(){
    var el = this.el
    el.addEventListener('grab-start', function(){
      el.removeAttribute('bouncing')
    })
    el.addEventListener('grab-end', function(){
      el.setAttribute('bouncing')
    })
  }
})

AFRAME.registerComponent('prova-video', {
  multiple: true,
  schema: {
    event: {type: 'string', default: ''},
    message: {type: 'string', default: 'Hello, World!'}
  },
  init: function () {
    const data = this.data
    this.el.addEventListener('click',(e)=>{
      let video = document.getElementById('test-video')
      console.log(video)
      video.play()
    })

  }
});
AFRAME.registerComponent('test-component', {
  multiple: true,
  schema: {
    event: {type: 'string', default: ''},
    message: {type: 'string', default: 'Hello, World!'}
  },
  init: function () {
    const data = this.data
    if(data.event){
        this.el.addEventListener(data.event, function(){
            console.log(data.message)
        })
    }

  }
});
function simulateMouseEvent(el, mouseEvent) {
  var event = new MouseEvent(mouseEvent, {
    'view': window,
    'bubbles': true,
    'cancelable': true
  });
  var canceled = !el.dispatchEvent(event);
}
AFRAME.registerComponent('map-events', {
  schema: {
    fromEvts: {type: 'array', default: ['hover-start', 'hover-end']},
    toEvts: {type: 'string', default: ['mouseenter', 'mouseleave']}
  },
  init: function () {
    const data = this.data
    const fromEvts = data.fromEvts
    const toEvts = data.toEvts
    let el = this.el
    let id = el.getAttribute('id')
    if(fromEvts.length != toEvts.length){
      console.error(el, 'Number of events to be mapped must be the same')
      return
    }
    fromEvts.forEach((fromEvt, i) =>{
      el.addEventListener(fromEvt, (e)=>{
        //let's simulate the corresponding mapped mouse event
        simulateMouseEvent(el, toEvts[i])
      })
    })

  }
})
AFRAME.registerComponent('signal-intersection', {
  init: function () {
    let el = this.el
    //use a global variable to avoid conflicts with raycaster
    el.addEventListener('raycaster-intersected', (e)=>{
      buttonIntersected = true
    })
    el.addEventListener('raycaster-intersected-cleared', (e)=>{
      buttonIntersected = false
    })
  }
})

AFRAME.registerComponent('test-button', {

  init: function () {
    const button = this.el.querySelector('#button')

    this.el.addEventListener('click', function(){
        console.log('clicked')
        let toggle = !button.getAttribute('visible')
        button.setAttribute('visible', toggle)
    })


  }
});

AFRAME.registerComponent('button-component', {

  init: function () {

    this.el.addEventListener('click', function(evt){
        console.log('plane clicked!')
        evt.stopPropagation();
    })


  }
});

AFRAME.registerComponent('event-info-component', {
  schema: {
    event: {type: 'string', default: ''},
  },
  init: function () {
    const data = this.data
    if(data.event){
        this.el.addEventListener(data.event, function(evt){
            console.log(evt)
        })
    }

  }
});



AFRAME.registerComponent('grab-test', {
  schema: {
    event: {type: 'string', default: ''},
  },
  init: function () {
    var el = this.el
    el.addEventListener('grab-start', function(evt){
      console.log('GRAB START:')
      console.log(el.getAttribute('id'))
      //console.log('grab:', el.is('grabbed'))

    })

    el.addEventListener('grab-end', function(evt){
      console.log('GRAB END:')
      console.log(el.getAttribute('id'))
      //console.log('grab:', el.is('grabbed'))
    })

    el.addEventListener('stateadded', function(evt){
      console.log('state-added', evt.detail)
      console.log(el.getAttribute('id'))
    })

    el.addEventListener('stateremoved', function(evt){
      console.log('state-removed', evt.detail)
      console.log(el.getAttribute('id'))
    })

  }
});

function getSibilingsIds(bubble, start=0, number=null){
  let id = bubble.getAttribute('id')
  let classes = bubble.classList
  let lvlLabel = null
  let sibilingsIds = []
  for(let i=0; i< classes.length; i++){
    if(classes.item(i).startsWith('level')){
      lvlLabel = classes.item(i)
      break
    }
  }
  if(lvlLabel == null){
    return null
  }
  //let sibilings = document.querySelectorAll('.'+lvlLabel)
  //BUBBLES CAN NOT BE PUSHED INTO SINGLE TERMS
  let sibilings = document.getElementsByClassName(lvlLabel + ' nested');
  for(let i=0; i<sibilings.length; i++){
    let currId = sibilings[i].getAttribute('id')
    if(currId != id){
      sibilingsIds.push(currId)
    }
  }
  if(start == 0 && number == null){
    return sibilingsIds.sort()
  }
  //let's sort the ids and use slice function to return only the requested portion
  return sibilingsIds.sort().slice(start, number==null ? null: start+number)
}
function loadListArrowHandler(){
  let leftArrow = true
  let rightArrow = true
  let menu = d3.select(this.parentNode)
  let menuId = menu.attr('id')
  let visible = menu.attr('visible')
  if(!visible){
    return
  }
  d3.event.stopPropagation()
  let button = d3.event.target.parentNode
  //raycaster sends event to arrow,
  //while mouse sends it to an inner element
  //so with raycaster the previous assignment
  //will be the menu (container)
  if(d3.select(button).classed('wristMenu')){
    button = d3.event.target
  }
  let controller = button.parentNode.parentNode
  console.log('button', button)
  console.log('controller', controller)


  //distinguish using class label
  let left = d3.select(button).classed('left')
  let verse = 1
  if(left){
    console.log('left')
    verse = -1
  }else{
    console.log('right')
  }
  let options = []
  let icons = []
  //compute the new starting index
  let newStart = loadMenuIndex + verse*loadMenuSize
  //if we are trying to go before page 0
  if(newStart < 0){
    return
  }
  //if we are back in page 0 left arrow is not necessary
  if(newStart == 0){
    leftArrow = false
  }
  function loadArrowCallback(detail){
    //remove listener from socket so that the same callback won't be called more than once
    socket.removeListener('file list', loadArrowCallback)
    //get all files in the folder
    let filenames = detail.filenames

    if(newStart >= filenames.length){
      return
    }
    options = filenames.sort().slice(newStart, newStart+loadMenuSize)
    if(options.length == 0){
      return
    }
    //if this is the last page rightArrow is not necessary
    if(newStart + options.length == filenames.length){
      rightArrow  = false
    }
    //update global variable
    loadMenuIndex = newStart
    //fill the icons array
    for(let i=0; i<options.length; i++){
      icons.push('disc')
    }
    //hide previous menu and push it a bit back to avoid click conflicts
    menu.attr('visible', false)
    .attr('position', wristMenuHidingPos)
    //create a new inner menu not hidden with arrows;
    //id must remain the same in this case
    newMenu = createWristMenu(controller, menuId, options, icons, loadButton, false, loadListArrowHandler, leftArrow, rightArrow)
    //mark the menu as an innerMenu
    newMenu.classed('innerMenu', true)
    //just click on inner menu to go back to previous menu
    newMenu.on('click', hideInnerWristMenus)
  }
  //request file list to server
  socket.emit('list request')
  //when we receive the answer
  socket.on('file list', loadArrowCallback)
}
function sibilingsListArrowHandler(){
  let leftArrow = true
  let rightArrow = true
  let menu = d3.select(this.parentNode)
  let visible = menu.attr('visible')
  if(!visible){
    return
  }
  d3.event.stopPropagation()
  let button = d3.event.target.parentNode
  //raycaster sends event to arrow,
  //while mouse sends it to an inner element
  //so with raycaster the previous assignment
  //will be the menu (container)
  if(d3.select(button).classed('menu')){
    button = d3.event.target
  }
  let bubble = button.parentNode.parentNode
  let bubbleId = bubble.getAttribute('id')
  //distinguish using class label
  let left = d3.select(button).classed('left')
  let verse = 1
  if(left){
    console.log('left')
    verse = -1
  }else{
    console.log('right')
  }
  //WORK ONLY WITH PUSH INTO LOGIC
  //compute the new starting index
  let newStart = menuMap[bubbleId] + verse*menuSize
  //if we are trying to go back in page 0
  if(newStart < 0){
    return
  }
  //if we go back to page 0 then left arrow is not necessary
  if(newStart == 0){
    leftArrow = false
  }


  let options = getSibilingsIds(bubble, newStart, menuSize)
  //if nothing is returned
  if(options.length == 0){
    return
  }
  options.forEach(function(element){console.log(element)})
  let icons = []
  for(let i=0; i<options.length; i++){
    icons.push('android-locate')
  }

  let allSibilings = getSibilingsIds(bubble)
  //hide right arrow if this is the last page
  if(newStart + options.length == allSibilings.length ){
    rightArrow = false
  }
  //update global variable
  menuMap[bubbleId] = newStart
  //create a new inner menu not hidden with arrows
  newMenu = createMenu(bubble, options, icons, pushIntoButton, false, true, sibilingsListArrowHandler, leftArrow, rightArrow)
  //remove previous menu
  menu.attr('visible', false)
  menu.attr('position', '0 0 0')
  //setTimeout((e)=>{menu.remove()}, menuDelay)
}

function pushIntoButton(data, index){
  d3.event.stopPropagation();
  //d3.select(this.parentNode.parentNode).classed('holdable', false)
  //d3.select(this).classed('holdable', false)
  //this.removeState('grabbed')
  //this.emit('grab-end', {})
  //this.removeState('hovered')
  //this.emit('hover-end', {})
  console.log(data)
  let scene = document.querySelector('a-scene')
  let menu = d3.select(this.parentNode)
  let bubble = this.parentNode.parentNode
  //let target = document.querySelector('#'+data)
  let target = document.getElementById(data)
  if(target == null){
    return
  }
  scene.emit('push-into', {requestingEntity: bubble, targetEntity: target})
  //hide push into menu
  menu.attr('visible', false)
  menu.attr('position', '0 0 0')
  //setTimeout((e)=>{menu.remove()}, menuDelay)
}
function deleteButton(data, index){
  const conf = 'Confirm'
  d3.event.stopPropagation();
  console.log(data)
  let scene = document.querySelector('a-scene')
  let menu = d3.select(this.parentNode)
  let bubble = this.parentNode.parentNode
  if(data == conf){
    scene.emit('delete', {requestingEntity: bubble})
  }
  //hide delete menu
  menu.attr('visible', false)
  menu.attr('position', '0 0 0')
  //setTimeout((e)=>{menu.remove()}, menuDelay)

}
function editNameButton(data, index){
  d3.event.stopPropagation();
  console.log(data)
  let newName = data
  let menu = d3.select(this.parentNode)
  let scene = document.querySelector('a-scene')
  let bubble = this.parentNode.parentNode
  //if there is already a bubble with that name at the same level or at the next return
  let another = document.getElementById(newName)
  let inner = document.getElementById(newName+'-inner')
  let parent = inner ? inner.parentNode : null
  if(another || (inner && parent == bubble)){
    printLog(duplicateNameMsg)
    return
  }
  waitingForName = false
  editingName = false
  scene.emit('edit-name', {requestingEntity: bubble, newName: newName})
  //delete name selection menu
  menu.attr('visible', false)
  menu.attr('position', '0 0 0')
  //setTimeout((e)=>{menu.remove()}, menuDelay)
}
function editFrequencyButton(data, index){
  d3.event.stopPropagation();
  console.log(data)
  let newFreq = Number(data)
  let menu = d3.select(this.parentNode)
  let scene = document.querySelector('a-scene')
  let bubble = this.parentNode.parentNode
  //if the button is not a valid number return
  if(isNaN(newFreq)){
    return
  }
  //if try to change frequency in a nested bubble return
  if(d3.select(bubble).classed('nested')){
    return
  }
  //set the global variables to false again
  waitingForName = false
  editingFrequency = false
  scene.emit('edit-frequency', {requestingEntity: bubble, newFrequency: newFreq})
  //delete name selection menu
  menu.attr('visible', false)
  menu.attr('position', '0 0 0')
  //setTimeout((e)=>{menu.remove()}, menuDelay)
}
function confirmNameButton(data, index){
  d3.event.stopPropagation();
  console.log(data)

  let newName = data
  let menu = d3.select(this.parentNode)
  let scene = document.querySelector('a-scene')
  let bubble = this.parentNode.parentNode
  let isNested = d3.select(bubble).classed('nested')
  //if there is already a bubble with that name at the same level or at the next return
  let another = document.getElementById(newName)
  let inner = document.getElementById(newName+'-inner')
  let parent = inner ? inner.parentNode : null
  if(another || (inner && parent == bubble)){
    printLog(duplicateNameMsg)
    return
  }
  waitingForName = false
  let options = ['Child category', 'Child term', 'Sibiling category', 'Sibiling term']
  let icons = ['android-add', 'android-add', 'android-add-circle', 'android-add-circle']
  if(!isNested){
    options = ['Sibiling category', 'Sibiling term']
    icons = ['android-add-circle', 'android-add-circle']
  }
  newMenu = createMenu(bubble, options, icons, function(d,i){
    d3.event.stopPropagation();
    console.log(d)
    console.log(data)
    let oldMenu = d3.select(this.parentNode)
    let category = true
    let child = true
    //based on order in options array
    //child term or sibiling term
    if(i == 1 || i== 3){
      category = false
    }
    //sibiling term or sibiling category or if the father is a term
    if(i == 2 || i == 3 || !isNested){
      child = false
    }
    if(child){
      scene.emit('add-child', {requestingEntity: bubble, newName: newName, category: category})
    }else{
      scene.emit('add-sibiling', {newName: newName, category: category})
    }
    oldMenu.attr('visible', false)
    oldMenu.attr('position', '0 0 0')
    //setTimeout((e)=>{oldMenu.remove()}, menuDelay)
  }, false, true)
  //delete name selection menu
  menu.attr('visible', false)
  menu.attr('position', '0 0 0')
  //setTimeout((e)=>{menu.remove()}, menuDelay)

}

function editButton(data, index){
  d3.event.stopPropagation();
  console.log(data)
  let menu = d3.select(this.parentNode)
  let bubble = this.parentNode.parentNode
  switch(data){
    case 'Edit name':
      waitingForName = true
      editingName = true
      //default name
      let newName = 'newName'+newNameCnt
      if(document.getElementById(newName) != null || document.getElementById(newName+'-inner') != null){
        newName = 'newName'+(++newNameCnt)
      }
      options = [newName]
      icons = ['edit']
      printLog(newNameMsg)
      //create a new inner menu not hidden
      newMenu = createMenu(bubble, options, icons, editNameButton, false, true)
      newMenu.classed('editNameMenu', true)
      //remove old menu
      menu.attr('visible', false)
      menu.attr('position', '0 0 0')
      //setTimeout((e)=>{menu.remove()}, menuDelay)
      break
    case 'Edit frequency':
      waitingForName = true
      editingFrequency = true
      //default frequency
      newFreq = '0'
      options = [newFreq]
      icons = ['edit']
      printLog(newFreqMsg)
      //create a new inner menu not hidden
      newMenu = createMenu(bubble, options, icons, editFrequencyButton, false, true)
      newMenu.classed('editFrequencyMenu', true)
      //remove old menu
      menu.attr('visible', false)
      menu.attr('position', '0 0 0')
      //setTimeout((e)=>{menu.remove()}, menuDelay)
      break
  }
}
function confirmSaveButton(data, index){
  d3.event.stopPropagation()
  let scene = document.querySelector('a-scene')
  let menu = d3.select(this.parentNode)
  let controller = this.parentNode.parentNode
  //update the global variable holding the currFilename
  currFilename = data
  scene.emit('save-file', {name: currFilename});
  waitingForSave = false
  printFilename()
  //hide inner menus and restore original wrist menu
  d3.select(controller).selectAll('.wristMenu')
  .each(function(d){
    let el = d3.select(this)
    let innerMenu = el.classed('innerMenu')
    if(innerMenu){
      el.attr('visible', false)
      .attr('position', wristMenuHidingPos)
    }else{
      el.attr('visible', true)
      .attr('position', wristMenuOriginalPos)
    }
  })
}
function loadButton(data, index){
  d3.event.stopPropagation();
  console.log(data)
  let scene = document.querySelector('a-scene')
  let menu = d3.select(this.parentNode)
  let controller = this.parentNode.parentNode
  let fileToLoad = data
  scene.emit('load-file', {name: fileToLoad})
  //no need to hide menu since the page will reload
}
function deleteButton(data, index){
  const conf = 'Confirm'
  d3.event.stopPropagation();
  console.log(data)
  let scene = document.querySelector('a-scene')
  let menu = d3.select(this.parentNode)
  let bubble = this.parentNode.parentNode
  if(data == conf){
    scene.emit('delete', {requestingEntity: bubble})
  }
  //hide delete menu
  menu.attr('visible', false)
  menu.attr('position', '0 0 0')
  //setTimeout((e)=>{menu.remove()}, menuDelay)

}
function hideInnerWristMenus(){
  let menu = d3.select(d3.event.target)
  let controller = d3.event.target.parentNode
  d3.select(controller).selectAll('.wristMenu').each(function(d){
    let wristMenu = d3.select(this)
    let menuPos = wristMenu.attr('position')
    let inner = wristMenu.classed('innerMenu')
    if(inner){
      //hide so that it will be deleted by the cleaner
      wristMenu.attr('visible', false)
      .attr('position', wristMenuHidingPos)
    }else{
      //if it's the original menu restore its original state
      wristMenu.attr('visible', true)
      .attr('position', wristMenuOriginalPos)
    }
  })

}

function updateWristMenu(stack){
  const zeroLevel = 'Base map'
  const icon = 'android-exit'
  const menuId = 'right-wrist-menu'
  let currMenu = document.getElementById(menuId)
  if(currMenu == null){
    //should never happen
    return
  }
  let controller = currMenu.parentNode
  let options = [zeroLevel]
  let icons = [icon]
  stack.forEach((element)=>{
    options.push(element.id)
    icons.push(icon)
  })
  d3.select(currMenu).remove()
  let newMenu = createWristMenu(controller, menuId, options, icons, rightWristMenuButtonHandler)
  //set global variable to false since there is a new right wrist menu
  rightWristMenuOpen = false
}
function leftWristMenuButtonHandler(data, index){
  d3.event.stopPropagation()
  let menu = d3.select(this.parentNode)
  let menuId = menu.attr('id')
  let controller = this.parentNode.parentNode
  let scene = document.querySelector('a-scene')
  let visible = menu.attr('visible')
  if(!visible){
    return
  }
  let options = []
  let icons = []
  let newMenu = null
  switch(data){
    case 'Load':
      function loadCallback(detail){
        //remove listener from socket so that the same callback won't be called more than once
        socket.removeListener('file list', loadCallback);
        //get all files in the folder
        let filenames = detail.filenames
        loadMenuIndex = 0
        options = filenames.sort().slice(loadMenuIndex, loadMenuIndex+loadMenuSize)
        if(options.length == 0){
          printLog(nothingToLoadMsg)
          return
        }
        //fill the icons array
        for(let i=0; i<options.length; i++){
          icons.push('disc')
        }
        let arrowCallback = loadListArrowHandler
        if(filenames.length <= loadMenuSize){
          arrowCallback = null
        }
        let menuPos = menu.attr('position')
        //hide main menu and push it a bit back to avoid click conflicts
        menu.attr('visible', false)
        .attr('position', wristMenuHidingPos)
        //create a new inner menu not hidden just with right arrow (start page);
        newMenu = createWristMenu(controller, menuId+'-load', options, icons, loadButton, false, arrowCallback, false, true)
        //mark the menu as an innerMenu
        newMenu.classed('innerMenu', true)
        //just click on inner menu to go back to previous menu
        newMenu.on('click', hideInnerWristMenus)
      }
      //request file list to server
      socket.emit('list request')
      //when we receive the answer
      socket.on('file list', loadCallback)
      break
    case 'Save':
      console.log(data)
      if(currFilename != null){
        printLog('Saved changes on \''+ currFilename +'\'')
        scene.emit('save-file', {name: currFilename});
        break
      }
      //otherwise if a name has not been defined
      //the code will keep executing below and
      //it will enter the 'Save as' case

    case 'Save as':
      //in order to allow editing by speech
      waitingForSave = true
      //default name
      let savename = 'newSave.json'
      if(newSaveCnt > 0){
        savename = 'newSave'+newSaveCnt+'.json'
      }
      function saveCallback(detail){
        //remove listener from socket so that the same callback won't be called more than once
        socket.removeListener('file list', saveCallback)
        //get all files in the folder
        let filenames = detail.filenames
        filenames.sort().forEach(function(file){
          if(file == savename){
            savename = 'newSave'+(++newSaveCnt)+'.json'
          }
        })
        options = [savename]
        icons = ['edit']
        printLog(newSaveMsg)
        //create a new wrist menu not hidden
        newMenu = createWristMenu(controller, menuId+'-saveAs', options, icons, confirmSaveButton, false)
        //mark the menu so it can be accessed outside the callback
        newMenu.classed('newSaveMenu', true)
        //mark the menu as an innerMenu
        .classed('innerMenu', true)
        //just click on inner menu to go back to previous menu
        .on('click', hideInnerWristMenus)
        //remove old menu
        menu.attr('visible', false)
        .attr('position', wristMenuHidingPos)
      }
      //request file list to server
      socket.emit('list request')
      //when we receive the answer
      socket.on('file list', saveCallback)
      break
    case 'New':
      console.log(data)
      //just reload the page with the base address
      window.location.replace(window.location.pathname)
    default:
      break
  }
}

function rightWristMenuButtonHandler(data, index){
  const dur = 500
  const pos = {x: 0, y: 0, z: 0}
  let menu = d3.select(this.parentNode)
  let controller = this.parentNode.parentNode
  let scene = document.querySelector('a-scene')
  let camera = document.querySelector('#camera');
  let cameraRig = document.querySelector('#cameraRig');
  let visible = menu.attr('visible')
  if(!visible){
    return
  }

  if(currStack.length < 1 || index == currStack.length){
    printLog(currLvlMsg)
    return
  }
  //let's prevent event to fire too often
  if(eventLocker)
    return;
  eventLocker = true;
  setTimeout(function(){
    eventLocker = false;
  }, blinkEventLockerTime)
  var fadeAnim = {
    property: 'material.opacity',
    from: 0,
    to: 1,
    dur: data.dur,
    easing: 'easeOutCubic'
  }
  // CREATE A TRANSPARENT BLACK IMAGE
  var blink = document.createElement('a-image');
  blink.setAttribute('material', {
    color: '#000000',
    opacity: 0
  });
  // SET THE BLACK IMAGE POSITION AND APPEND IT AS CAMERA'S CHILD ENTITY
  blink.object3D.position.z = -0.1;
  camera.appendChild(blink);
  blink.setAttribute('visible', true)
  blink.setAttribute('animation', fadeAnim);
  // WHEN FADE-IN ANIMATION COMPLETES, MOVE THE CAMERA RIG TO DESTINATION
  setTimeout(function () {
    cameraRig.setAttribute('position', pos);
    camera.setAttribute('position', {x: pos.x, y: pos.y+1.6, z: pos.z})
    //EMIT ENTER EVENT TO SCENE
    scene.emit('change-level', {newLevelId: data, stackIndex: index-1, entityToSignal: controller});
    // EMIT CUSTOM EVENT TO TRIGGER THE FADE-OUT ANIMATION
    //controller.emit('position-changed');

  }, dur);
  // ON CUSTOM EVENT, ANIMATE THE BLACK IMAGE (FADE-OUT)
  //in this case the event will be emitted from the scene in the
  //change-level handler
  controller.addEventListener('position-changed', function () {
    blink.setAttribute('animation', {
      to: 0
    });
    blink.setAttribute('visible', false)
    d3.select(blink).remove()
  });

}
function menuButtonHandler(data, index){
  let menu = d3.select(this.parentNode)
  let scene = document.querySelector('a-scene')
  let visible = menu.attr('visible')
  if(!visible){
    return
  }
  d3.event.stopPropagation()
  let pressed = this
  let bubble = this.parentNode.parentNode
  let bubbleId = bubble.getAttribute('id')
  let options = []
  let icons = []
  //console.log(this)
  switch(data){
    case 'Add':
      //hide main menu
      menu.attr('visible', false)
      .attr('position', '0 0 0')
      //nameCnt
      //nameListener = true
      waitingForName = true
      newName = 'newName'+newNameCnt
      if(document.getElementById(newName) != null || document.getElementById(newName+'-inner') != null){
        newName = 'newName'+(++newNameCnt)
      }
      options = [newName]
      icons = ['android-add']
      printLog(addNameMsg)
      //create a new inner menu not hidden
      newMenu = createMenu(bubble, options, icons, confirmNameButton, false, true)
      newMenu.classed('newNameMenu', true)
      break
    case 'Push into':

      if( menuMap[bubbleId] == 'undefined' ) {
        //initialize the global variables
        menuMap[bubbleId] = 0
      }
      options = getSibilingsIds(bubble, menuMap[bubbleId], menuSize)

      if(options.length == 0){
        //there is nothing in which you can push into
        printLog(noPushIntoMsg)
        return
      }
      //set global var to true
      requestingPushInto = bubble
      waitingForPushInto = true
      //print instruction
      printLog(pushIntoMsg)
      let arrowCallback = sibilingsListArrowHandler
      let allSibilings = getSibilingsIds(bubble)
      //hide arrows if the are no options enough
      if(allSibilings.length <= menuSize){
        arrowCallback = null
      }
      //hide main menu
      menu.attr('visible', false)
      .attr('position', '0 0 0')
      //fill the icons array
      for(let i=0; i<options.length; i++){
        icons.push('android-locate')
      }
      //create a new inner menu not hidden ust with right arrow: start of menu;
      //function(d, i) is the button handler
      newMenu = createMenu(bubble, options, icons, pushIntoButton, false, true, arrowCallback, false, true)
      //console.log('PROVA RIUSCITA')
      break
    case 'Push out':
      let lowestLvl = d3.select(bubble).classed('level0')
      if(lowestLvl){
        //we are at the lowest level
        printLog(noPushOutMsg)
        return
      }
      //hide main menu
      menu.attr('visible', false)
      .attr('position', '0 0 0')
      scene.emit('push-out', {requestingEntity: bubble})
      break
    case 'Edit':
      //hide main menu
      menu.attr('visible', false)
      .attr('position', '0 0 0')
      let nested = d3.select(bubble).classed('nested')
      if(nested){
        options= ['Edit name']
        icons = ['clipboard']
      }else{
        options= ['Edit name', 'Edit frequency']
        icons = ['clipboard', 'clipboard']
      }
      //create a new inner menu not hidden
      newMenu = createMenu(bubble, options, icons, editButton, false, true)
      break
    case 'Delete':
      //hide main menu
      menu.attr('visible', false)
      .attr('position', '0 0 0')
      options= ['Confirm', 'Cancel']
      icons = ['checkmark', 'close']
      //create a new inner menu not hidden
      newMenu = createMenu(bubble, options, icons, deleteButton, false, true)
      break
    default:
      break
  }

}
//TODO: set hidden to true by default
function createWristMenu(el, id, options, icons, buttonCallback, hidden=true, arrowCallback=null, leftArrow = true, rightArrow = true){
  if(options == null || options.constructor != Array){
    return null
  }
  let num = options.length
  let positions = []
  let pos = hidden ? wristMenuHidingPos : wristMenuOriginalPos
  const rot ="-20 0 0"
  const scale = '0.5 0.5 0.5'
  const menuHeight = 2.2
  const menuWidth = 1.5
  const buttonHeight = 0.2
  const buttonWidth = 1
  const buttonOffset = 0.1
  const zCoord = 0.1
  const arrowXOffset = 0.75
  const arrowYOffset = 0

  const leftArrPos = {x: -arrowXOffset, y: arrowYOffset , z: zCoord}
  const rightArrPos = {x: arrowXOffset, y: -arrowYOffset, z: zCoord}
  let currentY = menuHeight/2 - buttonHeight/2 - buttonOffset
  for(let i=0; i<num; i++){
    let yCoord = currentY
    //let's subtract in order to go down
    currentY -= (buttonHeight+4*buttonOffset)
    //storing the point's coordinates
    positions.push({x: 0, y: yCoord, z: zCoord});
  }
  let menu = d3.select(el)
  .append('a-entity')
  .classed('wristMenu', true)
  .attr('id', id)
  .attr('position', pos)
  .attr('rotation', rot)
  .attr('scale', scale)
  .attr('visible', !hidden)
  .attr('geometry', 'primitive:plane; width: '+ menuWidth +'; height: '+ menuHeight)
  .attr('material', 'color:#22252a; opacity: 0.7')
  //generate left arrow only if necessary
  if(arrowCallback != null && leftArrow){
    let leftArr = menu.append('a-gui-icon-label-button')
    .attr('id', id+'-menu-left')
    .classed('left', true)
    .classed('arrow', true)
    .attr('height', '0.6')
    .attr('width', '0.2')
    .attr('icon-font-size', '600px')
    .attr('icon', 'arrow-left-b')
    .attr('margin', '0 0 0.05 0')
    .attr('hover-color', '#ed5b21')
    .attr('position', leftArrPos)
    //wrote a component to map raycaster events on mouse events
    .attr('map-events', '')
    //wrote a component to set a global var when a button is intersected
    .attr('signal-intersection', '')
    .on('click', arrowCallback)

  }
  //options
  let buttons = menu.selectAll('.option')
  .data(options)
  .enter()
  .append('a-gui-icon-label-button')
  .classed('option', true)
  .attr('id', function(d){
    return id + '-' + d + '-menu-button';
  })
  .attr('value', function(d){return d})
  //.attr('mixin', 'menu-button')
  .attr('width', buttonWidth)
  .attr('height', buttonHeight)
  .attr('icon', function(d,i){
    if(icons == null || icons[i] == null){
      return 'alert'
    }
    return icons[i]
  })
  .attr('icon-font-size', '120px')
  .attr('font-family','Arial')
  .attr('font-size','70px')
  .attr('margin', '0 0 0.05 0')
  .attr('hover-color', '#ed5b21')
  //wrote a component to map raycaster events on mouse events
  .attr('map-events', '')
  //wrote a component to set a global var when a button is intersected
  .attr('signal-intersection', '')
  //.attr('grab-test', '')
  .attr('position', function(d,i){
    return positions[i]
  })
  .on('click', buttonCallback)
  //generate right arrow only if necessary
  if(arrowCallback != null && rightArrow){
    let rightArr = menu.append('a-gui-icon-label-button')
    .attr('id', id+'-menu-right')
    .classed('right', true)
    .classed('arrow', true)
    .attr('height', '0.6')
    .attr('width', '0.2')
    .attr('icon-font-size', '600px')
    .attr('icon', 'arrow-right-b')
    .attr('margin', '0 0 0.05 0')
    .attr('hover-color', '#ed5b21')
    .attr('position', rightArrPos)
    //wrote a component to map raycaster events on mouse events
    .attr('map-events', '')
    //wrote a component to set a global var when a button is intersected
    .attr('signal-intersection', '')
    .on('click', arrowCallback)
  }

  return menu
}
function createMenu(el, options, icons, buttonCallback, hidden=true, inner=false, arrowCallback=null, leftArrow=true, rightArrow=true){
  if(options == null || options.constructor != Array){
    return null
  }
  let num = options.length
  let positions = []
  const radius = 0.75
  const zCoord = 0.1
  let arrowOffset = 0.3
  //if there is just one arrow...
  if(!leftArrow || !rightArrow){
    //...place it at the center only if there is more than one option
    if(options.length > 1){
      arrowOffset = 0
    }
  }
  let leftArrPos = {x: 0, y: arrowOffset , z: zCoord}
  let rightArrPos = {x: 0, y: -arrowOffset, z: zCoord}
  //the angle from where we start placing icons
  let currentAngle = Math.PI/2
  //the angle offset
  const angleShift = 2*Math.PI/num
  for(let i=0; i<num; i++){
    let angle = currentAngle
    //let's subtract in order to go clockwise
    currentAngle -= angleShift
    //calculating the point's coordinates on the circle circumference
    let xCoord = radius * Math.cos(angle);
    let yCoord = radius * Math.sin(angle);
    //storing the point's coordinates
    positions.push({x: xCoord, y: yCoord, z: zCoord});
  }
  //if there is only one option put it at the center
  if(positions.length == 1){
    positions[0] = {x: 0, y:0, z: zCoord}
  }
  let id = el.getAttribute('id')
  let bubbleRadius = parseFloat(document.getElementById(id).components.geometry.attrValue.radius)
  if(bubbleRadius > 1){
    //happens if two bubbles have the same name
    bubbleRadius = 1
  }
  //scale menu in respect to bubble
  let scale = {x: bubbleRadius, y: bubbleRadius, z: bubbleRadius}
  let hiddenPos = {x: 0, y:0, z: bubbleRadius}
  let menu = d3.select(el)
  .append('a-entity')
  .classed('innerMenu', inner)
  .classed('menu', true)
  .attr('id', id+'-menu-container')
  .attr('position', hidden ?'0 0 0':hiddenPos)
  .attr('visible', !hidden)
  //.attr('geometry', 'primitive:plane; width: 1.5; height: 1.5')
  .attr('geometry', 'primitive:circle; radius:0.75')
  .attr('material', 'color:#22252a; opacity: 0.7')
  .attr('scale', scale)
  //generate left arrow only if necessary
  if(arrowCallback != null && leftArrow){
    let leftArr = menu.append('a-gui-icon-label-button')
    .attr('id', id+'-menu-left')
    .classed('left', true)
    .classed('arrow', true)
    .attr('height', '0.2')
    .attr('width', '0.2')
    .attr('icon-font-size', '400px')
    .attr('icon', 'arrow-left-b')
    .attr('margin', '0 0 0.05 0')
    .attr('hover-color', '#ed5b21')
    .attr('position', leftArrPos)
    //wrote a component to map raycaster events on mouse events
    .attr('map-events', '')
    //wrote a component to set a global var when a button is intersected
    .attr('signal-intersection', '')
    .on('click', arrowCallback)

  }
  //options
  let buttons = menu.selectAll('.option')
  .data(options)
  .enter()
  .append('a-gui-icon-label-button')
  .classed('option', true)
  .attr('id', function(d){
    return id + '-' + d + '-menu-button';
  })
  .attr('value', function(d){return d})
  //.attr('mixin', 'menu-button')
  .attr('width', '0.8')
  .attr('height','0.2')
  .attr('icon', function(d,i){
    if(icons == null || icons[i] == null){
      return 'alert'
    }
    return icons[i]
  })
  .attr('icon-font-size', '120px')
  .attr('font-family','Arial')
  .attr('font-size','70px')
  .attr('margin', '0 0 0.05 0')
  .attr('hover-color', '#ed5b21')
  //wrote a component to map raycaster events on mouse events
  .attr('map-events', '')
  //wrote a component to set a global var when a button is intersected
  .attr('signal-intersection', '')
  //.attr('grab-test', '')
  .attr('position', function(d,i){
    return positions[i]
  })
  //.attr('test-component__1', 'event:raycaster-intersected; message:INTERS')
  //.attr('test-component__2', 'event:raycaster-intersected-cleared; message: NINTERS')
  .on('click', buttonCallback)
  //.on('hover-start', function(){this.setAttribute('gui-icon-label-button', 'backgroundColor:#000000')})
  //.on('hover-end', function(){this.setAttribute('gui-icon-label-button', 'backgroundColor:#ffffff')})
  //.on('onclick', buttonCallback)
  //generate right arrow only if necessary
  if(arrowCallback != null && rightArrow){
    let rightArr = menu.append('a-gui-icon-label-button')
    .attr('id', id+'-menu-right')
    .classed('right', true)
    .classed('arrow', true)
    .attr('height', '0.2')
    .attr('width', '0.2')
    .attr('icon-font-size', '400px')
    .attr('icon', 'arrow-right-b')
    .attr('margin', '0 0 0.05 0')
    .attr('hover-color', '#ed5b21')
    .attr('position', rightArrPos)
    //wrote a component to map raycaster events on mouse events
    .attr('map-events', '')
    //wrote a component to set a global var when a button is intersected
    .attr('signal-intersection', '')
    .on('click', arrowCallback)
  }

  return menu

}
function printLog(message){
  let monitor = document.getElementById(logBoxId)
  d3.select(monitor).attr('value', message)
  setTimeout((e)=>{
    let currValue = d3.select(monitor).attr('value')
    //delete message only if it's currently written
    if(currValue == message){
      d3.select(monitor).attr('value', '')
    }
  }, logVisibilityTimeout)
}
function printFilename(){
  let monitor = document.getElementById(filenameBoxId)
  let filename = (currFilename == null) ? defaultFilename : currFilename
  d3.select(monitor).attr('value', filename)
}
function printPath(stack){
  let monitor = document.getElementById(pathBoxId)
  let pathStr = 'Base Map'
  stack.forEach((el) =>{pathStr += ('\n' + el.id)})
  d3.select(monitor).attr('value', pathStr)
}

AFRAME.registerComponent('my-menu', {
  schema: {
    event: {type: 'string', default: ''},
  },
  init: function () {
    let el = this.el
    let data = this.data
    let options = ['Add', 'Push into', 'Push out', 'Edit', 'Delete']
    let icons = ['plus', 'log-in', 'log-out', 'edit', 'android-delete']
    let menu = createMenu(el, options, icons, menuButtonHandler)
    //add the possibility to toggle menu clicking on element
    el.addEventListener('click', function(){
      let holdable = d3.select(el).classed('holdable')
      let opacity = el.getAttribute('material').opacity
      let opaque = opacity == 1
      let id = el.getAttribute('id')
      //if we click on another bubble when a push selection is waited
      if(waitingForPushInto && el != requestingPushInto){
        let scene = document.querySelector('a-scene')
        //check if the target is a term or a category
        let nested = d3.select(el).classed('nested')
        //if target:
        //1)is not a term
        //2)is not the requesting bubble
        //3)is not the bubble we are inside
        if(nested && holdable /*&& el != requestingPushInto*/){
          let innerMenu = d3.select(requestingPushInto.querySelector('.innerMenu'))
          scene.emit('push-into', {requestingEntity: requestingPushInto, targetEntity: el})
          //hide push into menu in requestingEntity
          innerMenu.attr('visible', false)
          innerMenu.attr('position', '0 0 0')

        }else{
          printLog(noPushIntoTermsMsg)
        }
        return
      }//fine selection push into
      if(leftWristMenuOpen|| rightWristMenuOpen || buttonIntersected || !holdable || opaque){
        return
      }
      let otherMenus = Array.from(document.getElementsByClassName('menu'))
      //iterate through all the other menus
      otherMenus.forEach((element)=>{
        let bubbleRadius = parseFloat(document.getElementById(id).components.geometry.attrValue.radius)
        if(bubbleRadius > 1){
          //happens if two bubbles have the same name
          bubbleRadius = 1
        }
        let hiddenPos = {x: 0, y:0, z:bubbleRadius}
        let parent = element.parentNode
        let menu = d3.select(element)
        let inner = menu.classed('innerMenu')
        if(parent == el && !inner){
          //the selected element's menu
          let toggle = menu.attr('visible')
          let newPos = toggle ? '0 0 0' : hiddenPos
          menu.attr('visible', !toggle)
          menu.attr('position', newPos)
        }else{
          //other menus
          menu.attr('position', '0 0 0')
          menu.attr('visible', false)
        }
      })
      //reset the corresponding value in menuMap
      menuMap[id] = 0
      //reset the variables for push into selection
      waitingForPushInto = false
      requestingPushInto = null

    })
  }
});

AFRAME.registerComponent('wrist-menu', {
  schema: {
    hand: {type: 'string', default: 'right'}
  },
  init: function () {
    let el = this.el
    let data = this.data
    let hand = data.hand
    let options = null
    let icons = null
    let callback = null
    let id = null
    let hidden = true
    //TODO: settare hidden a true in entrambi
    if(hand == 'right'){
      id = 'right-wrist-menu'
      options = ['Base map']
      icons = ['android-exit']
      callback = rightWristMenuButtonHandler
      //hidden = false
    }else if(hand == 'left'){
      id = 'left-wrist-menu'
      options = ['Load', 'Save as', 'Save', 'New']
      icons = ['android-folder-open', 'archive', 'archive', 'plus']
      callback = leftWristMenuButtonHandler
      //hidden = false
    }else{
      //should never happen
      return
    }

    let menu = createWristMenu(el, id, options, icons, callback, hidden)
    el.addEventListener('thumbsticktouchend', function(){
      //use another variable since the original menu could be destroyed and replaced
      let wristMenu = d3.select('#'+id)
      let toggle = wristMenu.attr('visible')
      //toggle global variables
      if(hand == 'right'){
        rightWristMenuOpen = !toggle
      }else if(hand == 'left'){
        leftWristMenuOpen = !toggle
      }
      //toggle menu visibility
      wristMenu.attr('visible', !toggle)
      wristMenu.attr('position', toggle ? wristMenuHidingPos : wristMenuOriginalPos)
      let menus = Array.from(document.getElementsByClassName('menu'))
      //hide all the other menus
      menus.forEach((m)=>{
        //console.log(m)
        if(d3.select(m).classed('innerMenu')){
          d3.select(m).remove()
        }else{
          if(d3.select(m).attr('visible')){
            d3.select(m)
            .attr('visible', false)
            .attr('position', '0 0 0')
          }
        }
      })
    })
    //fine thumbstick event listener
    //toggle menus also with keys (for PC version)
    document.addEventListener('keypress', function(event) {

    const leftMove = 'j';
    const rightMove = 'l';
    const upMove = 'i';
    const downMove = 'k';
    const inDepthMove = 'z';
    const outDepthMove = 'x';
    let key = event.key;

    if(lookAtEntity.getAttribute("id")!=outerBubbleId){

    	if(key == leftMove && lookAtEntityFlag){
      	let pos = lookAtEntity.getAttribute("position")
      	pos.x  = pos.x - 0.1
      	lastKey = key
      }else if(key == rightMove && lookAtEntityFlag){
      	let pos = lookAtEntity.getAttribute("position")
      	pos.x = pos.x + 0.1
      	lastKey = key
      }else if(key == upMove && lookAtEntityFlag){
      	let pos = lookAtEntity.getAttribute("position")
      	pos.y = pos.y + 0.1
      	lastKey = key
      }else if(key == downMove && lookAtEntityFlag){
      	let pos = lookAtEntity.getAttribute("position")
      	pos.y = pos.y - 0.1
      	lastKey = key
      }else if(key == inDepthMove && lookAtEntityFlag){
      	let pos = lookAtEntity.getAttribute("position")
      	pos.z = pos.z - 0.1
      	lastKey = key
      }else if(key == outDepthMove && lookAtEntityFlag){
      	let pos = lookAtEntity.getAttribute("position")
      	pos.z = pos.z + 0.1
      	lastKey = key
      }else{
      	lastKey = null
      }
    }
    });

    /*document.addEventListener('keyup', function(event) {
    	if(lastKey == 'j' || lastKey == 'k' || lastKey == 'l' || lastKey == 'i' || lastKey == 'z' || lastKey == 'x'){
    		lookAtEntityFlag = false;
    	}
    });*/

    document.addEventListener('keydown', function(event) {
      const toggleLeftMenuKey = 'q'
      const toggleRightMenuKey = 'e'
      
      let key = event.key
      //use another variable since the original menu could be destroyed and replaced
      let wristMenu = d3.select('#'+id)
      let toggle = wristMenu.attr('visible')
      //toggle global variable
      if(key == toggleLeftMenuKey && hand == 'left'){
        leftWristMenuOpen = !toggle
      }else if(key == toggleRightMenuKey && hand == 'right'){
        rightWristMenuOpen = !toggle
      }else{
        //in any other case does return immediately
        return
      }
      //toggle menu visibility
      wristMenu.attr('visible', !toggle)
      wristMenu.attr('position', toggle ? wristMenuHidingPos : wristMenuOriginalPos)
      let menus = Array.from(document.getElementsByClassName('menu'))
      //hide all the other menus
      menus.forEach((m)=>{
        //console.log(m)
        if(d3.select(m).classed('innerMenu')){
          d3.select(m).remove()
        }else{
          if(d3.select(m).attr('visible')){
            d3.select(m)
            .attr('visible', false)
            .attr('position', '0 0 0')
          }
        }
      })

    })
    //fine keydown event listener
  }
});

AFRAME.registerComponent('interact-with-mouse', {
    init: function() {
      	let self = this.el
        let el = this.el
      	self.addEventListener('mouseenter', function (event) {
      		lookAtEntityFlag = true;
          if(this.getAttribute("id")!=outerBubbleId)
      		  lookAtEntity = this;
		});
    }
});

AFRAME.registerComponent('pull-and-push', {
  //this component add event listeners to CONTROLLERS
  //in order to set the global variables used to move bubbles closer/further
  schema: {
    event: {type: 'string', default: ''},
  },
  init: function () {
    const data = this.data
    const pullStartEvents = ['abuttondown', 'xbuttondown']
    const pullEndEvents = ['abuttonup', 'xbuttonup']
    const pushStartEvents = ['bbuttondown', 'ybuttondown']
    const pushEndEvents = ['bbuttonup', 'ybuttonup']
    let el = this.el
    //updateMovingVariables(toPull, buttondown)
    pullStartEvents.forEach((event) => {el.addEventListener(event, (e) => {updateMovingVariables(true, true)});})
    pullEndEvents.forEach((event) => {el.addEventListener(event, (e) => {updateMovingVariables(true, false)});})
    pushStartEvents.forEach((event) => {el.addEventListener(event, (e) => {updateMovingVariables(false, true)});})
    pushEndEvents.forEach((event) => {el.addEventListener(event, (e) => {updateMovingVariables(false, false)});})
  }
});

AFRAME.registerComponent('moveable', {
  schema: {
    speed: {
      type: 'number',
      default: 2
    }
  },
  init: function() {
    this.target = this.el.sceneEl.querySelector('#cameraRig');
  },
  tick: function(t, dt) {
    var pullCondition = pull && this.el.is('grabbed')
    var pushCondition = push && this.el.is('grabbed')
    if(!(pullCondition || pushCondition)) return
    var currentPosition = this.el.object3D.position;
    var distanceToCam = currentPosition.distanceTo( this.target.object3D.position );
    var outOfBound = (pull && distanceToCam < 1) || (push && distanceToCam > 30)
    if (outOfBound) return
    var targetPos = this.el.object3D.worldToLocal(this.target.object3D.position.clone())
    var distance = dt*this.data.speed / 4000;
    //verse tells wether to move entity forward or backward
    var verse = pull ? 1 : -1
    this.el.object3D.translateOnAxis(targetPos, verse*distance);
  }
});


AFRAME.registerComponent('center-text', {
  init: function(){
    setTimeout((e)=>{
      let pos=this.el.getAttribute('position')
      //console.log(pos)
      let mesh = this.el.getObject3D('mesh');
      var box = new THREE.BoxHelper(mesh, 0x00ff00);
      var bbox = new THREE.Box3().setFromObject(box);

			var xScale = this.el.getAttribute("scale").x
      mesh.position.set((bbox.getSize().x / xScale) / -2, 0, 0)
    }, 100)
  }
})

AFRAME.registerComponent('new-read-json', {
  schema: {
    //event: {type: 'string', default: 'click'},
    structureFile: {type: 'string', default: 'data/taxonomy.json'},
    termsDetailFile: {type:'string', default: 'data/terms.csv'}
    //message: {type: 'string', default: 'Hello, World!'}
  },
  init: function(){
    //get file name from query param
    const urlParams = new URLSearchParams(window.location.search);
    const filenameParam = urlParams.get('filename')
    const basePath = saveFolder
    const data = this.data
    const scene =this.el
    const newRad = 8
    const oldOpacity = 0.7
    const newOpacity = 1
    const radOffset = 0.1
    let done = false
    let stack = []
    currStack = stack
    let structureFile = data.structureFile
    //if we have loaded a specific save
    if(filenameParam != null){
      structureFile = basePath + filenameParam
      //update global var
      currFilename = filenameParam
    }
    d3.json(structureFile).then(function(json){

      let currLvlData = json
      let prevLvlData = null
      //let currPos = null
      //let currRad = null
      //let currOpacity = null
      let currDepthLvl = 0
      //const newRad = 8
      //initialize currMaxFreq
      currMaxFreq = 0
      lookForMaxFreq(json)
      console.log('max Freq: ', currMaxFreq)

      //TODO: rimuovere dopo
      /*getAllTerms(json)
      let newConfig = {}
      newConfig.Records = allRecords
      socket.emit('save file', {name: 'terms.csv', file: csvString})
      socket.emit('save file', {name: 'terms.json', file: newConfig})*/
      //console.log(allRecords)
      createBubbles(currLvlData)
      printFilename()
      //printPath(stack)
      //EVENT LISTENER IN SCENE
      scene.addEventListener('bubble-enter', function(event){
        /*for(let inKey in json[outKey]){
          console.log(outKey + '->' + inKey + '->' + json[outKey][inKey].terms.length)

        }*/
        //
        currDepthLvl++
        let stackObj = {}
        let bubble = event.detail.collidingEntity
        let id = bubble.getAttribute('id')
        outerBubbleId = id
        let pos = bubble.getAttribute('position')
        let color = bubble.getAttribute('material').color
        let rad = bubble.getAttribute('geometry').radius
        let prevLvlLabel = 'level'+(currDepthLvl-1)
        let grabbed = bubble.is('grabbed')
        let attracted = d3.select(bubble).classed('attracted')
        if(attracted){
          //label is no more necessary
          d3.select(bubble).classed('attracted', false)
          //remove also the animation
          .attr('animation__move', null)
        }
        let spawnOffset = 0
        if(grabbed || attracted){
          //if the bubble crash with the camera before entering
          //it will have to spawn a bit deeper once we exit again
          spawnOffset = -2*rad
        }
        //add data to stack
        stackObj['id'] = id
        stackObj['pos'] = {x:pos.x, y:pos.y ,z:pos.z+spawnOffset}
        stackObj['color'] = color
        stackObj['rad'] = rad

        stack.push(stackObj)
        prevLvlData = currLvlData
        currLvlData = currLvlData[id]
        //printPath(stack)
        updateWristMenu(stack)
        //if we are at the last level currLvlData[id] will be null
        //currPos = {x: pos.x, y: pos.y, z: pos.z}
        //currRad = bubble.getAttribute('geometry').radius
        //currOpacity = bubble.getAttribute('material').opacity
        console.log('Bubble enter:', id);
        //console.log('COLORE ' + color)
        console.log(currLvlData)
        //d3.select('#'+id)
        //console.log(bubble.getAttribute('geometry'))

        //for su prevlvlBubbles ENTER
        let prevLvlBubbles = document.querySelectorAll('.'+prevLvlLabel)
        for(let i=0; i<prevLvlBubbles.length; i++){
          let curr = prevLvlBubbles[i]
          //if it is the bubble we just collided with, turn it into a dome
          if(curr.getAttribute('id') == id){
            //collided bubble
            for(let j=0; j<curr.childNodes.length; j++){
              let child = curr.childNodes[j]
              child.setAttribute('visible', false)
              //if child is a menu also move it
              if(d3.select(child).classed('menu')){
                child.setAttribute('position', '0 0 0')
              }
            }
            //the dome must not be moved with controllers
            curr.classList.remove('holdable')
            //ENLARGE THE BUBBLE
            curr.setAttribute('material', 'side', 'double')
            //bubble.setAttribute('material', 'opacity', 0.4)
            curr.setAttribute('geometry', 'radius', newRad)
            //pause animation to move bubble
            //curr.pause()

            curr.setAttribute('position', {x:0 , y:0 ,z:0})
          }else{
            //other bubbles will be pushed forward
            curr.setAttribute('material', 'opacity', newOpacity)
            //curr.pause()
            //
            //curr.removeAttribute('bouncing')
            //
            curr.object3D.position.z -= newRad
            //curr.play()
            //lvlBubbles[i].setAttribute('bouncing', true)

            /*let inners = lvlBubbles[i].querySelectorAll('[mixin = inner-bubble]')
            for(let i=0; i<inners.length; i++){
              //console.log(inners[i])
              inners[i].setAttribute('visible', false)
            }*/
          }
        }
        //hide bubbles from 2 levels above
        if(currDepthLvl > 1){
          let remoteLvlLabel = 'level'+(currDepthLvl-2)
          let remoteLvlBubbles = document.querySelectorAll('.'+remoteLvlLabel)
          for(let i=0; i<remoteLvlBubbles.length; i++){
            let curr = remoteLvlBubbles[i]
            //curr.removeEventListener('hitend')
            curr.setAttribute('visible', false)
            let prevDomeId = stack[stack.length -2].id
            if(curr.getAttribute('id') == prevDomeId){
              /*
              TRICK: to prevent the previous dome to fire the
              mouseenter/mouseleave event make it a little greater,
              so it goes behind the current dome
              */
              let currRad = curr.getAttribute('geometry').radius
              curr.setAttribute('geometry', 'radius', currRad+radOffset)
            }
            //curr.removeEventListener('mouseenter', showInfo)
            //curr.removeEventListener('mouseleave', hideInfo)
          }
        }
        //if we are entering a mid-level bubble generate other bubbles
        if(currLvlData != null){
          //generate next level bubble and place them into the dome
          createBubbles(currLvlData, currDepthLvl, '-6 1.5 -6', '6 2 -2')
        }else{
          //generate info plane for the last level term
          let terms = prevLvlData.terms
          let selectedTerm = null
          for(let i=0; i<terms.length; i++){
            if(terms[i].name == id){
              selectedTerm = terms[i]
              break
            }
          }
          //let header = 'Concerning ' + selectedTerm.name + ':'
          //let body = 'Frequency: ' + selectedTerm.freq
          //let header = 'Definition of ' + selectedTerm.name
          let header = selectedTerm.name
          let body = selectedTerm.def
          let termDocs = []
          let currDocIndex = 0
          let termImgs = []
          let currImgIndex = 0
          let termImg = null
          //create card
          let termCard = createCard(selectedTerm, currDepthLvl, header, body)
          //look for media in the file
          d3.csv(data.termsDetailFile).then(function(csvArray) {
            //let n=0
            csvArray.forEach((element)=>{

              if(element.term ==  selectedTerm.name){
                if(element.nc == 'image'){
                  let imgPath = element.filepath
                  //console.log(imgPath)
                  termImgs.push(imgPath)
                  //let image = createImage(selectedTerm, currDepthLvl, imgPath)
                  return
                }

                let newDocument = {}
                newDocument.title = element.title
                newDocument.subtitle = element.subtitle
                //console.log(newDocument)
                termDocs.push(newDocument)
              }
            })
            //console.log(n)
            //fine forEach
          })
          //fine d3.scv
          //BUG: images array takes a while to initialize, i don't know why
          setTimeout(()=>{
            if(termImgs.length > 0){
              termImg = createImage(selectedTerm, currDepthLvl, termImgs[0])
              termImg.on('click', ()=>{
                let clickedX = 0
                if(d3.event.detail == 0){
                  //raycaster case
                  let target = d3.event.target
                  let hand = d3.event.relatedTarget
                  clickedX = hand.components.raycaster.getIntersection(target).point.x
                }else{
                  //cursor case
                  clickedX = d3.event.detail.intersection.point.x
                }
                let verse = 1
                if(clickedX <= 0){
                  verse = -1
                }
                let newIndex = currImgIndex + verse*1
                newIndex = newIndex % (termImgs.length)
                //to enable backward scrolling
                if(newIndex < 0){
                  newIndex = termImgs.length-1
                }
                let newPath = termImgs[newIndex]
                //console.log(newPath)
                termImg.attr('material', 'shader:flat;src:'+newPath)
                //console.log(termImg.attr('material'))
                currImgIndex = newIndex
              })
            }
          }, 500)

          //callback su card
          termCard.on('click', ()=>{
            let cardHeader = termCard.select('.header')
            let cardBody = termCard.select('.body')
            let clickedX = 0
            if(d3.event.detail == 0){
              //raycaster case
              let target = d3.event.target
              let hand = d3.event.relatedTarget
              clickedX = hand.components.raycaster.getIntersection(target).point.x
            }else{
              //cursor case
              clickedX = d3.event.detail.intersection.point.x
            }
            let verse = 1
            if(clickedX <= 0){
              verse = -1
            }
            let newIndex = currDocIndex + verse*1
            newIndex = newIndex % (termDocs.length + 1)
            //to enable backward scrolling
            if(newIndex < 0){
              newIndex = termDocs.length
            }
            let newHeader = header
            let newBody = body
            //convention: 0 index is for base infos
            //if index > 0 subtract 1 and access the document array
            if(newIndex > 0 ){
              newHeader = termDocs[newIndex-1].title
              newBody =  termDocs[newIndex-1].subtitle
            }
            cardHeader.attr('value', newHeader)
            cardBody.attr('value', newBody)
            currDocIndex = newIndex
            //console.log(termDocs.length)
            //console.log(currIndex)

          })
          //fine card callback
        }

        //fine for su prevLvlBubbles
        //let childEnts = bubble.childNodes
        //console.log(childEnts)

        //ENLARGE THE BUBBLE
        //bubble.setAttribute('material', 'side', 'double')
        //bubble.setAttribute('material', 'opacity', 0.4)
        //bubble.setAttribute('geometry', 'radius', newRad)

        //pause animation to move bubble
        //bubble.pause()
        //bubble.setAttribute('position', {x:0 , y:0 ,z:0})
        //console.log(bubble)
        //console.log(bubble.object3D.position)
        //

      })
      //fine bubble enter
      scene.addEventListener('bubble-exit', function(event){
        currDepthLvl--
        let stackObj = stack.pop()
        let bubble = event.detail.collidingEntity
        let id = bubble.getAttribute('id')
        let pos = stackObj.pos
        let color = stackObj.color
        let rad = stackObj.rad
        currLvlData = prevLvlData
        prevLvlData = getPrevLvlData(json, stack)
        let nextLvlLabel = 'level'+(currDepthLvl+1)
        let currLvlLabel = 'level'+(currDepthLvl)
        console.log('Bubble exit:',id);
        console.log(prevLvlData)
        //console.log(bubble)
        //for su lvlBubbles EXIT
        //remove bubbles from the deeper level
        //printPath(stack)
        updateWristMenu(stack)
        let nextLvlBubbles = document.querySelectorAll('.'+nextLvlLabel)
        for(let i=0; i<nextLvlBubbles.length; i++){
          let curr = nextLvlBubbles[i]
          curr.parentNode.removeChild(curr)
        }
        //restore bubbles from current lvl
        let currLvlBubbles = document.querySelectorAll('.'+currLvlLabel)
        for(let i=0; i<currLvlBubbles.length; i++){
          let curr= currLvlBubbles[i]
          //if it is the bubble we just collided with, turn it back to a normal bubble
          if(curr.getAttribute('id') == id){
            //collided bubble
            for(let j=0; j<curr.childNodes.length; j++){
              let child = curr.childNodes[j]
              //show child only if it's not a menu
              if(!d3.select(child).classed('menu')){
                child.setAttribute('visible', true)
              }

            }
            //make it holdable again
            curr.classList.add('holdable')
            curr.setAttribute('material', 'side', 'front')
            curr.setAttribute('geometry', 'radius', rad)
            //curr.pause()
            curr.object3D.position.set(pos.x, pos.y , pos.z )
            //curr.play()
          }else{
            //other bubbles
            //console.log(lvlBubbles[i])
            curr.setAttribute('material', 'opacity', oldOpacity)
            //lvlBubbles[i].pause()
            //curr.pause()
            curr.object3D.position.z += newRad
            //curr.play()
            /*setTimeout(function(){
              curr.setAttribute('bouncing', true)
            }, 100)*/
            /*let inners = lvlBubbles[i].querySelectorAll('[mixin = inner-bubble]')
            for(let i=0; i<inners.length; i++){
              //console.log(inners[i])
              inners[i].setAttribute('visible', true)
            }*/
          }
        }
        //fine for su currLvlBubbles
        //restore bubbles from level above
        if(currDepthLvl > 0){
          let remoteLvlLabel = 'level'+(currDepthLvl-1)
          let remoteLvlBubbles = document.querySelectorAll('.'+remoteLvlLabel)
          for(let i=0; i<remoteLvlBubbles.length; i++){
            let curr = remoteLvlBubbles[i]
            //curr.classList.add('collidable')
            //curr.setAttribute('blink-teleportation', true)

            curr.setAttribute('visible', true)
            let currDomeId = stack[stack.length-1].id
            if(curr.getAttribute('id') == currDomeId){
              /*
              TRICK: to prevent the previous dome to fire the
              mouseenter/mouseleave event make it a little greater,
              so it goes behind the current dome.
              Here we bring it back as it was before
              */
              let currRad = curr.getAttribute('geometry').radius
              curr.setAttribute('geometry', 'radius', currRad-radOffset)
              //make it collidable again
              d3.select(curr).classed('collidable', true)
            }
            //curr.addEventListener('mouseenter', showInfo)
            //curr.addEventListener('mouseleave', hideInfo)
          }
        }

      })
      //fine bubble exit
      scene.addEventListener('add-sibiling',
      (e) =>{
        let category = e.detail.category
        let newName = e.detail.newName
        //create a new bubble into the dome with default radius
        let posMin = '-6 1.5 -6'
        let posMax = '6 2 -2'
        let newBubble = createBubble(newName, 'level'+currDepthLvl, category, posMin, posMax)
        let newBubbleText = createBubbleText(newBubble, newName+'-text', newName)
        //add new category or term to json
        if(category){
          //create a new property on same level and append it
          currLvlData[newName] = {}
        }else{
          //initialize a new term
          let newTerm = {doubt:0, freq: 0, name: newName, weight:1}
          //if curr level does not have a terms array yet, initialize it
          if(currLvlData.terms == null){
            currLvlData.terms = []
          }
          //add it to the curr level terms
          currLvlData.terms.push(newTerm)
          //generate the level bar as well
          let frequency = newTerm.freq
          let levelBar = createLevelBar(newBubble, newName+'-levelBar', frequency)
        }
        //fine add
        //add an inner bubble to the parent (if there is one)
        if(stack.length > 0){
          let newParentId = stack[stack.length-1].id
          let parentRadius = stack[stack.length-1].rad
          let newParent = document.getElementById(newParentId)
          //modificare inner bubble
          let newInnerBubble = createInnerBubble(d3.select(newParent), newName + '-inner', parentRadius)
          //initially invisible
          newInnerBubble.attr('visible', false)
        }
        console.log('currLvlData', currLvlData)
        console.log('currLvlData\[newName\]', currLvlData[newName])
        console.log('json', json)
        console.log('json\[newName\]', json[newName])
      })
      scene.addEventListener('add-child', (e) =>{
        let category = e.detail.category
        let newName = e.detail.newName
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          console.log('Adding child to a term?')
          console.log('work in progress')
        }
        //add new category or term to json
        if(category){
          //create a new property and append it
          currLvlData[requestingId][newName] = {}
        }else{
          //initialize a new term
          let newTerm = {doubt:0, freq: 0, name: newName, weight:1}
          //if next level does not have a terms array yet, initialize it
          if(currLvlData[requestingId].terms == null){
            currLvlData[requestingId].terms = []
          }
          //add it to the next level terms
          currLvlData[requestingId].terms.push(newTerm)
        }
        //fine add
        //modificare inner bubble
        let newInnerBubble = createInnerBubble(d3.select(requesting), newName + '-inner')
        console.log('requestingId', requestingId)
        console.log('currLvlData\[requestingId\]', currLvlData[requestingId])
        console.log('json', json)
        console.log('json\[requestingId\]', json[requestingId])
      })
      scene.addEventListener('push-into', (e) =>{
        //speed in m/s
        const speed = 4
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let target = e.detail.targetEntity
        let targetId = target.getAttribute('id')
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          let terms = currLvlData.terms
          //search for it in the array and delete from it
          for( let i = 0; i < terms.length; i++){
            if ( terms[i].name == requestingId) {
              requestingData = terms[i]
              terms.splice(i, 1);
              break;
            }
          }
        }
        let targetData = currLvlData[targetId]
        let requestingPos = requesting.getAttribute('position')
        let targetPos = target.getAttribute('position')
        let distance = requestingPos.distanceTo( targetPos );
        //duration in ms
        let dur = distance/speed*1000
        console.log('dur', dur/1000)
        let animationPush = {
          property: 'position',
          to: {x: targetPos.x, y: targetPos.y, z: targetPos.z},
          dur: dur,
          easing: 'easeInQuad'

        }
        //prevent the user from grabbing or colliding with entity while it's moving
        //and see if it is a last level term
        let nested = d3.select(requesting)
        .classed('holdable', false)
        .classed('collidable', false)
        .classed('nested')
        //set the animation
        requesting.removeAttribute('animation__push')
        requesting.setAttribute('animation__push', animationPush)
        requesting.addEventListener('animationcomplete', (e) =>{
          console.log(e.detail.name)
          //if we are pushing a nested bubble
          if(nested){
            //add it to the next level
            currLvlData[targetId][requestingId] = requestingData
            //delete the requesting property from the current level
            delete currLvlData[requestingId]
          }
          //if we are pushing a last level term
          else{
            //if next level does not have a terms array yet, initialize it
            if(currLvlData[targetId].terms == null){
              currLvlData[targetId].terms = []
            }
            //add it to the next level terms
            currLvlData[targetId].terms.push(requestingData)
            //has already been deleted from the current terms array
          }
          //remove the bubble from scene
          requesting.parentNode.removeChild(requesting)
          //add a new bubble into the target outer bubble
          //modificare inner bubble
          let newInnerBubble = createInnerBubble(d3.select(target), requestingId + '-inner')
          //remove an inner bubble from parent bubble (if there is one)
          if(stack.length > 0){
            d3.select(document.getElementById(requestingId+'-inner')).remove()
          }
          console.log('requestingId', requestingId)
          console.log('requestingData', requestingData)
          console.log('currLvlData\[targetId\]\[requestingId\]', currLvlData[targetId][requestingId])
          console.log('json', json)
          console.log('json\[targetId\]', json[targetId])
        })

        //set global vars again to false and null
        waitingForPushInto = false
        requestingPushInto = null
        //console.log('puntatore', targetData)
        //console.log('json', json[targetId])
      })
      scene.addEventListener('push-out', (e) =>{
        const speed = 6
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        if(stack.length < 1){
          return false
        }
        let currLvlLabel = 'level'+currDepthLvl
        let newLvlLabel = 'level'+(currDepthLvl-1)
        let parentId = stack[stack.length - 1].id
        //let parent = document.getElementById(parentId)
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          let terms = currLvlData.terms
          //search for it in the array and delete from it
          for( let i = 0; i < terms.length; i++){
            if ( terms[i].name == requestingId) {
              requestingData = terms[i]
              terms.splice(i, 1);
              break;
            }
          }
        }
        let requestingPos = requesting.getAttribute('position')
        //make sure the bubble will be pushed out of the dome
        let newZ = (requestingPos.z > 0 ? -requestingPos.z: requestingPos.z) - newRad
        let targetPos = {x: requestingPos.x, y:requestingPos.y, z:newZ}
        let distance = requestingPos.distanceTo( targetPos );
        //duration in ms
        let dur = distance/speed*1000
        let animationPush = {
          property: 'position',
          to: {x: targetPos.x, y: targetPos.y, z: targetPos.z},
          dur: dur,
          easing: 'easeInQuad'
        }
        //prevent the user from grabbing or colliding with entity while it's moving
        let nested = d3.select(requesting)
        .classed('holdable', false)
        .classed('collidable', false)
        //change lvl label to the requesting bubble and also its opacity
        .classed(currLvlLabel, false)
        .classed(newLvlLabel, true)
        .attr('material', 'opacity:1')
        //and finally see if it is a last level term
        .classed('nested')
        //set the animation
        requesting.removeAttribute('animation__push')
        requesting.setAttribute('animation__push', animationPush)
        requesting.addEventListener('animationcomplete', (e) =>{
          //if we are pushing a nested bubble
          if(nested){
            //add it to the prev level
            prevLvlData[requestingId] = requestingData
            //delete the requesting property from the current level
            delete currLvlData[requestingId]
          }
          //if we are pushing out a last level term
          else{
            //if prev level does not have a terms array yet, initialize it
            if(prevLvlData.terms == null){
              prevLvlData.terms = []
            }
            //add it to the prev level terms
            prevLvlData.terms.push(requestingData)
            //has already been deleted from the current terms array
          }
          //make bubble holdable again
          d3.select(requesting)
            .classed('holdable', true)
          //remove the inner bubble from its parent
          d3.select(document.getElementById(requestingId+'-inner')).remove()
          //add a new inner bubble into the target outer bubble from 2 levels above
          if(stack.length > 1){
            let newParentId = stack[stack.length-2].id
            let newParent = document.getElementById(newParentId)
            //modificare inner bubble
            let newInnerBubble = createInnerBubble(d3.select(newParent), requestingId + '-inner')
            //initially invisible
            newInnerBubble.attr('visible', false)
          }
          console.log('requestingId', requestingId)
          console.log('requestingData', requestingData)
          console.log('prevLvlData\[requestingId\]', prevLvlData[requestingId])
          console.log('json', json)
          console.log('json\[parentId\]', json[parentId])
        })
      })
      scene.addEventListener('edit-name', (e) =>{
        let newName = e.detail.newName
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          let terms = currLvlData.terms
          //search for it in the array and modify the name field
          for( let i = 0; i < terms.length; i++){
            if ( terms[i].name == requestingId) {
              terms[i].name = newName
              break;
            }
          }
        }else{
          //otherwise create another property with the new name
          currLvlData[newName] = requestingData
          //and delete the old one
          delete currLvlData[requestingId]
        }
        //update the bubble
        requesting.setAttribute('id', newName)
        //delete former text
        let oldText = document.getElementById(requestingId + '-text')
        d3.select(oldText).remove()
        //create a new one
        createBubbleText(d3.select(requesting), newName+'-text', newName)
        console.log('requestingId', requestingId)
        console.log('requestingData', requestingData)
        console.log('currLvlData\[requestingId\]', currLvlData[requestingId])
        console.log('currLvlData\[newName\]', currLvlData[newName])
        console.log('currLvlData', currLvlData)
        console.log('json', json)
      })
      scene.addEventListener('edit-frequency', (e) =>{
        let newFreq = e.detail.newFrequency
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let levelBar = document.getElementById(requestingId + '-levelBar')
        if(levelBar == null){
          //if there is no levelBar
          return
        }
        let barId = levelBar.getAttribute('id')
        let bubble = d3.select(levelBar.parentNode)
        if(currLvlData.terms == null){
          //if currLvl does not have a term array
          //IT SHOULD NEVER HAPPEN since we are modifying frequencies of terms
          return
        }
        //select all last level terms
        let terms = currLvlData.terms
        //search for the requesting one in the array and modify the freq field
        for( let i = 0; i < terms.length; i++){
          if ( terms[i].name == requestingId) {
            terms[i].freq = newFreq
            break;
          }
        }
        //max frequency could have changed
        currMaxFreq = 0
        lookForMaxFreq(json)
        console.log('max Freq: ', currMaxFreq)
        //update level bar
        d3.select(levelBar).remove()
        let newBar = createLevelBar(bubble, barId, newFreq)
        //UPDATE BUBBLES? MAYBE LATER
        console.log('requestingId', requestingId)
        console.log('currLvlData.terms', currLvlData.terms)
        console.log('json', json)
      })
      scene.addEventListener('delete', (e) =>{
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          let terms = currLvlData.terms
          //search for it in the terms array and delete from it
          for( let i = 0; i < terms.length; i++){
            if ( terms[i].name == requestingId) {
              requestingData = terms[i]
              terms.splice(i, 1);
              break;
            }
          }
        }
        //duration in ms
        const dur = 1000
        const minRad = 0.1
        let animationDelete = {
          property: 'scale',
          to: {x: minRad, y: minRad, z: minRad},
          dur: dur,
          easing: 'easeInQuad'
        }
        //prevent the user from grabbing or colliding with entity while it's moving
        //and see if it is a last level term
        let nested = d3.select(requesting)
        .classed('holdable', false)
        .classed('collidable', false)
        .classed('nested')
        //set the animation
        requesting.removeAttribute('animation__delete')
        requesting.setAttribute('animation__delete', animationDelete)
        requesting.addEventListener('animationcomplete', (e) =>{
          console.log(e.detail.name)
          //if we are pushing a nested bubble
          if(nested){
            //delete the requesting property from the current level
            delete currLvlData[requestingId]
          }
          //if we are deleting a last level term
          //has already been deleted from the current terms array
          //remove the bubble from scene
          requesting.parentNode.removeChild(requesting)
          //remove an inner bubble from parent bubble (if there is one)
          if(stack.length > 0){
            d3.select(document.getElementById(requestingId+'-inner')).remove()
          }
          //max frequency could have changed
          currMaxFreq = 0
          lookForMaxFreq(json)
          console.log('max Freq: ', currMaxFreq)
          //
          console.log('requestingId', requestingId)
          console.log('requestingData', requestingData)
          console.log('currLvlData\[requestingId\]', currLvlData[requestingId])
          console.log('currLvlData', currLvlData)
          console.log('json', json)
          //console.log('json\[targetId\]', json[targetId])
        })
      })
      scene.addEventListener('change-level', (e)=>{
        let entityToSignal = e.detail.entityToSignal
        let newLevelId= e.detail.newLevelId
        //e.g. base map ce l'ha a -1
        let stackIndex = e.detail.stackIndex
        //e.g. base map ce l'ha a 0
        let newCurrLvl = stackIndex + 1
        //set the stack as it was in the level immediately following the one we clicked
        stack.splice(stackIndex+2)
        //remove all the bubbles from last level to newCurrLvl+1
        //e.g. if base map remove all bubbles except level1 and level0
        for(let i=currDepthLvl; i>newCurrLvl+1; i--){
          let lvlLabel = 'level'+i
          d3.selectAll('.'+lvlLabel).remove()
        }
        //also set prevLvlData as it was in that level
        prevLvlData = getPrevLvlData(json, stack)
        //same for the lvl variable
        currDepthLvl = newCurrLvl+1
        //find the last dome bubble serching only among its sibiling bubbles (possible aliases)
        let lastDome = null
        let lastDomeId = stack[stack.length-1].id
        let sibilings = Array.from(document.getElementsByClassName('level'+newCurrLvl))
        sibilings.forEach((element)=>{
          if(element.getAttribute('id') == lastDomeId){
            lastDome = element
          }
          //make all sibilings visible and collidable again: bubble-exit doesn't do that
          d3.select(element)
          .attr('visible', true)
          .classed('collidable', true)
        })
        //and simulate a bubble exit
        if(lastDome != null){
          scene.emit('bubble-exit', {collidingEntity: lastDome})
          entityToSignal.emit('position-changed')
        }

      })
      scene.addEventListener('save-file', (e)=>{
        let name = e.detail.name
        socket.emit('save file', {name: name, file: json})
      })
      scene.addEventListener('load-file', (e)=>{
        //provvisorio, redirect sempre sullo stesso file
        let name = e.detail.name
        //reload the page with a new query param
        window.location.replace(window.location.pathname+"?"+$.param({filename: name}))
      })
      scene.addEventListener('find-word', (e)=>{
        let word = e.detail.word
        let path = getWordPath(json, word)
        if(path != null){
          printLog(path)
        }
      })


    }, function(error){
      //use this callback to handle errors in access to file
      let monitor = document.getElementById(speechBoxId)
      d3.select(monitor).attr('value', 'Error in file \''+structureFile+'\'')
      //the ony possible thing is to load another valid file
      scene.addEventListener('load-file', (e)=>{
        let name = e.detail.name
        //reload the page with a new query param
        window.location.replace(window.location.pathname+"?"+$.param({filename: name}))
      })
    })
    //fine d3.json

  }
  //fine init func
})

/*AFRAME.registerComponent('read-json', {
  schema: {
    //event: {type: 'string', default: 'click'},
    filename: {type: 'string', default: 'taxonomy.json'},
    //message: {type: 'string', default: 'Hello, World!'}
  },
  init: function(){
    const data = this.data
    const scene =this.el
    //console.log("%cREACH HERE", "color: black; font-style: italic; background-color: red;padding: 2px")
    console.log(data.filename)
    d3.json(data.filename).then(function(json){

      let currLvlData = json
      let prevLvlData = null
      let currPos = null
      let currRad = null
      let currOpacity = null
      let currDepthLvl = 0
      const newRad = 8
      createBubbles(currLvlData)
      console.log('LIVELLO 0')
      //let lvl0Bubbles = document.querySelectorAll('.level0')
      //console.log(lvl0Bubbles)
      //EVENT LISTENER IN SCENE
      scene.addEventListener('bubble-enter', function(event){



        currDepthLvl++
        let bubble = event.detail.collidingEntity
        let id = bubble.getAttribute('id')
        let pos = bubble.getAttribute('position')
        let color = bubble.getAttribute('material').color

        prevLvlData = currLvlData
        currLvlData = currLvlData[id]

        currPos = {x: pos.x, y: pos.y, z: pos.z}
        currRad = bubble.getAttribute('geometry').radius
        currOpacity = bubble.getAttribute('material').opacity
        console.log('Bubble enter:', id);
        console.log('COLORE ' + color)
        console.log(currLvlData)
        //d3.select('#'+id)
        //console.log(bubble.getAttribute('geometry'))

        //for su lvlBubbles ENTER
        let lvlBubbles = document.querySelectorAll('[mixin = myBubble]')
        for(let i=0; i<lvlBubbles.length; i++){
          let rmvCondition = (lvlBubbles[i].getAttribute('material').opacity == 1)
          console.log(rmvCondition)
          if(currDepthLvl > 1 && rmvCondition){
            //lvlBubbles[i].parentNode.removeChild(lvlBubbles[i]);
          }
          //if it is the bubble we just collided with, skip
          if(lvlBubbles[i].getAttribute('id') == id){
            //collided bubble
            for(let j=0; j<lvlBubbles[i].childNodes.length; j++){
              lvlBubbles[i].childNodes[j].setAttribute('visible', false)
            }
          }else{
            //other bubbles
            //console.log(lvlBubbles[i])
            lvlBubbles[i].setAttribute('material', 'opacity', 1)
            lvlBubbles[i].pause()
            //lvlBubbles[i].removeAttribute('bouncing')
            lvlBubbles[i].object3D.position.z -= newRad
            //lvlBubbles[i].setAttribute('bouncing', true)


          }
        }
        //fine for su lvlBubbles
        let childEnts = bubble.childNodes
        //console.log(childEnts)

        //ENLARGE THE BUBBLE
        bubble.setAttribute('material', 'side', 'double')
        //bubble.setAttribute('material', 'opacity', 0.4)
        bubble.setAttribute('geometry', 'radius', newRad)

        //pause animation to move bubble
        bubble.pause()
        bubble.setAttribute('position', {x:0 , y:0 ,z:0})
        //console.log(bubble)
        //console.log(bubble.object3D.position)
        //

      })
      //fine bubble enter
      scene.addEventListener('bubble-exit', function(event){
        currDepthLvl--
        //console.log(currPos)
        //console.log(currRad)
        let bubble = event.detail.collidingEntity
        let id = bubble.getAttribute('id')

        currLvlData = prevLvlData
        prevLvlData = getPrevLvlData(json, id)

        console.log('Bubble exit:',id);
        console.log(prevLvlData)
        //console.log(bubble)
        //for su lvlBubbles EXIT
        let lvlBubbles = document.querySelectorAll('[mixin = myBubble]')
        for(let i=0; i<lvlBubbles.length; i++){
          //if it is the bubble we just collided with, skip
          if(lvlBubbles[i].getAttribute('id') == id){
            //collided bubble
            for(let j=0; j<lvlBubbles[i].childNodes.length; j++){
              lvlBubbles[i].childNodes[j].setAttribute('visible', true)
            }
          }else{
            //other bubbles
            //console.log(lvlBubbles[i])
            lvlBubbles[i].setAttribute('material', 'opacity', currOpacity)
            //lvlBubbles[i].pause()
            lvlBubbles[i].object3D.position.z += newRad
            lvlBubbles[i].play()

          }
        }
        //fine for su lvlBubbles
        bubble.setAttribute('material', 'side', 'front')
        bubble.setAttribute('geometry', 'radius', currRad)
        //bubble.setAttribute('material', 'opacity', currOpacity)
        bubble.pause()
        bubble.object3D.position.set(currPos.x, currPos.y , currPos.z )
        bubble.play()

      })
      //fine bubble exit
    })
    //fine d3.json

  }
  //fine init func
})*/


/*AFRAME.registerComponent('read-json-old', {
  schema: {
    event: {type: 'string', default: 'click'},
    filename: {type: 'string', default: 'groups.json'},
    message: {type: 'string', default: 'Hello, World!'}
  },
  init: function(){
    const data = this.data
    const scene =this.el
    console.log("%cREACH HERE", "color: black; font-style: italic; background-color: red;padding: 2px")
    console.log(data.filename)
    d3.json(data.filename).then(function(json){

    console.log(json)
    var maxTot = 0
    var sum = 0
    let totFreq = {}
    //FIRST FOR to compute max among groups
    for(let key in json){
      //search the total freq for each group
      var tot = 0
      for(var i=0; i<json[key].length; i++){
        tot+= json[key][i].freq
      }
      //UPDATE MAP
      totFreq[key] = tot
      console.log(key + ': ' + tot)
      //UPDATE SUM
      sum += tot
      //UPDATE MAXTOT
      if(tot > maxTot){
        maxTot = tot
      }
    }
    console.log('MASSIMO: ' + maxTot)
    console.log('TOTALE: ' + sum)
    var i=1
    //SECOND FOR
    for (let key in json) {
      //DEBUG
      if(i>5) break;
      console.log("PROPERTY " + i++ +": " + key );
        //sum o maxTot?
        let perc = (totFreq[key]/maxTot)
        if(perc == 0)
          continue;
        const maxExtRadius = 10
        let extRadius = perc*maxExtRadius
        let posMin = ''
        let posMax = ''
        if(perc < 10/100 ){
          posMin = '-15 1 -15'
          posMax = '15 2 15'
        }else if(perc < 20/100){
          posMin = '-15 3.5 -20'
          posMax = '15 4 20'
        }else if(perc < 50/100){
          posMin = '-30 6 -30'
          posMax = '30 8 30'
        }else if(perc < 80/100){
          posMin = '-40 15 -50'
          posMax = '40 18 50'
        }else{
          posMin = '-70 25 -100'
          posMax = '70 30 100'
        }
        //random-position="min: -20 2 -15; max: 20 2.5 15"
        var bubble = d3.select('a-scene')
           .append('a-entity')
           .attr('id', key)
           .classed('bubble', true)
           .attr('mixin', 'bubble')
           .attr('geometry', 'radius:'+extRadius)
           .attr('random-position', 'min:' + posMin + ';max:' + posMax)
           .on('mouseenter', function(d, i){
             console.log('hovered: ' + key)
             console.log('total freq: ' + totFreq[key])
             //console.log('provv: ' + provvisorio)
             d3.select('#speechText')
               .attr('value', key)
           })
           .on('mouseleave', function(d, i){
             d3.select('#speechText')
               .attr('value', '')
           })
        var text = bubble.append('a-text')
           .attr('id', key + "-text")
           .attr('value', key)
           .attr('mixin', 'bubble-text')
           .attr('position', {x:0, y: -(extRadius + 0.3), z:0})
        var bubbleInfo = bubble.append('a-plane')
          .attr('id', key + "-info")
          .attr('mixin', 'bubble-info')
          .attr('position', {x:0, y: 0, z: (extRadius + 1)})
        var header = bubbleInfo.append("a-text")
          .attr('id', key + "-info-header")
          .attr("scale", "0.5 0.5 0.5")
          .attr("text", "align:center")
          .attr("value", "Concerning " + key + "- freq "+ totFreq[key] + "\n(" + (perc*100).toFixed(2) + "%):")
          .attr("color", "red")
          .attr("position", function(d, i){
            var x = 0
            var y = 0.35
            var z = 0
            return x + " " + y + " " + z
           })
        var words = bubbleInfo.selectAll("a-text .message")
          .data(json[key])
          .enter()
          .append("a-text")
          .classed('word', true)
          .attr('id', key + "-info-body")
          .attr("text", "align:left")
          .attr("color", "black")
          .attr("scale", "0.4 0.4 0.4")
          .attr("value", function(d, i){
            return '#'+ (i+1) + ": " + d.name + " f: " + d.freq //+ " w: " + d.weight
          })
          .attr("position", function(d, i){
            var rows = 4
            var x = -0.9+(0.9)*Math.floor(i/rows)
            var y = 0.15-0.15*(i%rows)
            var z = 0
            return x + " " + y + " " + z
           })
        //search the max freq for each group
        //let provvisorio = totFreq[key]
        var innerBubble = bubble.selectAll('a-sphere')
            .data(json[key])
            .enter()
            .append('a-sphere')
            .attr('mixin', 'inner-bubble')
            .attr('id', function(d){
              return d.name
            })
            .attr('geometry', function(d, i){
              //Normalize the radius respect to maxFreq
              var maxRadius = extRadius/2
              console.log('ER: ' + extRadius)
              console.log('MR: ' + maxRadius)
              console.log('F: ' + d.freq)
              console.log('TF: ' + totFreq[key])
              var radius = (d.freq/totFreq[key])*maxRadius
              console.log('RAD: ' + radius)
              return 'radius:' + radius
            })
            //random-position="min: -0.5 -0.5 -0.5; max: 0.5 0.5 0.5"

            .attr('random-position', 'min: ' + -extRadius/2 + ' ' + -extRadius/2 + ' ' + -extRadius/2 + ';' +
                                     'max: ' + extRadius/2 + ' ' + extRadius/2 + ' ' + extRadius/2)
            .on('mouseenter', function(d, i){
              var category = d3.select(this.parentNode)
                          .attr('id')
              console.log('total freq: ' + totFreq[key])
              //console.log('provv: ' + provvisorio)
              var detail = 'category: ' + category + '\n' +
                           'frequency: ' + d.freq + '\n' +
                           'Perc: ' + (d.freq/totFreq[key]*100).toFixed(2) + '\%\n'
              d3.select('#wordDetailHeader')
                .attr('value', d.name)
              d3.select('#wordDetailBody')
                .attr('value', detail)
            })
            .on('mouseleave', function(d, i){
              d3.select('#wordDetailHeader')
                .attr('value', '')
              d3.select('#wordDetailBody')
                .attr('value', '')
            })
        //assign some event listeners to the bubbles
        bubble.on(data.event, function(evt){
          const bubbleInfo = this.querySelector('a-plane')
          var isVisible = bubbleInfo.getAttribute("visible")
          var toggle = !isVisible
          bubbleInfo.setAttribute("visible", toggle)
        })
      }
      //fine for
    })
    //fine d3.json
  }
})*/


AFRAME.registerComponent('blink-teleportation', {
	schema: {
		pos: {type: 'vec3', default: {x:0, y:0, z:0}},
		dur: {type: 'number', default: 300},
    eventIn: {default: 'hitstart'},
    eventOut: {default: 'hitend'},
		hide: {type: 'boolean', default: false}
	},

	init: function () {
		var el = this.el;
		var data = this.data;
    //console.log(color)
    var radius = 1
		var camera = document.querySelector('#camera');
		var cameraRig = document.querySelector('#cameraRig');
		var cursor = document.querySelector('a-cursor');
    var scene = document.querySelector('a-scene');
    /*var collidableEls = document.querySelectorAll('[blink-teleportation]')
    console.log(collidableEls)*/
    var fadeAnim = {
      property: 'material.opacity',
      from: 0,
      to: 1,
      dur: data.dur,
      easing: 'easeOutCubic'
    }
		// CREATE A TRANSPARENT BLACK IMAGE
		var blink = document.createElement('a-image');
		blink.setAttribute('material', {
			color: '#000000',
			opacity: 0
		});
    // SET THE BLACK IMAGE POSITION AND APPEND IT AS CAMERA'S CHILD ENTITY
		blink.object3D.position.z = -0.1;
		camera.appendChild(blink);
    //set to false to prevent bug
    blink.setAttribute('visible', false)


		// ON COLLISION (hitstart), ANIMATE THE BLACK IMAGE (FADE-IN)
		el.addEventListener(data.eventIn, function () {
      //let's prevent event to fire too often
      if(eventLocker)
        return;
      eventLocker = true;
      setTimeout(function(){
        eventLocker = false;
      }, blinkEventLockerTime)
      //
      //set to true to make animation visible
      blink.setAttribute('visible', true)
			blink.setAttribute('animation', fadeAnim);
			// WHEN FADE-IN ANIMATION COMPLETES, MOVE THE CAMERA RIG TO DESTINATION
			setTimeout(function () {
				cameraRig.setAttribute('position', data.pos);
        camera.setAttribute('position', {x: data.pos.x, y: data.pos.y+1.6, z: data.pos.z})
        //el.setAttribute('material', 'side', 'double')
        //el.setAttribute('geometry', 'radius', 10)
        //collidableEls must be here in event handler in order
        //to select all elements when initialized
        var collidableEls = document.querySelectorAll('[blink-teleportation]')
        for (var i = 0; i < collidableEls.length; i++) {
          collidableEls[i].classList.remove('collidable')
        }
        //EMIT ENTER EVENT TO SCENE
        scene.emit('bubble-enter', {collidingEntity: el});
        // THEN MAKE ONLY THE SELECTED ELEMENT COLLIDABLE
        el.classList.add('collidable')
				// EMIT CUSTOM EVENT TO TRIGGER THE FADE-OUT ANIMATION
				el.emit('position-changed');

			}, data.dur);
		});

		// ON CUSTOM EVENT, ANIMATE THE BLACK IMAGE (FADE-OUT)
		el.addEventListener('position-changed', function () {
      blink.setAttribute('animation', {
				to: 0
			});
      blink.setAttribute('visible', false)
      //d3.select(blink).remove()
		});

    el.addEventListener(data.eventOut, function(){
      //let's prevent event to fire too often
      if(eventLocker)
        return;
      eventLocker = true;
      setTimeout(function(){
        eventLocker = false;
      }, blinkEventLockerTime)
      //
      //set to true to make animation visible
      blink.setAttribute('visible', true)
      blink.setAttribute('animation', fadeAnim);
      // WHEN FADE-IN ANIMATION COMPLETES, MOVE THE CAMERA RIG TO DESTINATION
			setTimeout(function () {
				cameraRig.setAttribute('position', data.pos);
        camera.setAttribute('position', {x: data.pos.x, y: data.pos.y+1.6, z: data.pos.z})
        //el.setAttribute('material', 'side', 'front')
        //el.setAttribute('geometry', 'radius', radius)
        var collidableEls = document.querySelectorAll('[blink-teleportation]')
        for (var i = 0; i < collidableEls.length; i++) {
          collidableEls[i].classList.add('collidable')
        }
        //EMIT EXIT EVENT TO SCENE
        scene.emit('bubble-exit', {collidingEntity: el});
				// EMIT CUSTOM EVENT TO TRIGGER THE FADE-OUT ANIMATION
				el.emit('position-changed');
			}, data.dur);
    });
	}
});