// var socket = io();
//a lock variable to prevent multiple event triggering
var eventLocker = false

//global variables to push and pull bubbles
var push = false
var pull = false
//global map to navigate through menus: it will hold the current starting index of menu options
var menuMap = {}
//global var to navigate through the saves list
var loadMenuIndex = 0
//global var to handle speech cmd into menus
var newNameCnt = 0
var newSaveCnt = 0
var waitingForName = false
var editingName = false
var editingFrequency = false
var waitingForSave = false
//global var to push without using menu
var requestingPushInto = null
var waitingForPushInto = false
//global var to travel through levels
var currStack = null
//global var to prevent conflicts with raycaster
var buttonIntersected = false
var rightWristMenuOpen = false
var leftWristMenuOpen = false
//global var to easily handle change in the frequency
var currMaxFreq = 0
//global var to hold the current filename
var currFilename = null
//TODO: rimuovere
var allRecords = []
var csvString = ""
var csvIndex = 1760

//this variable is to handle the interaction with keyboard
var lookAtEntityFlag = false
var lookAtEntity = null
var lastKey = null
var outerBubbleId = "";
//
const saveFolder = './saves/'
//dimension of each page of a list
const menuSize = 4
//dimension of load list
const loadMenuSize = 4
//const to prevent conflicts in clicking wrist menus
//original y was 0.3
//TODO: modificare original e hiding come segue
//original = "0 0.3 -0.5"
//hiding = "0 0.3 -0.6"
//original debug = "-2 1 -0.5"
//hiding debug "-2 1 -0.6"
const wristMenuOriginalPos = "0 0.6 -1.5"
const wristMenuHidingPos = "0 0.6 -20"
//TODO: provvisorio, da rimuovere
//const rightMenuPos = "2 1 -0.5"
//delay time (in ms) to destroy intersectable menu
const menuDelay = 1000
const cleanTime = 3000
//delay time to prevent multiple event firings
const blinkEventLockerTime = 1000
//delay
const hoverVisibilityTimeout = 3000
const speechVisibilityTimeout = 5000
const logVisibilityTimeout = 4000
//some const strings
const defaultFilename = 'New project'
const filenameBoxId = 'filenameBox'
const pathBoxId = 'pathBox'
const speechBoxId = 'speechBox'
const logBoxId = 'logBox'
//const messages to log
const nothingToLoadMsg = 'There are no savestates to load'
const noPushIntoTermsMsg = 'You can not push bubbles into individual terms'
const noPushIntoMsg = 'There are no categories in which you can push at this level'
const noPushOutMsg = 'You are not inside a category'
const duplicateNameMsg = 'Name already present, say another one'
const pushIntoMsg = 'Select a target from the list\nor click on a category bubble'
const addNameMsg = 'Speak and give a name to the new bubble\nClick on button when satisfied'
const newNameMsg = 'Speak and give a new name to the bubble\nClick on button when satisfied'
const newFreqMsg = 'Speak and give a new frequency to the term\nClick on button when satisfied'
const newSaveMsg = 'Speak and give a name to your savestate\nClick on button when satisfied'
const currLvlMsg = 'You are already in this level'


cleaner()



//receive the current stack and return the previous lvl of json
function getPrevLvlData(json, stack){
  //if stack is empty prev is  null
  let prev = null
  //if stack has only one element prev is json. In any case initialize prev
  if(stack.length >= 1)
    prev = json
  //enter for only if length > 1
  for(let i=0; i<(stack.length-1); i++){
    let id = stack[i].id
    prev = prev[id]

  }
  return prev
}

//EVENT LISTENER paint-on-mouseenter is attached directly to the html element
AFRAME.registerComponent('paint-on-mouseenter', {
  schema: {
    //il parametro da passare
    to: {default: '2.5 2.5 2.5'},
    color: {default: 'red'}
  },
  //la funzione che viene eseguita
  //mouseenter mouseleave
  //hover-start hover-end
  init: function () {
    var data = this.data;
    var originalColor = this.el.getAttribute('color')
    var originalScale = this.el.getAttribute('scale')
    var originalOpacity = this.el.getAttribute('material').opacity
    var scaleString = originalScale.x + " " + originalScale.y + " " + originalScale.z
    this.el.addEventListener('mouseenter', function () {
      //read again the color because it can change
      originalColor = this.getAttribute('color')
      console.log('mouseenter! Original color was: ' + originalColor)
      //console.log(this.is('cursor-hovered'))
      this.setAttribute('color', data.color);
      this.setAttribute('scale', data.to)
      this.setAttribute('material', 'opacity: 0.5')
    });
    this.el.addEventListener('mouseleave', function () {
      //console.log('mouseleave! Original color was: ' + originalColor)
      //console.log(this.is('cursor-hovered'))
      this.setAttribute('color', originalColor);
      this.setAttribute('scale', scaleString)
      //console.log("originalOpacity: " + originalOpacity)
      this.setAttribute('material', 'opacity: ' + originalOpacity)
    });
  }
});

AFRAME.registerComponent('scale-on-mouseenter', {
  schema: {
    //il parametro da passare
    to: {default: '2.5 2.5 2.5'},
    color: {default: 'red'}
  },
  //la funzione che viene eseguita
  //mouseenter mouseleave
  //hover-start hover-end
  init: function () {
    var data = this.data;
    var originalColor = this.el.getAttribute('color')
    var originalScale = this.el.getAttribute('scale')
    var originalOpacity = this.el.getAttribute('material').opacity
    var scaleString = originalScale.x + " " + originalScale.y + " " + originalScale.z
    this.el.addEventListener('mouseenter', function () {
      //read again the color because it can change
      //originalColor = this.getAttribute('color')
      //console.log('mouseenter! Original color was: ' + originalColor)
      //console.log(this.is('cursor-hovered'))
      //this.setAttribute('color', data.color);
      this.setAttribute('scale', data.to)
      this.setAttribute('material', 'opacity: 0.5')
    });
    this.el.addEventListener('mouseleave', function () {
      //console.log('mouseleave! Original color was: ' + originalColor)
      //console.log(this.is('cursor-hovered'))
      //this.setAttribute('color', originalColor);
      this.setAttribute('scale', scaleString)
      //console.log("originalOpacity: " + originalOpacity)
      this.setAttribute('material', 'opacity: ' + originalOpacity)
    });
  }
});


AFRAME.registerComponent("random-position",{
  schema:{
    min:{default:{x:-10,y:-10,z:-10},type:"vec3"},
    max:{default:{x:10,y:10,z:10},type:"vec3"},
    away:{default: false}
  },
  update:function(){
    var t=this.data,a=t.max,e=t.min;
    //
    let randX = (Math.random()*(a.x-e.x)+e.x).toFixed(2);
    let randY = (Math.random()*(a.y-e.y)+e.y).toFixed(2);
    let randZ = (Math.random()*(a.z-e.z)+e.z).toFixed(2);

    this.el.setAttribute("position",{
      x:randX,
      y:randY,
      z:randZ
    }
    )
  }
});

AFRAME.registerComponent("bouncing",{
  schema:{
    amplitude:{default:0.2,type:"float"}
  },
  init: function(){
    var data=this.data
    var offset = data.amplitude/2
    var pos =this.el.getAttribute("position")
    var from= {x:pos.x, y:pos.y-offset, z:pos.z}
    var to = {x:pos.x, y:pos.y+offset, z:pos.z}
    var animation = {
      property: 'position',
      from: from,
      to: to,
      dir: 'alternate',
      loop: true,
      easing: 'easeInOutQuad',
      enabled: true
    }
    this.el.setAttribute("animation__bouncing",animation)
    //update animation when entity is moved
    /*this.el.addEventListener('componentchanged', function (evt) {
      if (evt.detail.name === 'position') {
        var el = evt.target
        console.log('Entity has moved to', el.getAttribute('position'), '!');
        console.log(animation)
        pos=el.getAttribute("position")
        animation.from ={x:pos.x, y:pos.y-offset, z:pos.z}
        animation.to = {x:pos.x, y:pos.y+offset, z:pos.z}
        el.setAttribute("animation__bouncing",animation)
      }
    }) */

  },
  remove:function(){
    //this.el.setAttribute('animation__bouncing', 'enabled', false)
    this.el.removeAttribute('animation__bouncing')
    //var pos = this.el.getAttribute("position")
    //this.el.setAttribute('position', {x:pos.x, y:pos.y + this.offset, z:pos.z })
    /*var pos = this.el.getAttribute('position')
    if(pos.y < 2)
      this.el.setAttribute('position', {x:pos.x, y:2, z:pos.z})*/
  }
});
AFRAME.registerComponent('stop-bouncing-when-grabbed', {
  init: function(){
    var el = this.el
    el.addEventListener('grab-start', function(){
      el.removeAttribute('bouncing')
    })
    el.addEventListener('grab-end', function(){
      el.setAttribute('bouncing')
    })
  }
})

AFRAME.registerComponent('prova-video', {
  multiple: true,
  schema: {
    event: {type: 'string', default: ''},
    message: {type: 'string', default: 'Hello, World!'}
  },
  init: function () {
    const data = this.data
    this.el.addEventListener('click',(e)=>{
      let video = document.getElementById('test-video')
      console.log(video)
      video.play()
    })

  }
});

AFRAME.registerComponent('test-component', {
  multiple: true,
  schema: {
    event: {type: 'string', default: ''},
    message: {type: 'string', default: 'Hello, World!'}
  },
  init: function () {
    const data = this.data
    if(data.event){
        this.el.addEventListener(data.event, function(){
            console.log(data.message)
        })
    }

  }
});

function simulateMouseEvent(el, mouseEvent) {
  var event = new MouseEvent(mouseEvent, {
    'view': window,
    'bubbles': true,
    'cancelable': true
  });
  var canceled = !el.dispatchEvent(event);
}
AFRAME.registerComponent('map-events', {
  schema: {
    fromEvts: {type: 'array', default: ['hover-start', 'hover-end']},
    toEvts: {type: 'string', default: ['mouseenter', 'mouseleave']}
  },
  init: function () {
    const data = this.data
    const fromEvts = data.fromEvts
    const toEvts = data.toEvts
    let el = this.el
    let id = el.getAttribute('id')
    if(fromEvts.length != toEvts.length){
      console.error(el, 'Number of events to be mapped must be the same')
      return
    }
    fromEvts.forEach((fromEvt, i) =>{
      el.addEventListener(fromEvt, (e)=>{
        //let's simulate the corresponding mapped mouse event
        simulateMouseEvent(el, toEvts[i])
      })
    })

  }
})
AFRAME.registerComponent('signal-intersection', {
  init: function () {
    let el = this.el
    //use a global variable to avoid conflicts with raycaster
    el.addEventListener('raycaster-intersected', (e)=>{
      buttonIntersected = true
    })
    el.addEventListener('raycaster-intersected-cleared', (e)=>{
      buttonIntersected = false
    })
  }
})

AFRAME.registerComponent('test-button', {

  init: function () {
    const button = this.el.querySelector('#button')

    this.el.addEventListener('click', function(){
        console.log('clicked')
        let toggle = !button.getAttribute('visible')
        button.setAttribute('visible', toggle)
    })


  }
});

AFRAME.registerComponent('button-component', {

  init: function () {

    this.el.addEventListener('click', function(evt){
        console.log('plane clicked!')
        evt.stopPropagation();
    })


  }
});

AFRAME.registerComponent('event-info-component', {
  schema: {
    event: {type: 'string', default: ''},
  },
  init: function () {
    const data = this.data
    if(data.event){
        this.el.addEventListener(data.event, function(evt){
            console.log(evt)
        })
    }

  }
});

AFRAME.registerComponent('grab-test', {
  schema: {
    event: {type: 'string', default: ''},
  },
  init: function () {
    var el = this.el
    el.addEventListener('grab-start', function(evt){
      console.log('GRAB START:')
      console.log(el.getAttribute('id'))
      //console.log('grab:', el.is('grabbed'))

    })

    el.addEventListener('grab-end', function(evt){
      console.log('GRAB END:')
      console.log(el.getAttribute('id'))
      //console.log('grab:', el.is('grabbed'))
    })

    el.addEventListener('stateadded', function(evt){
      console.log('state-added', evt.detail)
      console.log(el.getAttribute('id'))
    })

    el.addEventListener('stateremoved', function(evt){
      console.log('state-removed', evt.detail)
      console.log(el.getAttribute('id'))
    })

  }
});

AFRAME.registerComponent('my-menu', {
  schema: {
    event: {type: 'string', default: ''},
  },
  init: function () {
    let el = this.el
    let data = this.data
    let options = ['Add', 'Push into', 'Push out', 'Edit', 'Delete']
    let icons = ['plus', 'log-in', 'log-out', 'edit', 'android-delete']
    let menu = createMenu(el, options, icons, menuButtonHandler)
    //add the possibility to toggle menu clicking on element
    el.addEventListener('click', function(){
      let holdable = d3.select(el).classed('holdable')
      let opacity = el.getAttribute('material').opacity
      let opaque = opacity == 1
      let id = el.getAttribute('id')
      //if we click on another bubble when a push selection is waited
      if(waitingForPushInto && el != requestingPushInto){
        let scene = document.querySelector('a-scene')
        //check if the target is a term or a category
        let nested = d3.select(el).classed('nested')
        //if target:
        //1)is not a term
        //2)is not the requesting bubble
        //3)is not the bubble we are inside
        if(nested && holdable /*&& el != requestingPushInto*/){
          let innerMenu = d3.select(requestingPushInto.querySelector('.innerMenu'))
          scene.emit('push-into', {requestingEntity: requestingPushInto, targetEntity: el})
          //hide push into menu in requestingEntity
          innerMenu.attr('visible', false)
          innerMenu.attr('position', '0 0 0')

        }else{
          printLog(noPushIntoTermsMsg)
        }
        return
      }//fine selection push into
      if(leftWristMenuOpen|| rightWristMenuOpen || buttonIntersected || !holdable || opaque){
        return
      }
      let otherMenus = Array.from(document.getElementsByClassName('menu'))
      //iterate through all the other menus
      otherMenus.forEach((element)=>{
        let bubbleRadius = parseFloat(document.getElementById(id).components.geometry.attrValue.radius)
        if(bubbleRadius > 1){
          //happens if two bubbles have the same name
          bubbleRadius = 1
        }
        let hiddenPos = {x: 0, y:0, z:bubbleRadius}
        let parent = element.parentNode
        let menu = d3.select(element)
        let inner = menu.classed('innerMenu')
        if(parent == el && !inner){
          //the selected element's menu
          let toggle = menu.attr('visible')
          let newPos = toggle ? '0 0 0' : hiddenPos
          menu.attr('visible', !toggle)
          menu.attr('position', newPos)
        }else{
          //other menus
          menu.attr('position', '0 0 0')
          menu.attr('visible', false)
        }
      })
      //reset the corresponding value in menuMap
      menuMap[id] = 0
      //reset the variables for push into selection
      waitingForPushInto = false
      requestingPushInto = null

    })
  }
});

AFRAME.registerComponent('wrist-menu', {
  schema: {
    hand: {type: 'string', default: 'right'}
  },
  init: function () {
    let el = this.el
    let data = this.data
    let hand = data.hand
    let options = null
    let icons = null
    let callback = null
    let id = null
    let hidden = true
    //TODO: settare hidden a true in entrambi
    if(hand == 'right'){
      id = 'right-wrist-menu'
      options = ['Base map']
      icons = ['android-exit']
      callback = rightWristMenuButtonHandler
      //hidden = false
    }else if(hand == 'left'){
      id = 'left-wrist-menu'
      options = ['Load', 'Save as', 'Save', 'New']
      icons = ['android-folder-open', 'archive', 'archive', 'plus']
      callback = leftWristMenuButtonHandler
      //hidden = false
    }else{
      //should never happen
      return
    }

    let menu = createWristMenu(el, id, options, icons, callback, hidden)
    el.addEventListener('thumbsticktouchend', function(){
      //use another variable since the original menu could be destroyed and replaced
      let wristMenu = d3.select('#'+id)
      let toggle = wristMenu.attr('visible')
      //toggle global variables
      if(hand == 'right'){
        rightWristMenuOpen = !toggle
      }else if(hand == 'left'){
        leftWristMenuOpen = !toggle
      }
      //toggle menu visibility
      wristMenu.attr('visible', !toggle)
      wristMenu.attr('position', toggle ? wristMenuHidingPos : wristMenuOriginalPos)
      let menus = Array.from(document.getElementsByClassName('menu'))
      //hide all the other menus
      menus.forEach((m)=>{
        //console.log(m)
        if(d3.select(m).classed('innerMenu')){
          d3.select(m).remove()
        }else{
          if(d3.select(m).attr('visible')){
            d3.select(m)
            .attr('visible', false)
            .attr('position', '0 0 0')
          }
        }
      })
    })
    //fine thumbstick event listener
    //toggle menus also with keys (for PC version)
    document.addEventListener('keypress', function(event) {

    const leftMove = 'j';
    const rightMove = 'l';
    const upMove = 'i';
    const downMove = 'k';
    const inDepthMove = 'z';
    const outDepthMove = 'x';
    let key = event.key;

    if(lookAtEntity.getAttribute("id")!=outerBubbleId){

      if(key == leftMove && lookAtEntityFlag){
        let pos = lookAtEntity.getAttribute("position")
        pos.x  = pos.x - 0.1
        lastKey = key
      }else if(key == rightMove && lookAtEntityFlag){
        let pos = lookAtEntity.getAttribute("position")
        pos.x = pos.x + 0.1
        lastKey = key
      }else if(key == upMove && lookAtEntityFlag){
        let pos = lookAtEntity.getAttribute("position")
        pos.y = pos.y + 0.1
        lastKey = key
      }else if(key == downMove && lookAtEntityFlag){
        let pos = lookAtEntity.getAttribute("position")
        pos.y = pos.y - 0.1
        lastKey = key
      }else if(key == inDepthMove && lookAtEntityFlag){
        let pos = lookAtEntity.getAttribute("position")
        pos.z = pos.z - 0.1
        lastKey = key
      }else if(key == outDepthMove && lookAtEntityFlag){
        let pos = lookAtEntity.getAttribute("position")
        pos.z = pos.z + 0.1
        lastKey = key
      }else{
        lastKey = null
      }
    }
    });

    /*document.addEventListener('keyup', function(event) {
      if(lastKey == 'j' || lastKey == 'k' || lastKey == 'l' || lastKey == 'i' || lastKey == 'z' || lastKey == 'x'){
        lookAtEntityFlag = false;
      }
    });*/

    document.addEventListener('keydown', function(event) {
      const toggleLeftMenuKey = 'q'
      const toggleRightMenuKey = 'e'
      
      let key = event.key
      //use another variable since the original menu could be destroyed and replaced
      let wristMenu = d3.select('#'+id)
      let toggle = wristMenu.attr('visible')
      //toggle global variable
      if(key == toggleLeftMenuKey && hand == 'left'){
        leftWristMenuOpen = !toggle
      }else if(key == toggleRightMenuKey && hand == 'right'){
        rightWristMenuOpen = !toggle
      }else{
        //in any other case does return immediately
        return
      }
      //toggle menu visibility
      wristMenu.attr('visible', !toggle)
      wristMenu.attr('position', toggle ? wristMenuHidingPos : wristMenuOriginalPos)
      let menus = Array.from(document.getElementsByClassName('menu'))
      //hide all the other menus
      menus.forEach((m)=>{
        //console.log(m)
        if(d3.select(m).classed('innerMenu')){
          d3.select(m).remove()
        }else{
          if(d3.select(m).attr('visible')){
            d3.select(m)
            .attr('visible', false)
            .attr('position', '0 0 0')
          }
        }
      })

    })
    //fine keydown event listener
  }
});

AFRAME.registerComponent('interact-with-mouse', {
    init: function() {
      let data = this.data;
        let self = this.el;
        let el = this.el;
        self.addEventListener('mouseenter', function (event) {
          console.log("enter");
          lookAtEntityFlag = true;
          if(this.getAttribute("id")!=outerBubbleId)
            lookAtEntity = this;
         });

         el.addEventListener('click', function (event) {
           console.log(data.color);
           el.setAttribute('material', 'color', data.color);
         });
    }
});

AFRAME.registerComponent('pull-and-push', {
  //this component add event listeners to CONTROLLERS
  //in order to set the global variables used to move bubbles closer/further
  schema: {
    event: {type: 'string', default: ''},
  },
  init: function () {
    const data = this.data
    const pullStartEvents = ['abuttondown', 'xbuttondown']
    const pullEndEvents = ['abuttonup', 'xbuttonup']
    const pushStartEvents = ['bbuttondown', 'ybuttondown']
    const pushEndEvents = ['bbuttonup', 'ybuttonup']
    let el = this.el
    //updateMovingVariables(toPull, buttondown)
    pullStartEvents.forEach((event) => {el.addEventListener(event, (e) => {updateMovingVariables(true, true)});})
    pullEndEvents.forEach((event) => {el.addEventListener(event, (e) => {updateMovingVariables(true, false)});})
    pushStartEvents.forEach((event) => {el.addEventListener(event, (e) => {updateMovingVariables(false, true)});})
    pushEndEvents.forEach((event) => {el.addEventListener(event, (e) => {updateMovingVariables(false, false)});})
  }
});

AFRAME.registerComponent('moveable', {
  schema: {
    speed: {
      type: 'number',
      default: 2
    }
  },
  init: function() {
    this.target = this.el.sceneEl.querySelector('#cameraRig');
  },
  tick: function(t, dt) {
    var pullCondition = pull && this.el.is('grabbed')
    var pushCondition = push && this.el.is('grabbed')
    if(!(pullCondition || pushCondition)) return
    var currentPosition = this.el.object3D.position;
    var distanceToCam = currentPosition.distanceTo( this.target.object3D.position );
    var outOfBound = (pull && distanceToCam < 1) || (push && distanceToCam > 30)
    if (outOfBound) return
    var targetPos = this.el.object3D.worldToLocal(this.target.object3D.position.clone())
    var distance = dt*this.data.speed / 4000;
    //verse tells wether to move entity forward or backward
    var verse = pull ? 1 : -1
    this.el.object3D.translateOnAxis(targetPos, verse*distance);
  }
});


AFRAME.registerComponent('center-text', {
  init: function(){
    let el = this.el; 
   
  el.addEventListener("model-loeaded", (e) => {
      let pos= el.getAttribute('position')
      
      let mesh =  el.getObject3D('mesh');
      var box = new THREE.BoxHelper(mesh, 0x00ff00);
      var bbox = new THREE.Box3().setFromObject(box);

      var xScale = el.getAttribute("scale").x
      mesh.position.set((bbox.getSize().x / xScale) / -2, 0, 0)
  }); 
  }
})

AFRAME.registerComponent('new-read-json', {
  schema: {
    //event: {type: 'string', default: 'click'},
    structureFile: {type: 'string', default: 'http://localhost/data/taxonomy.json'},
    termsDetailFile: {type:'string', default: 'http://localhost/data/terms.csv'}
    //message: {type: 'string', default: 'Hello, World!'}
  },
  init: function(){
    //get file name from query param
    const urlParams = new URLSearchParams(window.location.search);
    const filenameParam = urlParams.get('filename')
    const basePath = saveFolder
    const data = this.data
    const scene =this.el
    const newRad = 8
    const oldOpacity = 0.7
    const newOpacity = 1
    const radOffset = 0.1
    let done = false
    let stack = []
    currStack = stack
    let structureFile = data.structureFile
    //if we have loaded a specific save
    if(filenameParam != null){
      structureFile = basePath + filenameParam
      //update global var
      currFilename = filenameParam
    }
    d3.json(structureFile).then(function(json){

      let currLvlData = json
      let prevLvlData = null      
      let currDepthLvl = 0      
      currMaxFreq = 0
      lookForMaxFreq(json)
      console.log('max Freq: ', currMaxFreq)

      
      createBubbles(currLvlData)
      printFilename()      
      //EVENT LISTENER IN SCENE
      scene.addEventListener('bubble-enter', function(event){
        currDepthLvl++
        let stackObj = {}
        let bubble = event.detail.collidingEntity
        let id = bubble.getAttribute('id')
        outerBubbleId = id
        let pos = bubble.getAttribute('position')
        let color = bubble.getAttribute('material').color
        let rad = bubble.getAttribute('geometry').radius
        let prevLvlLabel = 'level'+(currDepthLvl-1)
        let grabbed = bubble.is('grabbed')
        let attracted = d3.select(bubble).classed('attracted')
        if(attracted){
          //label is no more necessary
          d3.select(bubble).classed('attracted', false)
          //remove also the animation
          .attr('animation__move', null)
        }
        let spawnOffset = 0
        if(grabbed || attracted){
          //if the bubble crash with the camera before entering
          //it will have to spawn a bit deeper once we exit again
          spawnOffset = -2*rad
        }
        //add data to stack
        stackObj['id'] = id
        stackObj['pos'] = {x:pos.x, y:pos.y ,z:pos.z+spawnOffset}
        stackObj['color'] = color
        stackObj['rad'] = rad

        stack.push(stackObj)
        prevLvlData = currLvlData
        currLvlData = currLvlData[id]
        //printPath(stack)
        updateWristMenu(stack)
        //if we are at the last level currLvlData[id] will be null
        //currPos = {x: pos.x, y: pos.y, z: pos.z}
        //currRad = bubble.getAttribute('geometry').radius
        //currOpacity = bubble.getAttribute('material').opacity
        console.log('Bubble enter:', id);
        //console.log('COLORE ' + color)
        console.log(currLvlData)
        //d3.select('#'+id)
        //console.log(bubble.getAttribute('geometry'))

        //for su prevlvlBubbles ENTER
        let prevLvlBubbles = document.querySelectorAll('.'+prevLvlLabel)
        for(let i=0; i<prevLvlBubbles.length; i++){
          let curr = prevLvlBubbles[i]
          //if it is the bubble we just collided with, turn it into a dome
          if(curr.getAttribute('id') == id){
            //collided bubble
            for(let j=0; j<curr.childNodes.length; j++){
              let child = curr.childNodes[j]
              child.setAttribute('visible', false)
              //if child is a menu also move it
              if(d3.select(child).classed('menu')){
                child.setAttribute('position', '0 0 0')
              }
            }
            //the dome must not be moved with controllers
            curr.classList.remove('holdable')
            //ENLARGE THE BUBBLE
            curr.setAttribute('material', 'side', 'double')
            //bubble.setAttribute('material', 'opacity', 0.4)
            curr.setAttribute('geometry', 'radius', newRad)
            //pause animation to move bubble
            //curr.pause()

            curr.setAttribute('position', {x:0 , y:0 ,z:0})
          }else{
            //other bubbles will be pushed forward
            curr.setAttribute('material', 'opacity', newOpacity)            
            curr.object3D.position.z -= newRad            
          }
        }
        //hide bubbles from 2 levels above
        if(currDepthLvl > 1){
          let remoteLvlLabel = 'level'+(currDepthLvl-2)
          let remoteLvlBubbles = document.querySelectorAll('.'+remoteLvlLabel)
          for(let i=0; i<remoteLvlBubbles.length; i++){
            let curr = remoteLvlBubbles[i]            
            curr.setAttribute('visible', false)
            let prevDomeId = stack[stack.length -2].id
            if(curr.getAttribute('id') == prevDomeId){
              /*
              TRICK: to prevent the previous dome to fire the
              mouseenter/mouseleave event make it a little greater,
              so it goes behind the current dome
              */
              let currRad = curr.getAttribute('geometry').radius
              curr.setAttribute('geometry', 'radius', currRad+radOffset)
            }
            //curr.removeEventListener('mouseenter', showInfo)
            //curr.removeEventListener('mouseleave', hideInfo)
          }
        }
        //if we are entering a mid-level bubble generate other bubbles
        if(currLvlData != null){
          //generate next level bubble and place them into the dome
          createBubbles(currLvlData, currDepthLvl, '-6 1.5 -6', '6 2 -2')
        }else{
          //generate info plane for the last level term
          let terms = prevLvlData.terms
          let selectedTerm = null
          for(let i=0; i<terms.length; i++){
            if(terms[i].name == id){
              selectedTerm = terms[i]
              break
            }
          }
          //let header = 'Concerning ' + selectedTerm.name + ':'
          //let body = 'Frequency: ' + selectedTerm.freq
          //let header = 'Definition of ' + selectedTerm.name
          let header = selectedTerm.name
          let body = selectedTerm.def
          let termDocs = []
          let currDocIndex = 0
          let termImgs = []
          let currImgIndex = 0
          let termImg = null
          //create card
          let termCard = createCard(selectedTerm, currDepthLvl, header, body)
          //look for media in the file
          d3.csv(data.termsDetailFile).then(function(csvArray) {
            //let n=0
            csvArray.forEach((element)=>{

              if(element.term ==  selectedTerm.name){
                if(element.nc == 'image'){
                  let imgPath = element.filepath
                  //console.log(imgPath)
                  termImgs.push(imgPath)
                  //let image = createImage(selectedTerm, currDepthLvl, imgPath)
                  return
                }
                else if(element.nc == 'video'){
                  let domeId = selectedTerm.name
                  let classLabel = 'level'+currDepthLvl
                  d3.select('a-assets').append('video')
                        .attr('id', domeId + "-videosource")
                        .attr('autoplay','true')
                        .attr('src', "http://localhost/media/"+element.filepath)

                  let videoCard = d3.select('a-scene')
                      .append('a-video')
                      .attr('id', domeId + "-video")
                      .attr('mixin', 'myBubble-video')
                      .attr("src","#"+domeId + "-videosource")

                  videoCard.on('click', ()=>{
                    $(this).destroy()
                  })

                }
                else if(element.nc == 'model'){
                	let domeId = selectedTerm.name
        					let classLabel = 'level'+currDepthLvl
                  if(selectedTerm.name=="chicory")
          					d3.select('a-assets').append('a-asset-item')
          					    .attr('id', element.filepath + "-obj")
          					    .attr('src', "http://localhost/media/obj/"+element.filepath+".obj")
                  else{
                    d3.select('a-assets').append('a-asset-item')
                        .attr('id', element.filepath + "-obj")
                        .attr('src', "http://localhost/media/obj/"+element.filepath+".obj")
                    d3.select('a-assets').append('a-asset-item')
                      .attr('id', element.filepath + "-mtl")
                      .attr('src', "http://localhost/media/obj/"+element.filepath+".mtl")
                  }
                  if(selectedTerm.name=="chicory"){
          					let modelCard = d3.select('a-scene')
          						.append('a-entity')
          							.attr('id', domeId + "-model")
          							.attr('mixin', 'myBubble-model')
          							//.attr('obj-model', "obj: #"+element.filepath+"-obj; mtl: #"+element.filepath+"-mtl")
                        .attr('obj-model', "obj: #"+element.filepath+"-obj")
                        .attr('scale','0.03 0.03 0.03')
                        .attr('position',"-2 1 -2")
          							.classed(classLabel, true)
          							.classed('card', true)
                  }
                  else{
                    let modelCard = d3.select('a-scene')
                      .append('a-entity')
                        .attr('id', domeId + "-model")
                        .attr('mixin', 'myBubble-model')
                        .attr('obj-model', "obj: #"+element.filepath+"-obj; mtl: #"+element.filepath+"-mtl")
                        .attr('obj-model', "obj: #"+element.filepath+"-obj")
                        .classed(classLabel, true)
                        .classed('card', true)
                  }
                }

                let newDocument = {}
                newDocument.title = element.title
                newDocument.subtitle = element.subtitle
                //console.log(newDocument)
                termDocs.push(newDocument)
              }
            })
            //console.log(n)
            //fine forEach
          })
          //fine d3.scv


          //BUG: images array takes a while to initialize, i don't know why
          setTimeout(()=>{
            if(termImgs.length > 0){
              termImg = createImage(selectedTerm, currDepthLvl, termImgs[0])
              termImg.on('click', ()=>{
                let clickedX = 0
                if(d3.event.detail == 0){
                  //raycaster case
                  let target = d3.event.target
                  let hand = d3.event.relatedTarget
                  clickedX = hand.components.raycaster.getIntersection(target).point.x
                }else{
                  //cursor case
                  clickedX = d3.event.detail.intersection.point.x
                }
                let verse = 1
                if(clickedX <= 0){
                  verse = -1
                }
                let newIndex = currImgIndex + verse*1
                newIndex = newIndex % (termImgs.length)
                //to enable backward scrolling
                if(newIndex < 0){
                  newIndex = termImgs.length-1
                }
                let newPath = termImgs[newIndex]
                //console.log(newPath)
                termImg.attr('material', 'shader:flat;src:'+newPath)
                //console.log(termImg.attr('material'))
                currImgIndex = newIndex
              })
            }
          }, 500)

          //callback su card
          termCard.on('click', ()=>{
            let cardHeader = termCard.select('.header')
            let cardBody = termCard.select('.body')
            let clickedX = 0
            if(d3.event.detail == 0){
              //raycaster case
              let target = d3.event.target
              let hand = d3.event.relatedTarget
              clickedX = hand.components.raycaster.getIntersection(target).point.x
            }else{
              //cursor case
              clickedX = d3.event.detail.intersection.point.x
            }
            let verse = 1
            if(clickedX <= 0){
              verse = -1
            }
            let newIndex = currDocIndex + verse*1
            newIndex = newIndex % (termDocs.length + 1)
            //to enable backward scrolling
            if(newIndex < 0){
              newIndex = termDocs.length
            }
            let newHeader = header
            let newBody = body
            //convention: 0 index is for base infos
            //if index > 0 subtract 1 and access the document array
            if(newIndex > 0 ){
              newHeader = termDocs[newIndex-1].title
              newBody =  termDocs[newIndex-1].subtitle
            }
            cardHeader.attr('value', newHeader)
            cardBody.attr('value', newBody)
            currDocIndex = newIndex
            //console.log(termDocs.length)
            //console.log(currIndex)

          })
          //fine card callback
        }

        //fine for su prevLvlBubbles
        //let childEnts = bubble.childNodes
        //console.log(childEnts)

        //ENLARGE THE BUBBLE
        //bubble.setAttribute('material', 'side', 'double')
        //bubble.setAttribute('material', 'opacity', 0.4)
        //bubble.setAttribute('geometry', 'radius', newRad)

        //pause animation to move bubble
        //bubble.pause()
        //bubble.setAttribute('position', {x:0 , y:0 ,z:0})
        //console.log(bubble)
        //console.log(bubble.object3D.position)
        //

      })
      //fine bubble enter
      scene.addEventListener('bubble-exit', function(event){
        currDepthLvl--
        let stackObj = stack.pop()
        let bubble = event.detail.collidingEntity
        let id = bubble.getAttribute('id')
        let pos = stackObj.pos
        let color = stackObj.color
        let rad = stackObj.rad
        currLvlData = prevLvlData
        prevLvlData = getPrevLvlData(json, stack)
        let nextLvlLabel = 'level'+(currDepthLvl+1)
        let currLvlLabel = 'level'+(currDepthLvl)
        console.log('Bubble exit:',id);
        console.log(prevLvlData)
        //console.log(bubble)
        //for su lvlBubbles EXIT
        //remove bubbles from the deeper level
        //printPath(stack)
        updateWristMenu(stack)
        let nextLvlBubbles = document.querySelectorAll('.'+nextLvlLabel)
        for(let i=0; i<nextLvlBubbles.length; i++){
          let curr = nextLvlBubbles[i]
          curr.parentNode.removeChild(curr)
        }
        //restore bubbles from current lvl
        let currLvlBubbles = document.querySelectorAll('.'+currLvlLabel)
        for(let i=0; i<currLvlBubbles.length; i++){
          let curr= currLvlBubbles[i]
          //if it is the bubble we just collided with, turn it back to a normal bubble
          if(curr.getAttribute('id') == id){
            //collided bubble
            for(let j=0; j<curr.childNodes.length; j++){
              let child = curr.childNodes[j]
              //show child only if it's not a menu
              if(!d3.select(child).classed('menu')){
                child.setAttribute('visible', true)
              }

            }
            //make it holdable again
            curr.classList.add('holdable')
            curr.setAttribute('material', 'side', 'front')
            curr.setAttribute('geometry', 'radius', rad)
            //curr.pause()
            curr.object3D.position.set(pos.x, pos.y , pos.z )
            //curr.play()
          }else{
            //other bubbles
            //console.log(lvlBubbles[i])
            curr.setAttribute('material', 'opacity', oldOpacity)
            //lvlBubbles[i].pause()
            //curr.pause()
            curr.object3D.position.z += newRad
            //curr.play()
            /*setTimeout(function(){
              curr.setAttribute('bouncing', true)
            }, 100)*/
            /*let inners = lvlBubbles[i].querySelectorAll('[mixin = inner-bubble]')
            for(let i=0; i<inners.length; i++){
              //console.log(inners[i])
              inners[i].setAttribute('visible', true)
            }*/
          }
        }
        //fine for su currLvlBubbles
        //restore bubbles from level above
        if(currDepthLvl > 0){
          let remoteLvlLabel = 'level'+(currDepthLvl-1)
          let remoteLvlBubbles = document.querySelectorAll('.'+remoteLvlLabel)
          for(let i=0; i<remoteLvlBubbles.length; i++){
            let curr = remoteLvlBubbles[i]
            //curr.classList.add('collidable')
            //curr.setAttribute('blink-teleportation', true)

            curr.setAttribute('visible', true)
            let currDomeId = stack[stack.length-1].id
            if(curr.getAttribute('id') == currDomeId){
              /*
              TRICK: to prevent the previous dome to fire the
              mouseenter/mouseleave event make it a little greater,
              so it goes behind the current dome.
              Here we bring it back as it was before
              */
              let currRad = curr.getAttribute('geometry').radius
              curr.setAttribute('geometry', 'radius', currRad-radOffset)
              //make it collidable again
              d3.select(curr).classed('collidable', true)
            }
            //curr.addEventListener('mouseenter', showInfo)
            //curr.addEventListener('mouseleave', hideInfo)
          }
        }

      })
      //fine bubble exit
      scene.addEventListener('add-sibiling',
      (e) =>{
        let category = e.detail.category
        let newName = e.detail.newName
        //create a new bubble into the dome with default radius
        let posMin = '-6 1.5 -6'
        let posMax = '6 2 -2'
        let newBubble = createBubble(newName, 'level'+currDepthLvl, category, posMin, posMax)
        let newBubbleText = createBubbleText(newBubble, newName+'-text', newName)
        //add new category or term to json
        if(category){
          //create a new property on same level and append it
          currLvlData[newName] = {}
        }else{
          //initialize a new term
          let newTerm = {doubt:0, freq: 0, name: newName, weight:1}
          //if curr level does not have a terms array yet, initialize it
          if(currLvlData.terms == null){
            currLvlData.terms = []
          }
          //add it to the curr level terms
          currLvlData.terms.push(newTerm)
          //generate the level bar as well
          let frequency = newTerm.freq
          //let levelBar = createLevelBar(newBubble, newName+'-levelBar', frequency)
        }
        //fine add
        //add an inner bubble to the parent (if there is one)
        if(stack.length > 0){
          let newParentId = stack[stack.length-1].id
          let parentRadius = stack[stack.length-1].rad
          let newParent = document.getElementById(newParentId)
          //modificare inner bubble
          let newInnerBubble = createInnerBubble(d3.select(newParent), newName + '-inner', parentRadius)
          //initially invisible
          newInnerBubble.attr('visible', false)
        }
        console.log('currLvlData', currLvlData)
        console.log('currLvlData\[newName\]', currLvlData[newName])
        console.log('json', json)
        console.log('json\[newName\]', json[newName])
      })
      scene.addEventListener('add-child', (e) =>{
        let category = e.detail.category
        let newName = e.detail.newName
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          console.log('Adding child to a term?')
          console.log('work in progress')
        }
        //add new category or term to json
        if(category){
          //create a new property and append it
          currLvlData[requestingId][newName] = {}
        }else{
          //initialize a new term
          let newTerm = {doubt:0, freq: 0, name: newName, weight:1}
          //if next level does not have a terms array yet, initialize it
          if(currLvlData[requestingId].terms == null){
            currLvlData[requestingId].terms = []
          }
          //add it to the next level terms
          currLvlData[requestingId].terms.push(newTerm)
        }
        //fine add
        //modificare inner bubble
        let newInnerBubble = createInnerBubble(d3.select(requesting), newName + '-inner')
        console.log('requestingId', requestingId)
        console.log('currLvlData\[requestingId\]', currLvlData[requestingId])
        console.log('json', json)
        console.log('json\[requestingId\]', json[requestingId])
      })
      scene.addEventListener('push-into', (e) =>{
        //speed in m/s
        const speed = 4
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let target = e.detail.targetEntity
        let targetId = target.getAttribute('id')
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          let terms = currLvlData.terms
          //search for it in the array and delete from it
          for( let i = 0; i < terms.length; i++){
            if ( terms[i].name == requestingId) {
              requestingData = terms[i]
              terms.splice(i, 1);
              break;
            }
          }
        }
        let targetData = currLvlData[targetId]
        let requestingPos = requesting.getAttribute('position')
        let targetPos = target.getAttribute('position')
        let distance = requestingPos.distanceTo( targetPos );
        //duration in ms
        let dur = distance/speed*1000
        console.log('dur', dur/1000)
        let animationPush = {
          property: 'position',
          to: {x: targetPos.x, y: targetPos.y, z: targetPos.z},
          dur: dur,
          easing: 'easeInQuad'

        }
        //prevent the user from grabbing or colliding with entity while it's moving
        //and see if it is a last level term
        let nested = d3.select(requesting)
        .classed('holdable', false)
        .classed('collidable', false)
        .classed('nested')
        //set the animation
        requesting.removeAttribute('animation__push')
        requesting.setAttribute('animation__push', animationPush)
        requesting.addEventListener('animationcomplete', (e) =>{
          console.log(e.detail.name)
          //if we are pushing a nested bubble
          if(nested){
            //add it to the next level
            currLvlData[targetId][requestingId] = requestingData
            //delete the requesting property from the current level
            delete currLvlData[requestingId]
          }
          //if we are pushing a last level term
          else{
            //if next level does not have a terms array yet, initialize it
            if(currLvlData[targetId].terms == null){
              currLvlData[targetId].terms = []
            }
            //add it to the next level terms
            currLvlData[targetId].terms.push(requestingData)
            //has already been deleted from the current terms array
          }
          //remove the bubble from scene
          requesting.parentNode.removeChild(requesting)
          //add a new bubble into the target outer bubble
          //modificare inner bubble
          let newInnerBubble = createInnerBubble(d3.select(target), requestingId + '-inner')
          //remove an inner bubble from parent bubble (if there is one)
          if(stack.length > 0){
            d3.select(document.getElementById(requestingId+'-inner')).remove()
          }
          console.log('requestingId', requestingId)
          console.log('requestingData', requestingData)
          console.log('currLvlData\[targetId\]\[requestingId\]', currLvlData[targetId][requestingId])
          console.log('json', json)
          console.log('json\[targetId\]', json[targetId])
        })

        //set global vars again to false and null
        waitingForPushInto = false
        requestingPushInto = null
        //console.log('puntatore', targetData)
        //console.log('json', json[targetId])
      })
      scene.addEventListener('push-out', (e) =>{
        const speed = 6
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        if(stack.length < 1){
          return false
        }
        let currLvlLabel = 'level'+currDepthLvl
        let newLvlLabel = 'level'+(currDepthLvl-1)
        let parentId = stack[stack.length - 1].id
        //let parent = document.getElementById(parentId)
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          let terms = currLvlData.terms
          //search for it in the array and delete from it
          for( let i = 0; i < terms.length; i++){
            if ( terms[i].name == requestingId) {
              requestingData = terms[i]
              terms.splice(i, 1);
              break;
            }
          }
        }
        let requestingPos = requesting.getAttribute('position')
        //make sure the bubble will be pushed out of the dome
        let newZ = (requestingPos.z > 0 ? -requestingPos.z: requestingPos.z) - newRad
        let targetPos = {x: requestingPos.x, y:requestingPos.y, z:newZ}
        let distance = requestingPos.distanceTo( targetPos );
        //duration in ms
        let dur = distance/speed*1000
        let animationPush = {
          property: 'position',
          to: {x: targetPos.x, y: targetPos.y, z: targetPos.z},
          dur: dur,
          easing: 'easeInQuad'
        }
        //prevent the user from grabbing or colliding with entity while it's moving
        let nested = d3.select(requesting)
        .classed('holdable', false)
        .classed('collidable', false)
        //change lvl label to the requesting bubble and also its opacity
        .classed(currLvlLabel, false)
        .classed(newLvlLabel, true)
        .attr('material', 'opacity:1')
        //and finally see if it is a last level term
        .classed('nested')
        //set the animation
        requesting.removeAttribute('animation__push')
        requesting.setAttribute('animation__push', animationPush)
        requesting.addEventListener('animationcomplete', (e) =>{
          //if we are pushing a nested bubble
          if(nested){
            //add it to the prev level
            prevLvlData[requestingId] = requestingData
            //delete the requesting property from the current level
            delete currLvlData[requestingId]
          }
          //if we are pushing out a last level term
          else{
            //if prev level does not have a terms array yet, initialize it
            if(prevLvlData.terms == null){
              prevLvlData.terms = []
            }
            //add it to the prev level terms
            prevLvlData.terms.push(requestingData)
            //has already been deleted from the current terms array
          }
          //make bubble holdable again
          d3.select(requesting)
            .classed('holdable', true)
          //remove the inner bubble from its parent
          d3.select(document.getElementById(requestingId+'-inner')).remove()
          //add a new inner bubble into the target outer bubble from 2 levels above
          if(stack.length > 1){
            let newParentId = stack[stack.length-2].id
            let newParent = document.getElementById(newParentId)
            //modificare inner bubble
            let newInnerBubble = createInnerBubble(d3.select(newParent), requestingId + '-inner')
            //initially invisible
            newInnerBubble.attr('visible', false)
          }
          console.log('requestingId', requestingId)
          console.log('requestingData', requestingData)
          console.log('prevLvlData\[requestingId\]', prevLvlData[requestingId])
          console.log('json', json)
          console.log('json\[parentId\]', json[parentId])
        })
      })
      scene.addEventListener('edit-name', (e) =>{
        let newName = e.detail.newName
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          let terms = currLvlData.terms
          //search for it in the array and modify the name field
          for( let i = 0; i < terms.length; i++){
            if ( terms[i].name == requestingId) {
              terms[i].name = newName
              break;
            }
          }
        }else{
          //otherwise create another property with the new name
          currLvlData[newName] = requestingData
          //and delete the old one
          delete currLvlData[requestingId]
        }
        //update the bubble
        requesting.setAttribute('id', newName)
        //delete former text
        let oldText = document.getElementById(requestingId + '-text')
        d3.select(oldText).remove()
        //create a new one
        createBubbleText(d3.select(requesting), newName+'-text', newName)
        console.log('requestingId', requestingId)
        console.log('requestingData', requestingData)
        console.log('currLvlData\[requestingId\]', currLvlData[requestingId])
        console.log('currLvlData\[newName\]', currLvlData[newName])
        console.log('currLvlData', currLvlData)
        console.log('json', json)
      })
      scene.addEventListener('edit-frequency', (e) =>{
        let newFreq = e.detail.newFrequency
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let levelBar = document.getElementById(requestingId + '-levelBar')
        if(levelBar == null){
          //if there is no levelBar
          return
        }
        let barId = levelBar.getAttribute('id')
        let bubble = d3.select(levelBar.parentNode)
        if(currLvlData.terms == null){
          //if currLvl does not have a term array
          //IT SHOULD NEVER HAPPEN since we are modifying frequencies of terms
          return
        }
        //select all last level terms
        let terms = currLvlData.terms
        //search for the requesting one in the array and modify the freq field
        for( let i = 0; i < terms.length; i++){
          if ( terms[i].name == requestingId) {
            terms[i].freq = newFreq
            break;
          }
        }
        //max frequency could have changed
        currMaxFreq = 0
        lookForMaxFreq(json)
        console.log('max Freq: ', currMaxFreq)
        //update level bar
        d3.select(levelBar).remove()
        //let newBar = createLevelBar(bubble, barId, newFreq)
        //UPDATE BUBBLES? MAYBE LATER
        console.log('requestingId', requestingId)
        console.log('currLvlData.terms', currLvlData.terms)
        console.log('json', json)
      })
      scene.addEventListener('delete', (e) =>{
        let requesting = e.detail.requestingEntity
        let requestingId = requesting.getAttribute('id')
        let requestingData = currLvlData[requestingId]
        if(requestingData == null){
          //if it's a last level term
          let terms = currLvlData.terms
          //search for it in the terms array and delete from it
          for( let i = 0; i < terms.length; i++){
            if ( terms[i].name == requestingId) {
              requestingData = terms[i]
              terms.splice(i, 1);
              break;
            }
          }
        }
        //duration in ms
        const dur = 1000
        const minRad = 0.1
        let animationDelete = {
          property: 'scale',
          to: {x: minRad, y: minRad, z: minRad},
          dur: dur,
          easing: 'easeInQuad'
        }
        //prevent the user from grabbing or colliding with entity while it's moving
        //and see if it is a last level term
        let nested = d3.select(requesting)
        .classed('holdable', false)
        .classed('collidable', false)
        .classed('nested')
        //set the animation
        requesting.removeAttribute('animation__delete')
        requesting.setAttribute('animation__delete', animationDelete)
        requesting.addEventListener('animationcomplete', (e) =>{
          console.log(e.detail.name)
          //if we are pushing a nested bubble
          if(nested){
            //delete the requesting property from the current level
            delete currLvlData[requestingId]
          }
          //if we are deleting a last level term
          //has already been deleted from the current terms array
          //remove the bubble from scene
          requesting.parentNode.removeChild(requesting)
          //remove an inner bubble from parent bubble (if there is one)
          if(stack.length > 0){
            d3.select(document.getElementById(requestingId+'-inner')).remove()
          }
          //max frequency could have changed
          currMaxFreq = 0
          lookForMaxFreq(json)
          console.log('max Freq: ', currMaxFreq)
          //
          console.log('requestingId', requestingId)
          console.log('requestingData', requestingData)
          console.log('currLvlData\[requestingId\]', currLvlData[requestingId])
          console.log('currLvlData', currLvlData)
          console.log('json', json)
          //console.log('json\[targetId\]', json[targetId])
        })
      })
      scene.addEventListener('change-level', (e)=>{
        let entityToSignal = e.detail.entityToSignal
        let newLevelId= e.detail.newLevelId
        //e.g. base map ce l'ha a -1
        let stackIndex = e.detail.stackIndex
        //e.g. base map ce l'ha a 0
        let newCurrLvl = stackIndex + 1
        //set the stack as it was in the level immediately following the one we clicked
        stack.splice(stackIndex+2)
        //remove all the bubbles from last level to newCurrLvl+1
        //e.g. if base map remove all bubbles except level1 and level0
        for(let i=currDepthLvl; i>newCurrLvl+1; i--){
          let lvlLabel = 'level'+i
          d3.selectAll('.'+lvlLabel).remove()
        }
        //also set prevLvlData as it was in that level
        prevLvlData = getPrevLvlData(json, stack)
        //same for the lvl variable
        currDepthLvl = newCurrLvl+1
        //find the last dome bubble serching only among its sibiling bubbles (possible aliases)
        let lastDome = null
        let lastDomeId = stack[stack.length-1].id
        let sibilings = Array.from(document.getElementsByClassName('level'+newCurrLvl))
        sibilings.forEach((element)=>{
          if(element.getAttribute('id') == lastDomeId){
            lastDome = element
          }
          //make all sibilings visible and collidable again: bubble-exit doesn't do that
          d3.select(element)
          .attr('visible', true)
          .classed('collidable', true)
        })
        //and simulate a bubble exit
        if(lastDome != null){
          scene.emit('bubble-exit', {collidingEntity: lastDome})
          entityToSignal.emit('position-changed')
        }

      })
      scene.addEventListener('save-file', (e)=>{
        let name = e.detail.name
        socket.emit('save file', {name: name, file: json})
      })
      scene.addEventListener('load-file', (e)=>{
        //provvisorio, redirect sempre sullo stesso file
        let name = e.detail.name
        //reload the page with a new query param
        window.location.replace(window.location.pathname+"?"+$.param({filename: name}))
      })
      scene.addEventListener('find-word', (e)=>{
        let word = e.detail.word
        let path = getWordPath(json, word)
        if(path != null){
          printLog(path)
        }
      })


    }, function(error){
      //use this callback to handle errors in access to file
      let monitor = document.getElementById(speechBoxId)
      d3.select(monitor).attr('value', 'Error in file \''+structureFile+'\'')
      //the ony possible thing is to load another valid file
      scene.addEventListener('load-file', (e)=>{
        let name = e.detail.name
        //reload the page with a new query param
        window.location.replace(window.location.pathname+"?"+$.param({filename: name}))
      })
    })
    //fine d3.json

  }
  //fine init func
})

AFRAME.registerComponent('blink-teleportation', {
  schema: {
    pos: {type: 'vec3', default: {x:0, y:0, z:0}},
    dur: {type: 'number', default: 300},
    eventIn: {default: 'hitstart'},
    eventOut: {default: 'hitend'},
    hide: {type: 'boolean', default: false}
  },

  init: function () {
    var el = this.el;
    var data = this.data;
    //console.log(color)
    var radius = 1
    var camera = document.querySelector('#camera');
    var cameraRig = document.querySelector('#cameraRig');
    var cursor = document.querySelector('a-cursor');
    var scene = document.querySelector('a-scene');
    /*var collidableEls = document.querySelectorAll('[blink-teleportation]')
    console.log(collidableEls)*/
    var fadeAnim = {
      property: 'material.opacity',
      from: 0,
      to: 1,
      dur: data.dur,
      easing: 'easeOutCubic'
    }
    // CREATE A TRANSPARENT BLACK IMAGE
    var blink = document.createElement('a-image');
    blink.setAttribute('material', {
      color: '#000000',
      opacity: 0
    });
    // SET THE BLACK IMAGE POSITION AND APPEND IT AS CAMERA'S CHILD ENTITY
    blink.object3D.position.z = -0.1;
    camera.appendChild(blink);
    //set to false to prevent bug
    blink.setAttribute('visible', false)


    // ON COLLISION (hitstart), ANIMATE THE BLACK IMAGE (FADE-IN)
    el.addEventListener(data.eventIn, function () {
      //let's prevent event to fire too often
      if(eventLocker)
        return;
      eventLocker = true;
      setTimeout(function(){
        eventLocker = false;
      }, blinkEventLockerTime)
      //
      //set to true to make animation visible
      blink.setAttribute('visible', true)
      blink.setAttribute('animation', fadeAnim);
      // WHEN FADE-IN ANIMATION COMPLETES, MOVE THE CAMERA RIG TO DESTINATION
      setTimeout(function () {
        cameraRig.setAttribute('position', data.pos);
        camera.setAttribute('position', {x: data.pos.x, y: data.pos.y+1.6, z: data.pos.z})
        //el.setAttribute('material', 'side', 'double')
        //el.setAttribute('geometry', 'radius', 10)
        //collidableEls must be here in event handler in order
        //to select all elements when initialized
        var collidableEls = document.querySelectorAll('[blink-teleportation]')
        for (var i = 0; i < collidableEls.length; i++) {
          collidableEls[i].classList.remove('collidable')
        }
        //EMIT ENTER EVENT TO SCENE
        scene.emit('bubble-enter', {collidingEntity: el});
        // THEN MAKE ONLY THE SELECTED ELEMENT COLLIDABLE
        el.classList.add('collidable')
        // EMIT CUSTOM EVENT TO TRIGGER THE FADE-OUT ANIMATION
        el.emit('position-changed');

      }, data.dur);
    });

    // ON CUSTOM EVENT, ANIMATE THE BLACK IMAGE (FADE-OUT)
    el.addEventListener('position-changed', function () {
      blink.setAttribute('animation', {
        to: 0
      });
      blink.setAttribute('visible', false)
      //d3.select(blink).remove()
    });

    el.addEventListener(data.eventOut, function(){
      //let's prevent event to fire too often
      if(eventLocker)
        return;
      eventLocker = true;
      setTimeout(function(){
        eventLocker = false;
      }, blinkEventLockerTime)
      //
      //set to true to make animation visible
      blink.setAttribute('visible', true)
      blink.setAttribute('animation', fadeAnim);
      // WHEN FADE-IN ANIMATION COMPLETES, MOVE THE CAMERA RIG TO DESTINATION
      setTimeout(function () {
        cameraRig.setAttribute('position', data.pos);
        camera.setAttribute('position', {x: data.pos.x, y: data.pos.y+1.6, z: data.pos.z})
        //el.setAttribute('material', 'side', 'front')
        //el.setAttribute('geometry', 'radius', radius)
        var collidableEls = document.querySelectorAll('[blink-teleportation]')
        for (var i = 0; i < collidableEls.length; i++) {
          collidableEls[i].classList.add('collidable')
        }
        //EMIT EXIT EVENT TO SCENE
        scene.emit('bubble-exit', {collidingEntity: el});
        // EMIT CUSTOM EVENT TO TRIGGER THE FADE-OUT ANIMATION
        el.emit('position-changed');
      }, data.dur);
    });
  }
});
