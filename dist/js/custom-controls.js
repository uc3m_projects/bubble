AFRAME.registerComponent('custom-controls', {
  schema: {
    enabled: { default: true },
    reverseEnabled: { default: true }
  },

  init: function () {
  	  debugger;
    this.dVelocity = new THREE.Vector3();
    this.bindMethods();
    this.direction = 0;
  },

  play: function () {
    this.addEventListeners();
  },

  pause: function () {
    this.removeEventListeners();
    this.dVelocity.set(0, 0, 0);
  },

  remove: function () {
    this.pause();
  },

  addEventListeners: function () {
    const sceneEl = this.el.sceneEl;
    const canvasEl = sceneEl.canvas;

    if (!canvasEl) {
      sceneEl.addEventListener('render-target-loaded', this.addEventListeners.bind(this));
      return;
    }

    document.querySelector("#btn-forward").addEventListener("touchstart", this.onTouchStart);
    document.querySelector("#btn-forward").addEventListener("touchend", this.onTouchEnd);
    document.querySelector("#btn-backward").addEventListener("touchstart", this.onTouchStart);
    document.querySelector("#btn-backward").addEventListener("touchend", this.onTouchEnd);

  },

  removeEventListeners: function () {
    const canvasEl = this.el.sceneEl && this.el.sceneEl.canvas;
    if (!canvasEl) { return; }

    document.querySelector("#btn-forward").removeEventListener("touchstart", this.onTouchStart);
    document.querySelector("#btn-forward").removeEventListener("touchend", this.onTouchEnd);
    document.querySelector("#btn-backward").removeEventListener("touchstart", this.onTouchStart);
    document.querySelector("#btn-backward").removeEventListener("touchend", this.onTouchEnd);

  },

  isVelocityActive: function () {
    return this.data.enabled && !!this.direction;
  },

  getVelocityDelta: function () {
    this.dVelocity.z = this.direction;
    return this.dVelocity.clone();
  },

  bindMethods: function () {
    this.onTouchStart = this.onTouchStart.bind(this);
    this.onTouchEnd = this.onTouchEnd.bind(this);
  },

  onTouchStart: function (e) {
    e.srcElement.setAttribute("src", "https://dei.inf.uc3m.es/immersivebubbles/play_circle_outline.svg");
    this.direction = e.srcElement.id === "btn-forward" ? -1 : 1;
    e.preventDefault();
  },

  onTouchEnd: function (e) {
    e.srcElement.setAttribute("src", "https://dei.inf.uc3m.es/immersivebubbles/play_circle_filled.svg");
    this.direction = 0;
    e.preventDefault();
  }
});
