function cleaner(){
    setInterval((e) =>{
      let innerMenus = Array.from(document.getElementsByClassName('innerMenu'))
      innerMenus.forEach((el)=>{
        let visible = d3.select(el).attr('visible')
        if(!visible){
          d3.select(el).remove()
        }
      })
    }, cleanTime)
  }
//return true if string is a valid color
function validTextColor(stringToTest) {
    //Alter the following conditions according to your need.
    if (stringToTest === "") { return false; }
    if (stringToTest === "inherit") { return false; }
    if (stringToTest === "transparent") { return false; }

    var image = document.createElement("img");
    image.style.color = "rgb(0, 0, 0)";
    image.style.color = stringToTest;
    if (image.style.color !== "rgb(0, 0, 0)") { return true; }
    image.style.color = "rgb(255, 255, 255)";
    image.style.color = stringToTest;
    return image.style.color !== "rgb(255, 255, 255)";
}
//a single function to set/unset pull/push variable when button is down/up
function updateMovingVariables(toPull=true, buttondown=true){
  //if buttondown is true the pull/push variable must be set to true and viceversa
  if(toPull){
    pull = buttondown
  }else{
    push = buttondown
  }
}

//recursive function to search max freq
function lookForMaxFreq(obj){
  if(obj instanceof Array ){
    //we are in the term array
    obj.forEach((el)=>{
      if(el instanceof Object && el.hasOwnProperty('freq')){
        if(el.freq > currMaxFreq){
          //update frequency
          currMaxFreq = el.freq
        }
      }
    })
  }else if(obj instanceof Object){
    //we are in another object
    for(let prop in obj){
      if(obj.hasOwnProperty(prop)){
        //call recursion on obj[prop]
        lookForMaxFreq(obj[prop])
      }
    }
  }

}
//recursive function to search the path for a word
function getWordPath(obj, word){
  if(obj instanceof Array ){
    //we are in the term array
    for(let i=0; i<obj.length; i++){
      if(obj[i] instanceof Object && obj[i].hasOwnProperty('name')){
        if(obj[i].name == word){
          //update found
          return word
        }
      }
    }
    return null
  }else if(obj instanceof Object){
    //we are in another object
    for(let prop in obj){
      if(prop == word){
        return prop
      }else{
        //call recursion on obj[prop]
        let returned = getWordPath(obj[prop], word)
        if(returned != null && typeof returned !== 'undefined'){
          return prop + '/' + returned
        }
      }
    }
    return null
  }
}
//recursive function to build the proportions object
function getProportions(obj){
  let returnObj = {}
  let sum = 0
  let max = 0
  if(obj instanceof Array ){
    //we are in the term array
    for(let i=0; i<obj.length; i++){
      if(obj[i] instanceof Object && obj[i].hasOwnProperty('freq') && obj[i].hasOwnProperty('name')){
        let termName = obj[i].name
        //update sum
        sum += obj[i].freq
        //update max
        if(obj[i].freq > max){
          max = obj[i].freq
        }
        //create a subfield for each term
        returnObj[termName] = {}
        //add these fields only to have the same recursive structure
        returnObj[termName]['sumFreq'] = obj[i].freq
        returnObj[termName]['maxFreq'] = obj[i].freq
      }
    }
    returnObj['sumFreq'] = sum
    returnObj['maxFreq'] = max
    return returnObj

  }else if(obj instanceof Object){
    //we are in another object: iterate through properties
    for(let prop in obj){
      if(prop != 'color' && prop != 'name'){
        //fill the property with the result obtained applying recursion on the next level
        if(prop == 'terms'){
          return getProportions(obj[prop])
        }else{
          returnObj[prop] = getProportions(obj[prop])
        }
        //update sum
        sum += returnObj[prop]['sumFreq']
        //update max using the sums of inner levels
        if(returnObj[prop]['sumFreq'] > max){
          max = returnObj[prop]['sumFreq']
        }
      }//fine if su campi particolari: serve davvero?

    }//fine for
    //fill the properties for this level
    returnObj['sumFreq'] = sum
    returnObj['maxFreq'] = max
    return returnObj
  }
}
//recursive function to search all terms and fill the csv
function getAllTerms(obj){

  if(obj instanceof Array ){
    //we are in the term array
    obj.forEach((el)=>{
      if(el instanceof Object && el.hasOwnProperty('name')){
        const limit = 5
        let argument = {}
        argument.keywords = el.name
        argument.limit = limit
        argument.print_urls = false
        argument.format = 'jpg'
        argument.aspect_ratio = 'wide'
        let argStr = ""
        for(let i=0; i< limit; i++){
          argStr += csvIndex++ +"," + el.name + ",image,,http://localhost/media/" + el.name +(i+1)+".jpg,,,\n\n"
        }
        allRecords.push(argument)
        csvString += argStr

      }
    })
  }else if(obj instanceof Object){
    //we are in another object
    for(let prop in obj){
      if(obj.hasOwnProperty(prop)){
        //call recursion on obj[prop]
        getAllTerms(obj[prop])
      }
    }
  }

}
function showInfo(){
  let visible = this.getAttribute('visible')
  /*if(!visible)
    return false*/
  let id = this.getAttribute('id')
  if(id != null){
    d3.select('#'+speechBoxId)
      .attr('value', id)
    //after a certain time remove text
    /*setTimeout((e)=>{
      d3.select('#'+speechBoxId)
        .attr('value', '')
    }, hoverVisibilityTimeout)*/
  }
}

function hideInfo(){
  let visible = this.getAttribute('visible')
  if(!visible)
    return false
  d3.select('#'+speechBoxId)
    .attr('value', '')
}
function getArraysFromObject(json){
  let obj = {}
  let termKeys = 'termKeys'
  let termData = 'termData'
  let keys = 'keys'
  let data = 'data'
  obj[keys] = [];
  obj[data] = [];
  obj[termKeys] = [];
  obj[termData] = [];

  // we fill the arrays with the object properties
  for (let key in json) {
    if (key!='color' && key!='name' && json.hasOwnProperty(key)) {
      if(key == 'terms'){
        //fill the terms arrays with the array data itself
        for(let i=0; i<json.terms.length; i++){
          obj[termKeys].push(json.terms[i].name)
          obj[termData].push(json.terms[i])
        }
      }else{
        obj[keys].push(key);
        obj[data].push(json[key]); // Not necessary, but cleaner, in my opinion.
      }

    }
  }

  return obj
}
function createImage(jsonData, lvl, path){
  const pos = {x: 0, y: 3, z: -3}
  let domeId = jsonData.name
  let classLabel = 'level'+lvl
  let imageCard = d3.select('a-scene')
    .append('a-entity')
    .attr('id', domeId + "-image")
    .attr('mixin', 'myBubble-image')
    .attr('material', 'shader:flat;src:'+path)
    .classed(classLabel, true)
    .classed('card', true)

  return imageCard

}
function createCard(jsonData, lvl, headerStr="Header", bodyStr="Body"){
  /*if(jsonData == null)
    return
  let domeId = jsonData.name
  let freq = jsonData.freq*/
  let domeId = jsonData.name
  let classLabel = 'level'+lvl
  let bubbleInfo = d3.select('a-scene')
    .append('a-plane')
    .attr('id', domeId + "-info")
    .attr('mixin', 'myBubble-info')
    .classed(classLabel, true)
    .classed('card', true)
    //.attr('position', {x:0, y: 0, z: -3})
  let header = bubbleInfo
    .append("a-text")
    .attr('id', domeId + "-info-header")
    .attr("scale", "0.5 0.5 0.5")
    .attr("text", "align:center")
    //.attr("value", "Concerning " + domeId)
    .attr("value", headerStr)
    .attr("color", "red")
    .attr("position", function(d, i){
      var x = 0
      var y = 0.55
      var z = 0
      return x + " " + y + " " + z
    })
    .classed('header', true);
  let words = bubbleInfo
    .append("a-text")
    .attr('id', domeId + "-info-body")
    .attr("text", "align:left")
    .attr("color", "black")
    .attr("scale", "0.4 0.4 0.4")
    //.attr("value", 'frequency: ' + freq)
    .attr("value", bodyStr)
    .attr("position", function(d, i){
      var x = -0.9
      var y = 0.15
      var z = 0
      return x + " " + y + " " + z
    })
    .classed('body', true);
    return bubbleInfo

}
function createBubble(id, lvlLabel, nested, posMin = '-10 1.6 -8', posMax = '10 2 -2', rad = 1){
  /**
   * The way who the position was established has an error. Can not use min and max values.
   * Instead of it the position are establish with random values between the min and max establish
   * by parameters and set it as object. This is the way that the official documentation recommended doing it 
   */

  // start get the random position
  let get_position = (min, max) => parseFloat((Math.random()*(max-min)+ min).toFixed(2));
  let tmp_pos = posMin.split(' ').map(n => parseInt(n));
  let min = {'x': tmp_pos[0], 'y': tmp_pos[1], 'z': tmp_pos[2]};
  tmp_pos = posMax.split(' ').map(n => parseInt(n));
  let max = {'x': tmp_pos[0], 'y': tmp_pos[1], 'z': tmp_pos[2]};
  let pos = { 'x': get_position(min.x, max.x), 'y': get_position(min.y, max.y), 'z': get_position(min.z, max.z)}
  // end get random position and start stablish the values

  let bubble = d3.select('a-scene')
     .append('a-entity')
     .attr('id', id)
     .classed('collidable', true)
     .classed('holdable', true)
     .classed(lvlLabel, true)
     .classed('nested', nested)
     .attr('geometry', 'radius:'+ rad)
     //.attr('radius:', rad)
     .attr('mixin', 'myBubble')
     //.attr('position', 'min:' + posMin + ';max:' + posMax)
     .attr('position', pos)
     //.attr('interact-with-mouse', "color: red")
     .on('mouseenter', showInfo)
     .on('mouseleave', hideInfo)
     .on('hover-start', showInfo)
     .on('hover-end', hideInfo)

  return bubble
}

function createBubbleText(bubble, id, value){
  let bubbleId = bubble.attr('id')
  let bubbleRadius = parseFloat(document.getElementById(bubbleId).components.geometry.attrValue.radius)
  if(bubbleRadius > 1){
    bubbleRadius = 1
  }
  let textPos = {x: 0, y: -(bubbleRadius+0.3*bubbleRadius), z:0}
  let scale = {x: bubbleRadius, y: bubbleRadius, z: bubbleRadius}
  let text = bubble.append('a-entity')
      .attr('id', id)
      .attr('mixin', 'myBubble-text')
      .attr('position', textPos)
      .attr('text-geometry', 'value:'+value)
      //.attr('scale', scale)
  return text
}
function createInnerBubble(bubble, id, outerRad = 0, innerRad = 0){
  let bubbleRadius = parseFloat(document.getElementById(bubble.attr('id')).components.geometry.attrValue.radius)
  if(bubbleRadius > 1){
    //happens if two bubbles have the same name
    bubbleRadius = 1
  }
  if(outerRad == 0){
    outerRad = bubbleRadius
  }
  if(innerRad == 0){
    innerRad = (1/4)*outerRad
  }
  let innerBubble = bubble.append('a-entity')
      .classed('innerBubble', true)
      .attr('id', id)
      .attr('mixin', 'inner-bubble')
      .attr('geometry', 'radius:'+innerRad)
      .attr('random-position', 'min: ' + -outerRad/2 + ' ' + -outerRad/2 + ' ' + -outerRad/2 + ';' +
                               'max: ' + outerRad/2 + ' ' + outerRad/2 + ' ' + outerRad/2)
  return innerBubble
}
function createLevelBar(bubble, id, frequency){
  const maxLength = 100
  const startAngle = 40
  let perc = (frequency/currMaxFreq)
  let newLength = perc*maxLength
  let newStart = (startAngle+maxLength) - newLength
  let color = 'red'
  if(perc > 33/100){
    color = 'yellow'
  }
  if(perc > 66/100){
    color = 'green'
  }
  let enveloper = bubble.append('a-entity')
      .classed('levelBar', true)
      .attr('id', id)
      .attr('position', '0 0.3 0')
  let container = enveloper.append('a-entity')
      .attr('id', id +'-container')
      .attr('mixin', 'level-bar-container')
  let content = enveloper.append('a-entity')
      .attr('id', id +'-content')
      .attr('mixin', 'level-bar-content')
      .attr('position', '0 0 0.01')
      .attr('geometry', 'thetaStart:'+newStart +';thetaLength:' + newLength)
      .attr('material', 'color:'+color)
  enveloper.on('click', ()=>{
    d3.event.stopPropagation()
  })
  return enveloper
}
function removeInnerBubble(bubble, id){
  d3.select(document.getElementById(id)).remove()
}
function createBubbles(jsonData, lvl=0, posMin = '-10 1.6 -8', posMax = '10 2 -2'){
  var i=0
  if(jsonData == null){
    //happens when we enter in an empty bubble
    return
  }
  let classLabel = 'level'+lvl
  /*let lastLevel = false
  if(jsonData.terms != null){
    lastLevel = true
  }*/
  let obj = getArraysFromObject(jsonData)
  let proportions = getProportions(jsonData)
  console.log(proportions)
  let outers = [{data: obj['data'], keys:obj['keys'] , nested: true},
    {data: obj['termData'], keys: obj['termKeys'], nested: false}]

  //create the outer bubbles using both terms and properties
  outers.forEach(function(entry){
    let data = entry['data']
    let keys = entry['keys']
    let nested = entry['nested']
    for(let i=0; i<data.length; i++){
      const maxCategoryRad = 1
      const termRad = 1
      let outerRad = termRad
      let outKey = keys[i]
      if(nested){
        //if it is not a term compute the radius for the outer bubble
        if(typeof proportions[outKey] !== 'undefined'){
          let sumFreq = proportions[outKey]['sumFreq']
          let maxFreq = proportions['maxFreq']
          if(sumFreq > 0){
            //represent the rad proportionally to frequency
            outerRad = (sumFreq/maxFreq)*maxCategoryRad
          }else{
            //if the category is empty represent it as 10% of max size
            outerRad = maxCategoryRad/10
          }
        }

      }
      let bubble = createBubble(outKey, classLabel, nested, posMin, posMax, outerRad)
      let text = createBubbleText(bubble, outKey+'-text', outKey)
      //only for the properties generate the inner bubbles
      if(nested){
        //generate the inner bubbles following the same schema
        let innerObj = getArraysFromObject(data[i])
        let inners = [{data: innerObj['data'], keys:innerObj['keys'] , nested: true},
          {data: innerObj['termData'], keys: innerObj['termKeys'], nested: false}]
        inners.forEach(function(innerEntry){
          let innerData = innerEntry['data']
          let innerKeys = innerEntry['keys']
          for(let j=0; j<innerData.length; j++){
            let inKey = innerKeys[j]
            let maxInnerRad = outerRad/4
            //default value for inner rad
            let innerRad = maxInnerRad/2
            if(typeof proportions[outKey] !== 'undefined' && typeof proportions[outKey][inKey] !== 'undefined'){
              let sumFreqInner = proportions[outKey][inKey]['sumFreq']
              let maxFreqInner = proportions[outKey]['maxFreq']
              if(sumFreqInner > 0){
                //represent the rad proportionally to frequency
                innerRad = (sumFreqInner/maxFreqInner)*maxInnerRad
              }
            }
            let innerBubble = createInnerBubble(bubble, inKey+'-inner', outerRad, innerRad)
          }//fine for su inners
        })
      }else{
        //if it is a term generate the level bar
        let frequency = data[i].freq
        //let levelBar = createLevelBar(bubble, outKey+'-levelBar', frequency)
      }//fine if su nested
    }//fine for su outers
  })

  //


}


function getSibilingsIds(bubble, start=0, number=null){
    let id = bubble.getAttribute('id')
    let classes = bubble.classList
    let lvlLabel = null
    let sibilingsIds = []
    for(let i=0; i< classes.length; i++){
      if(classes.item(i).startsWith('level')){
        lvlLabel = classes.item(i)
        break
      }
    }
    if(lvlLabel == null){
      return null
    }
    //let sibilings = document.querySelectorAll('.'+lvlLabel)
    //BUBBLES CAN NOT BE PUSHED INTO SINGLE TERMS
    let sibilings = document.getElementsByClassName(lvlLabel + ' nested');
    for(let i=0; i<sibilings.length; i++){
      let currId = sibilings[i].getAttribute('id')
      if(currId != id){
        sibilingsIds.push(currId)
      }
    }
    if(start == 0 && number == null){
      return sibilingsIds.sort()
    }
    //let's sort the ids and use slice function to return only the requested portion
    return sibilingsIds.sort().slice(start, number==null ? null: start+number)
  }
  function loadListArrowHandler(){
    let leftArrow = true
    let rightArrow = true
    let menu = d3.select(this.parentNode)
    let menuId = menu.attr('id')
    let visible = menu.attr('visible')
    if(!visible){
      return
    }
    d3.event.stopPropagation()
    let button = d3.event.target.parentNode
    //raycaster sends event to arrow,
    //while mouse sends it to an inner element
    //so with raycaster the previous assignment
    //will be the menu (container)
    if(d3.select(button).classed('wristMenu')){
      button = d3.event.target
    }
    let controller = button.parentNode.parentNode
    console.log('button', button)
    console.log('controller', controller)
  
  
    //distinguish using class label
    let left = d3.select(button).classed('left')
    let verse = 1
    if(left){
      console.log('left')
      verse = -1
    }else{
      console.log('right')
    }
    let options = []
    let icons = []
    //compute the new starting index
    let newStart = loadMenuIndex + verse*loadMenuSize
    //if we are trying to go before page 0
    if(newStart < 0){
      return
    }
    //if we are back in page 0 left arrow is not necessary
    if(newStart == 0){
      leftArrow = false
    }
    function loadArrowCallback(detail){
      //remove listener from socket so that the same callback won't be called more than once
      socket.removeListener('file list', loadArrowCallback)
      //get all files in the folder
      let filenames = detail.filenames
  
      if(newStart >= filenames.length){
        return
      }
      options = filenames.sort().slice(newStart, newStart+loadMenuSize)
      if(options.length == 0){
        return
      }
      //if this is the last page rightArrow is not necessary
      if(newStart + options.length == filenames.length){
        rightArrow  = false
      }
      //update global variable
      loadMenuIndex = newStart
      //fill the icons array
      for(let i=0; i<options.length; i++){
        icons.push('disc')
      }
      //hide previous menu and push it a bit back to avoid click conflicts
      menu.attr('visible', false)
      .attr('position', wristMenuHidingPos)
      //create a new inner menu not hidden with arrows;
      //id must remain the same in this case
      newMenu = createWristMenu(controller, menuId, options, icons, loadButton, false, loadListArrowHandler, leftArrow, rightArrow)
      //mark the menu as an innerMenu
      newMenu.classed('innerMenu', true)
      //just click on inner menu to go back to previous menu
      newMenu.on('click', hideInnerWristMenus)
    }
    //request file list to server
    socket.emit('list request')
    //when we receive the answer
    socket.on('file list', loadArrowCallback)
  }
  function sibilingsListArrowHandler(){
    let leftArrow = true
    let rightArrow = true
    let menu = d3.select(this.parentNode)
    let visible = menu.attr('visible')
    if(!visible){
      return
    }
    d3.event.stopPropagation()
    let button = d3.event.target.parentNode
    //raycaster sends event to arrow,
    //while mouse sends it to an inner element
    //so with raycaster the previous assignment
    //will be the menu (container)
    if(d3.select(button).classed('menu')){
      button = d3.event.target
    }
    let bubble = button.parentNode.parentNode
    let bubbleId = bubble.getAttribute('id')
    //distinguish using class label
    let left = d3.select(button).classed('left')
    let verse = 1
    if(left){
      console.log('left')
      verse = -1
    }else{
      console.log('right')
    }
    //WORK ONLY WITH PUSH INTO LOGIC
    //compute the new starting index
    let newStart = menuMap[bubbleId] + verse*menuSize
    //if we are trying to go back in page 0
    if(newStart < 0){
      return
    }
    //if we go back to page 0 then left arrow is not necessary
    if(newStart == 0){
      leftArrow = false
    }
  
  
    let options = getSibilingsIds(bubble, newStart, menuSize)
    //if nothing is returned
    if(options.length == 0){
      return
    }
    options.forEach(function(element){console.log(element)})
    let icons = []
    for(let i=0; i<options.length; i++){
      icons.push('android-locate')
    }
  
    let allSibilings = getSibilingsIds(bubble)
    //hide right arrow if this is the last page
    if(newStart + options.length == allSibilings.length ){
      rightArrow = false
    }
    //update global variable
    menuMap[bubbleId] = newStart
    //create a new inner menu not hidden with arrows
    newMenu = createMenu(bubble, options, icons, pushIntoButton, false, true, sibilingsListArrowHandler, leftArrow, rightArrow)
    //remove previous menu
    menu.attr('visible', false)
    menu.attr('position', '0 0 0')
    //setTimeout((e)=>{menu.remove()}, menuDelay)
  }
  
  function pushIntoButton(data, index){
    d3.event.stopPropagation();
    //d3.select(this.parentNode.parentNode).classed('holdable', false)
    //d3.select(this).classed('holdable', false)
    //this.removeState('grabbed')
    //this.emit('grab-end', {})
    //this.removeState('hovered')
    //this.emit('hover-end', {})
    console.log(data)
    let scene = document.querySelector('a-scene')
    let menu = d3.select(this.parentNode)
    let bubble = this.parentNode.parentNode
    //let target = document.querySelector('#'+data)
    let target = document.getElementById(data)
    if(target == null){
      return
    }
    scene.emit('push-into', {requestingEntity: bubble, targetEntity: target})
    //hide push into menu
    menu.attr('visible', false)
    menu.attr('position', '0 0 0')
    //setTimeout((e)=>{menu.remove()}, menuDelay)
  }
  function deleteButton(data, index){
    const conf = 'Confirm'
    d3.event.stopPropagation();
    console.log(data)
    let scene = document.querySelector('a-scene')
    let menu = d3.select(this.parentNode)
    let bubble = this.parentNode.parentNode
    if(data == conf){
      scene.emit('delete', {requestingEntity: bubble})
    }
    //hide delete menu
    menu.attr('visible', false)
    menu.attr('position', '0 0 0')
    //setTimeout((e)=>{menu.remove()}, menuDelay)
  
  }
  function editNameButton(data, index){
    d3.event.stopPropagation();
    console.log(data)
    let newName = data
    let menu = d3.select(this.parentNode)
    let scene = document.querySelector('a-scene')
    let bubble = this.parentNode.parentNode
    //if there is already a bubble with that name at the same level or at the next return
    let another = document.getElementById(newName)
    let inner = document.getElementById(newName+'-inner')
    let parent = inner ? inner.parentNode : null
    if(another || (inner && parent == bubble)){
      printLog(duplicateNameMsg)
      return
    }
    waitingForName = false
    editingName = false
    scene.emit('edit-name', {requestingEntity: bubble, newName: newName})
    //delete name selection menu
    menu.attr('visible', false)
    menu.attr('position', '0 0 0')
    //setTimeout((e)=>{menu.remove()}, menuDelay)
  }
  function editFrequencyButton(data, index){
    d3.event.stopPropagation();
    console.log(data)
    let newFreq = Number(data)
    let menu = d3.select(this.parentNode)
    let scene = document.querySelector('a-scene')
    let bubble = this.parentNode.parentNode
    //if the button is not a valid number return
    if(isNaN(newFreq)){
      return
    }
    //if try to change frequency in a nested bubble return
    if(d3.select(bubble).classed('nested')){
      return
    }
    //set the global variables to false again
    waitingForName = false
    editingFrequency = false
    scene.emit('edit-frequency', {requestingEntity: bubble, newFrequency: newFreq})
    //delete name selection menu
    menu.attr('visible', false)
    menu.attr('position', '0 0 0')
    //setTimeout((e)=>{menu.remove()}, menuDelay)
  }
  function confirmNameButton(data, index){
    d3.event.stopPropagation();
    console.log(data)
  
    let newName = data
    let menu = d3.select(this.parentNode)
    let scene = document.querySelector('a-scene')
    let bubble = this.parentNode.parentNode
    let isNested = d3.select(bubble).classed('nested')
    //if there is already a bubble with that name at the same level or at the next return
    let another = document.getElementById(newName)
    let inner = document.getElementById(newName+'-inner')
    let parent = inner ? inner.parentNode : null
    if(another || (inner && parent == bubble)){
      printLog(duplicateNameMsg)
      return
    }
    waitingForName = false
    let options = ['Child category', 'Child term', 'Sibiling category', 'Sibiling term']
    let icons = ['android-add', 'android-add', 'android-add-circle', 'android-add-circle']
    if(!isNested){
      options = ['Sibiling category', 'Sibiling term']
      icons = ['android-add-circle', 'android-add-circle']
    }
    newMenu = createMenu(bubble, options, icons, function(d,i){
      d3.event.stopPropagation();
      console.log(d)
      console.log(data)
      let oldMenu = d3.select(this.parentNode)
      let category = true
      let child = true
      //based on order in options array
      //child term or sibiling term
      if(i == 1 || i== 3){
        category = false
      }
      //sibiling term or sibiling category or if the father is a term
      if(i == 2 || i == 3 || !isNested){
        child = false
      }
      if(child){
        scene.emit('add-child', {requestingEntity: bubble, newName: newName, category: category})
      }else{
        scene.emit('add-sibiling', {newName: newName, category: category})
      }
      oldMenu.attr('visible', false)
      oldMenu.attr('position', '0 0 0')
      //setTimeout((e)=>{oldMenu.remove()}, menuDelay)
    }, false, true)
    //delete name selection menu
    menu.attr('visible', false)
    menu.attr('position', '0 0 0')
    //setTimeout((e)=>{menu.remove()}, menuDelay)
  
  }
  
  function editButton(data, index){
    d3.event.stopPropagation();
    console.log(data)
    let menu = d3.select(this.parentNode)
    let bubble = this.parentNode.parentNode
    switch(data){
      case 'Edit name':
        waitingForName = true
        editingName = true
        //default name
        let newName = 'newName'+newNameCnt
        if(document.getElementById(newName) != null || document.getElementById(newName+'-inner') != null){
          newName = 'newName'+(++newNameCnt)
        }
        options = [newName]
        icons = ['edit']
        printLog(newNameMsg)
        //create a new inner menu not hidden
        newMenu = createMenu(bubble, options, icons, editNameButton, false, true)
        newMenu.classed('editNameMenu', true)
        //remove old menu
        menu.attr('visible', false)
        menu.attr('position', '0 0 0')
        //setTimeout((e)=>{menu.remove()}, menuDelay)
        break
      case 'Edit frequency':
        waitingForName = true
        editingFrequency = true
        //default frequency
        newFreq = '0'
        options = [newFreq]
        icons = ['edit']
        printLog(newFreqMsg)
        //create a new inner menu not hidden
        newMenu = createMenu(bubble, options, icons, editFrequencyButton, false, true)
        newMenu.classed('editFrequencyMenu', true)
        //remove old menu
        menu.attr('visible', false)
        menu.attr('position', '0 0 0')
        //setTimeout((e)=>{menu.remove()}, menuDelay)
        break
    }
  }
  function confirmSaveButton(data, index){
    d3.event.stopPropagation()
    let scene = document.querySelector('a-scene')
    let menu = d3.select(this.parentNode)
    let controller = this.parentNode.parentNode
    //update the global variable holding the currFilename
    currFilename = data
    scene.emit('save-file', {name: currFilename});
    waitingForSave = false
    printFilename()
    //hide inner menus and restore original wrist menu
    d3.select(controller).selectAll('.wristMenu')
    .each(function(d){
      let el = d3.select(this)
      let innerMenu = el.classed('innerMenu')
      if(innerMenu){
        el.attr('visible', false)
        .attr('position', wristMenuHidingPos)
      }else{
        el.attr('visible', true)
        .attr('position', wristMenuOriginalPos)
      }
    })
  }
  function loadButton(data, index){
    d3.event.stopPropagation();
    console.log(data)
    let scene = document.querySelector('a-scene')
    let menu = d3.select(this.parentNode)
    let controller = this.parentNode.parentNode
    let fileToLoad = data
    scene.emit('load-file', {name: fileToLoad})
    //no need to hide menu since the page will reload
  }
  function deleteButton(data, index){
    const conf = 'Confirm'
    d3.event.stopPropagation();
    console.log(data)
    let scene = document.querySelector('a-scene')
    let menu = d3.select(this.parentNode)
    let bubble = this.parentNode.parentNode
    if(data == conf){
      scene.emit('delete', {requestingEntity: bubble})
    }
    //hide delete menu
    menu.attr('visible', false)
    menu.attr('position', '0 0 0')
    //setTimeout((e)=>{menu.remove()}, menuDelay)
  
  }
  function hideInnerWristMenus(){
    let menu = d3.select(d3.event.target)
    let controller = d3.event.target.parentNode
    d3.select(controller).selectAll('.wristMenu').each(function(d){
      let wristMenu = d3.select(this)
      let menuPos = wristMenu.attr('position')
      let inner = wristMenu.classed('innerMenu')
      if(inner){
        //hide so that it will be deleted by the cleaner
        wristMenu.attr('visible', false)
        .attr('position', wristMenuHidingPos)
      }else{
        //if it's the original menu restore its original state
        wristMenu.attr('visible', true)
        .attr('position', wristMenuOriginalPos)
      }
    })
  
  }
  
  function updateWristMenu(stack){
    const zeroLevel = 'Base map'
    const icon = 'android-exit'
    const menuId = 'right-wrist-menu'
    let currMenu = document.getElementById(menuId)
    if(currMenu == null){
      //should never happen
      return
    }
    let controller = currMenu.parentNode
    let options = [zeroLevel]
    let icons = [icon]
    stack.forEach((element)=>{
      options.push(element.id)
      icons.push(icon)
    })
    d3.select(currMenu).remove()
    let newMenu = createWristMenu(controller, menuId, options, icons, rightWristMenuButtonHandler)
    //set global variable to false since there is a new right wrist menu
    rightWristMenuOpen = false
  }
  function leftWristMenuButtonHandler(data, index){
    d3.event.stopPropagation()
    let menu = d3.select(this.parentNode)
    let menuId = menu.attr('id')
    let controller = this.parentNode.parentNode
    let scene = document.querySelector('a-scene')
    let visible = menu.attr('visible')
    if(!visible){
      return
    }
    let options = []
    let icons = []
    let newMenu = null
    switch(data){
      case 'Load':
        function loadCallback(detail){
          //remove listener from socket so that the same callback won't be called more than once
          socket.removeListener('file list', loadCallback);
          //get all files in the folder
          let filenames = detail.filenames
          loadMenuIndex = 0
          options = filenames.sort().slice(loadMenuIndex, loadMenuIndex+loadMenuSize)
          if(options.length == 0){
            printLog(nothingToLoadMsg)
            return
          }
          //fill the icons array
          for(let i=0; i<options.length; i++){
            icons.push('disc')
          }
          let arrowCallback = loadListArrowHandler
          if(filenames.length <= loadMenuSize){
            arrowCallback = null
          }
          let menuPos = menu.attr('position')
          //hide main menu and push it a bit back to avoid click conflicts
          menu.attr('visible', false)
          .attr('position', wristMenuHidingPos)
          //create a new inner menu not hidden just with right arrow (start page);
          newMenu = createWristMenu(controller, menuId+'-load', options, icons, loadButton, false, arrowCallback, false, true)
          //mark the menu as an innerMenu
          newMenu.classed('innerMenu', true)
          //just click on inner menu to go back to previous menu
          newMenu.on('click', hideInnerWristMenus)
        }
        //request file list to server
        socket.emit('list request')
        //when we receive the answer
        socket.on('file list', loadCallback)
        break
      case 'Save':
        console.log(data)
        if(currFilename != null){
          printLog('Saved changes on \''+ currFilename +'\'')
          scene.emit('save-file', {name: currFilename});
          break
        }
        //otherwise if a name has not been defined
        //the code will keep executing below and
        //it will enter the 'Save as' case
  
      case 'Save as':
        //in order to allow editing by speech
        waitingForSave = true
        //default name
        let savename = 'newSave.json'
        if(newSaveCnt > 0){
          savename = 'newSave'+newSaveCnt+'.json'
        }
        function saveCallback(detail){
          //remove listener from socket so that the same callback won't be called more than once
          socket.removeListener('file list', saveCallback)
          //get all files in the folder
          let filenames = detail.filenames
          filenames.sort().forEach(function(file){
            if(file == savename){
              savename = 'newSave'+(++newSaveCnt)+'.json'
            }
          })
          options = [savename]
          icons = ['edit']
          printLog(newSaveMsg)
          //create a new wrist menu not hidden
          newMenu = createWristMenu(controller, menuId+'-saveAs', options, icons, confirmSaveButton, false)
          //mark the menu so it can be accessed outside the callback
          newMenu.classed('newSaveMenu', true)
          //mark the menu as an innerMenu
          .classed('innerMenu', true)
          //just click on inner menu to go back to previous menu
          .on('click', hideInnerWristMenus)
          //remove old menu
          menu.attr('visible', false)
          .attr('position', wristMenuHidingPos)
        }
        //request file list to server
        socket.emit('list request')
        //when we receive the answer
        socket.on('file list', saveCallback)
        break
      case 'New':
        console.log(data)
        //just reload the page with the base address
        window.location.replace(window.location.pathname)
      default:
        break
    }
  }
  
  function rightWristMenuButtonHandler(data, index){
    const dur = 500
    const pos = {x: 0, y: 0, z: 0}
    let menu = d3.select(this.parentNode)
    let controller = this.parentNode.parentNode
    let scene = document.querySelector('a-scene')
    let camera = document.querySelector('#camera');
    let cameraRig = document.querySelector('#cameraRig');
    let visible = menu.attr('visible')
    if(!visible){
      return
    }
  
    if(currStack.length < 1 || index == currStack.length){
      printLog(currLvlMsg)
      return
    }
    //let's prevent event to fire too often
    if(eventLocker)
      return;
    eventLocker = true;
    setTimeout(function(){
      eventLocker = false;
    }, blinkEventLockerTime)
    var fadeAnim = {
      property: 'material.opacity',
      from: 0,
      to: 1,
      dur: data.dur,
      easing: 'easeOutCubic'
    }
    // CREATE A TRANSPARENT BLACK IMAGE
    var blink = document.createElement('a-image');
    blink.setAttribute('material', {
      color: '#000000',
      opacity: 0
    });
    // SET THE BLACK IMAGE POSITION AND APPEND IT AS CAMERA'S CHILD ENTITY
    blink.object3D.position.z = -0.1;
    camera.appendChild(blink);
    blink.setAttribute('visible', true)
    blink.setAttribute('animation', fadeAnim);
    // WHEN FADE-IN ANIMATION COMPLETES, MOVE THE CAMERA RIG TO DESTINATION
    setTimeout(function () {
      cameraRig.setAttribute('position', pos);
      camera.setAttribute('position', {x: pos.x, y: pos.y+1.6, z: pos.z})
      //EMIT ENTER EVENT TO SCENE
      scene.emit('change-level', {newLevelId: data, stackIndex: index-1, entityToSignal: controller});
      // EMIT CUSTOM EVENT TO TRIGGER THE FADE-OUT ANIMATION
      //controller.emit('position-changed');
  
    }, dur);
    // ON CUSTOM EVENT, ANIMATE THE BLACK IMAGE (FADE-OUT)
    //in this case the event will be emitted from the scene in the
    //change-level handler
    controller.addEventListener('position-changed', function () {
      blink.setAttribute('animation', {
        to: 0
      });
      blink.setAttribute('visible', false)
      d3.select(blink).remove()
    });
  
  }
  function menuButtonHandler(data, index){
    let menu = d3.select(this.parentNode)
    let scene = document.querySelector('a-scene')
    let visible = menu.attr('visible')
    if(!visible){
      return
    }
    d3.event.stopPropagation()
    let pressed = this
    let bubble = this.parentNode.parentNode
    let bubbleId = bubble.getAttribute('id')
    let options = []
    let icons = []
    //console.log(this)
    switch(data){
      case 'Add':
        //hide main menu
        menu.attr('visible', false)
        .attr('position', '0 0 0')
        //nameCnt
        //nameListener = true
        waitingForName = true
        newName = 'newName'+newNameCnt
        if(document.getElementById(newName) != null || document.getElementById(newName+'-inner') != null){
          newName = 'newName'+(++newNameCnt)
        }
        options = [newName]
        icons = ['android-add']
        printLog(addNameMsg)
        //create a new inner menu not hidden
        newMenu = createMenu(bubble, options, icons, confirmNameButton, false, true)
        newMenu.classed('newNameMenu', true)
        break
      case 'Push into':
  
        if( menuMap[bubbleId] == 'undefined' ) {
          //initialize the global variables
          menuMap[bubbleId] = 0
        }
        options = getSibilingsIds(bubble, menuMap[bubbleId], menuSize)
  
        if(options.length == 0){
          //there is nothing in which you can push into
          printLog(noPushIntoMsg)
          return
        }
        //set global var to true
        requestingPushInto = bubble
        waitingForPushInto = true
        //print instruction
        printLog(pushIntoMsg)
        let arrowCallback = sibilingsListArrowHandler
        let allSibilings = getSibilingsIds(bubble)
        //hide arrows if the are no options enough
        if(allSibilings.length <= menuSize){
          arrowCallback = null
        }
        //hide main menu
        menu.attr('visible', false)
        .attr('position', '0 0 0')
        //fill the icons array
        for(let i=0; i<options.length; i++){
          icons.push('android-locate')
        }
        //create a new inner menu not hidden ust with right arrow: start of menu;
        //function(d, i) is the button handler
        newMenu = createMenu(bubble, options, icons, pushIntoButton, false, true, arrowCallback, false, true)
        //console.log('PROVA RIUSCITA')
        break
      case 'Push out':
        let lowestLvl = d3.select(bubble).classed('level0')
        if(lowestLvl){
          //we are at the lowest level
          printLog(noPushOutMsg)
          return
        }
        //hide main menu
        menu.attr('visible', false)
        .attr('position', '0 0 0')
        scene.emit('push-out', {requestingEntity: bubble})
        break
      case 'Edit':
        //hide main menu
        menu.attr('visible', false)
        .attr('position', '0 0 0')
        let nested = d3.select(bubble).classed('nested')
        if(nested){
          options= ['Edit name']
          icons = ['clipboard']
        }else{
          options= ['Edit name', 'Edit frequency']
          icons = ['clipboard', 'clipboard']
        }
        //create a new inner menu not hidden
        newMenu = createMenu(bubble, options, icons, editButton, false, true)
        break
      case 'Delete':
        //hide main menu
        menu.attr('visible', false)
        .attr('position', '0 0 0')
        options= ['Confirm', 'Cancel']
        icons = ['checkmark', 'close']
        //create a new inner menu not hidden
        newMenu = createMenu(bubble, options, icons, deleteButton, false, true)
        break
      default:
        break
    }
  
  }
  
  //TODO: set hidden to true by default
  function createWristMenu(el, id, options, icons, buttonCallback, hidden=true, arrowCallback=null, leftArrow = true, rightArrow = true){
    if(options == null || options.constructor != Array){
      return null
    }
    let num = options.length
    let positions = []
    let pos = hidden ? wristMenuHidingPos : wristMenuOriginalPos
    const rot ="-20 0 0"
    const scale = '0.5 0.5 0.5'
    const menuHeight = 2.2
    const menuWidth = 1.5
    const buttonHeight = 0.2
    const buttonWidth = 1
    const buttonOffset = 0.1
    const zCoord = 0.1
    const arrowXOffset = 0.75
    const arrowYOffset = 0
  
    const leftArrPos = {x: -arrowXOffset, y: arrowYOffset , z: zCoord}
    const rightArrPos = {x: arrowXOffset, y: -arrowYOffset, z: zCoord}
    let currentY = menuHeight/2 - buttonHeight/2 - buttonOffset
    for(let i=0; i<num; i++){
      let yCoord = currentY
      //let's subtract in order to go down
      currentY -= (buttonHeight+4*buttonOffset)
      //storing the point's coordinates
      positions.push({x: 0, y: yCoord, z: zCoord});
    }
    let menu = d3.select(el)
    .append('a-entity')
    .classed('wristMenu', true)
    .attr('id', id)
    .attr('position', pos)
    .attr('rotation', rot)
    .attr('scale', scale)
    .attr('visible', !hidden)
    .attr('geometry', 'primitive:plane; width: '+ menuWidth +'; height: '+ menuHeight)
    .attr('material', 'color:#22252a; opacity: 0.7')
    //generate left arrow only if necessary
    if(arrowCallback != null && leftArrow){
      let leftArr = menu.append('a-gui-icon-label-button')
      .attr('id', id+'-menu-left')
      .classed('left', true)
      .classed('arrow', true)
      .attr('height', '0.6')
      .attr('width', '0.2')
      .attr('icon-font-size', '600px')
      .attr('icon', 'arrow-left-b')
      .attr('margin', '0 0 0.05 0')
      .attr('hover-color', '#ed5b21')
      .attr('position', leftArrPos)
      //wrote a component to map raycaster events on mouse events
      .attr('map-events', '')
      //wrote a component to set a global var when a button is intersected
      .attr('signal-intersection', '')
      .on('click', arrowCallback)
  
    }
    //options
    let buttons = menu.selectAll('.option')
    .data(options)
    .enter()
    .append('a-gui-icon-label-button')
    .classed('option', true)
    .attr('id', function(d){
      return id + '-' + d + '-menu-button';
    })
    .attr('value', function(d){return d})
    //.attr('mixin', 'menu-button')
    .attr('width', buttonWidth)
    .attr('height', buttonHeight)
    .attr('icon', function(d,i){
      if(icons == null || icons[i] == null){
        return 'alert'
      }
      return icons[i]
    })
    .attr('icon-font-size', '120px')
    .attr('font-family','Arial')
    .attr('font-size','70px')
    .attr('margin', '0 0 0.05 0')
    .attr('hover-color', '#ed5b21')
    //wrote a component to map raycaster events on mouse events
    .attr('map-events', '')
    //wrote a component to set a global var when a button is intersected
    .attr('signal-intersection', '')
    //.attr('grab-test', '')
    .attr('position', function(d,i){
      return positions[i]
    })
    .on('click', buttonCallback)
    //generate right arrow only if necessary
    if(arrowCallback != null && rightArrow){
      let rightArr = menu.append('a-gui-icon-label-button')
      .attr('id', id+'-menu-right')
      .classed('right', true)
      .classed('arrow', true)
      .attr('height', '0.6')
      .attr('width', '0.2')
      .attr('icon-font-size', '600px')
      .attr('icon', 'arrow-right-b')
      .attr('margin', '0 0 0.05 0')
      .attr('hover-color', '#ed5b21')
      .attr('position', rightArrPos)
      //wrote a component to map raycaster events on mouse events
      .attr('map-events', '')
      //wrote a component to set a global var when a button is intersected
      .attr('signal-intersection', '')
      .on('click', arrowCallback)
    }
  
    return menu
  }
  function createMenu(el, options, icons, buttonCallback, hidden=true, inner=false, arrowCallback=null, leftArrow=true, rightArrow=true){
    if(options == null || options.constructor != Array){
      return null
    }
    let num = options.length
    let positions = []
    const radius = 0.75
    const zCoord = 0.1
    let arrowOffset = 0.3
    //if there is just one arrow...
    if(!leftArrow || !rightArrow){
      //...place it at the center only if there is more than one option
      if(options.length > 1){
        arrowOffset = 0
      }
    }
    let leftArrPos = {x: 0, y: arrowOffset , z: zCoord}
    let rightArrPos = {x: 0, y: -arrowOffset, z: zCoord}
    //the angle from where we start placing icons
    let currentAngle = Math.PI/2
    //the angle offset
    const angleShift = 2*Math.PI/num
    for(let i=0; i<num; i++){
      let angle = currentAngle
      //let's subtract in order to go clockwise
      currentAngle -= angleShift
      //calculating the point's coordinates on the circle circumference
      let xCoord = radius * Math.cos(angle);
      let yCoord = radius * Math.sin(angle);
      //storing the point's coordinates
      positions.push({x: xCoord, y: yCoord, z: zCoord});
    }
    //if there is only one option put it at the center
    if(positions.length == 1){
      positions[0] = {x: 0, y:0, z: zCoord}
    }
    let id = el.getAttribute('id')
    let bubbleRadius = parseFloat(document.getElementById(id).components.geometry.attrValue.radius)
    if(bubbleRadius > 1){
      //happens if two bubbles have the same name
      bubbleRadius = 1
    }
    //scale menu in respect to bubble
    let scale = {x: bubbleRadius, y: bubbleRadius, z: bubbleRadius}
    let hiddenPos = {x: 0, y:0, z: bubbleRadius}
    let menu = d3.select(el)
    .append('a-entity')
    .classed('innerMenu', inner)
    .classed('menu', true)
    .attr('id', id+'-menu-container')
    .attr('position', hidden ?'0 0 0':hiddenPos)
    .attr('visible', !hidden)
    //.attr('geometry', 'primitive:plane; width: 1.5; height: 1.5')
    .attr('geometry', 'primitive:circle; radius:0.75')
    .attr('material', 'color:#22252a; opacity: 0.7')
    .attr('scale', scale)
    //generate left arrow only if necessary
    if(arrowCallback != null && leftArrow){
      let leftArr = menu.append('a-gui-icon-label-button')
      .attr('id', id+'-menu-left')
      .classed('left', true)
      .classed('arrow', true)
      .attr('height', '0.2')
      .attr('width', '0.2')
      .attr('icon-font-size', '400px')
      .attr('icon', 'arrow-left-b')
      .attr('margin', '0 0 0.05 0')
      .attr('hover-color', '#ed5b21')
      .attr('position', leftArrPos)
      //wrote a component to map raycaster events on mouse events
      .attr('map-events', '')
      //wrote a component to set a global var when a button is intersected
      .attr('signal-intersection', '')
      .on('click', arrowCallback)
  
    }
    //options
    let buttons = menu.selectAll('.option')
    .data(options)
    .enter()
    .append('a-gui-icon-label-button')
    .classed('option', true)
    .attr('id', function(d){
      return id + '-' + d + '-menu-button';
    })
    .attr('value', function(d){return d})
    //.attr('mixin', 'menu-button')
    .attr('width', '0.8')
    .attr('height','0.2')
    .attr('icon', function(d,i){
      if(icons == null || icons[i] == null){
        return 'alert'
      }
      return icons[i]
    })
    .attr('icon-font-size', '120px')
    .attr('font-family','Arial')
    .attr('font-size','70px')
    .attr('margin', '0 0 0.05 0')
    .attr('hover-color', '#ed5b21')
    //wrote a component to map raycaster events on mouse events
    .attr('map-events', '')
    //wrote a component to set a global var when a button is intersected
    .attr('signal-intersection', '')
    //.attr('grab-test', '')
    .attr('position', function(d,i){
      return positions[i]
    })
    //.attr('test-component__1', 'event:raycaster-intersected; message:INTERS')
    //.attr('test-component__2', 'event:raycaster-intersected-cleared; message: NINTERS')
    .on('click', buttonCallback)
    //.on('hover-start', function(){this.setAttribute('gui-icon-label-button', 'backgroundColor:#000000')})
    //.on('hover-end', function(){this.setAttribute('gui-icon-label-button', 'backgroundColor:#ffffff')})
    //.on('onclick', buttonCallback)
    //generate right arrow only if necessary
    if(arrowCallback != null && rightArrow){
      let rightArr = menu.append('a-gui-icon-label-button')
      .attr('id', id+'-menu-right')
      .classed('right', true)
      .classed('arrow', true)
      .attr('height', '0.2')
      .attr('width', '0.2')
      .attr('icon-font-size', '400px')
      .attr('icon', 'arrow-right-b')
      .attr('margin', '0 0 0.05 0')
      .attr('hover-color', '#ed5b21')
      .attr('position', rightArrPos)
      //wrote a component to map raycaster events on mouse events
      .attr('map-events', '')
      //wrote a component to set a global var when a button is intersected
      .attr('signal-intersection', '')
      .on('click', arrowCallback)
    }
  
    return menu
  
  }
  function printLog(message){
    let monitor = document.getElementById(logBoxId)
    d3.select(monitor).attr('value', message)
    setTimeout((e)=>{
      let currValue = d3.select(monitor).attr('value')
      //delete message only if it's currently written
      if(currValue == message){
        d3.select(monitor).attr('value', '')
      }
    }, logVisibilityTimeout)
  }
  function printFilename(){
    let monitor = document.getElementById(filenameBoxId)
    let filename = (currFilename == null) ? defaultFilename : currFilename
    d3.select(monitor).attr('value', filename)
  }
  function printPath(stack){
    let monitor = document.getElementById(pathBoxId)
    let pathStr = 'Base Map'
    stack.forEach((el) =>{pathStr += ('\n' + el.id)})
    d3.select(monitor).attr('value', pathStr)
  }
  